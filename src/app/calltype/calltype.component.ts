import { Component, OnInit, NgZone, HostListener } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
import { ViewpriceService } from "../viewprice/viewprice.service";
import { ProfileService } from "../profile/profile.service";
import { CheckoutService } from "../checkout/checkout.service";
import { Configuration } from "../app.constant";
import { MissionService } from "../app.service";
import { LanguageService } from "../app.language";

declare var $: any;
declare var google: any;
declare var flatpickr: any;
declare var AWS: any;
declare var moment: any;
declare var toastr: any;
declare var require: any;
var creditCardType = require("credit-card-type");
var getTypeInfo = require("credit-card-type").getTypeInfo;
var CardType = require("credit-card-type").types;
declare var Stripe: any;
@Component({
  selector: "calltype",
  templateUrl: "./calltype.component.html",
  styleUrls: ["./calltype.component.css"]
})
export class CalltypeComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _zone: NgZone,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _serviceCheckout: CheckoutService,
    private _serviceView: ViewpriceService,
    private _serviceProfile: ProfileService,
    private _lang: LanguageService
  ) {}

  bidLan: any;
  shimmer: any = [];
  loaderList: any;
  loadershimmer = false;
  biddingList: any;
  catId: any;
  count = 0;
  errMsg: any;
  bidPrice: any;
  questionArr: any;
  questionAndAnswer: any = [];
  orderSuccess: any;

  sendDate: any = [];
  asapSchedule: any;
  shiftTime = 0;
  endDate: any;
  startDTime: any;
  endDTime: any;
  daysLists: any;
  timing: any;
  counts = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  bookingType = 1;

  cardWallet: any;
  cardWallets = true;
  id: any;
  cardId: any;
  addMoney: string;
  walletMoney: any;
  paymentMethod = 1;
  paidByWallet = 0;
  payGetCartList: any;
  monthYear: string;
  Month: string;
  Year: string;
  cardCredit: string;
  cardBrand: string;
  cardcvv: string;
  cardHolder: string;
  validDM = false;
  validCD: any;
  stripeToken: any;
  stripeCardDetails = false;
  errorSaveCart: any;
  PaymentLoader = false;
  errorMsg: any;
  newCardToggle: any;

  couponCode: string;
  successMsg: any;
  loaderApply: any;
  couponPrice: any;
  deliveryFee: any;
  promoId: any;
  cancelBtn: any;
  promoCode: any;

  cityAddress: any;
  savedAddress: any;
  addressID: any;
  index: any;
  searchLocationToggle: any;
  typeOne = false;
  typeTwo = false;
  typeThree = false;
  otherText = false;
  addressType = 3;
  Others: string;
  infowindow: any;
  latlng: any;
  widthBar = 0;
  headerrestOpen: any;
  endpicker: any;
  startpicker: any;
  bidShift: any;

  ngOnInit() {    
    this.bidLan = this._lang.bidding;
    this._missionService.scrollTop(true);
    this.placeHolder();
    this.route.params.subscribe(params => {
      this.catId = params["id"];
      this.listService(this.catId);
      // this.sendGetCart();
      // this.sendGetAddress();
      // this.getWallet();
      this.getDate();
    });
    let stripKey = this._conf.getItem("PublishableKey");
    // console.log(stripKey)
    Stripe.setPublishableKey(stripKey);
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }
  scollPos() {
    var div = document.getElementById("addBiddingList").scrollTop;
    console.log(div);
  }

  callType = {
    Description: "",
    active: false,
    checked: false,
    isDefault: false,
    isManadatory: 1,
    preDefined: [
      {
        _id: "1",
        name: "In - Call",
        description:
          "Booking an appointment to visit the provider at his/her office"
      },
      {
        _id: "2",
        name: "Out - Call",
        description: "Looking for a provider to visit your location"
      }
      // {_id: "3", name: "Tele - Call", description:'Booking an appointment and solve by calling'},
    ],
    question: "What are you booking?",
    questionDescription: { SV: "", en: "" },
    questionName: "What are you booking?",
    questionType: 0,
    _id: "1"
  };

  scheduleList: any;

  listService(id) {
    // console.log(id)
    let list = {
      catId: id,
      providerId: "0"
    };
    this.loadershimmer = false;
    this._serviceView.viewServiceList(list).subscribe(
      (res: any) => {
        console.log("customer/services", res);
        this.loadershimmer = true;
        this.biddingList = res.categoryData;
        this.questionArr = res.categoryData.questionArr.filter(a => {
          if (
            a.questionType == 5 ||
            a.questionType == 6 ||
            a.questionType == 7 ||
            a.questionType == 8 ||
            a.questionType == 9 ||
            a.questionType == 10 ||
            a.questionType == 2
          ) {
            return a;
          }
        });
        this.questionArr.forEach(x => {
          x.active = false;
          x.checked = false;
          if (x.questionType == 10) {
            x.answer = [];
          }
        });   
        const inCall =  res.categoryData.callType.incall;     
        const outCall =  res.categoryData.callType.outcall;   
        !inCall?this.callType.preDefined.splice(0,1):!outCall?this.callType.preDefined.splice(1,1):this.callType;
        this.questionArr = [this.callType, ...this.questionArr];        
        this.questionArr[0].active = true;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  addHour(i: number) {
    this.counts = Math.min(Math.max(1, this.counts + i));
  }

  TimePicker(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    var nexthour = moment()
      .add(1, "hour")
      .add(1, "minutes")
      .format("hh a");
    $(".timePick").val(nexthour);
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      altInput: true,
      dateFormat: "h:i K",
      altFormat: "h:i K",
      defaultDate: nexthour,
      // minDate: nexthour,
      // minTime: nexthour,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        // let Time = $("#" + value).val();
        // console.log(dateobj, datestr)
        $(".timePick").val(datestr);
      }
    });
  }

  TimeDuration(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    $(".timeduration").val("01:00");
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      dateFormat: "H:i",
      altFormat: "H:i",
      defaultDate: "01:00",
      minDate: "01:00",
      // minTime: nexthour,
      time_24hr: true,
      altInput: true,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        $(".timeduration").val(datestr);
      }
    });
  }

  public shiftContent = [
    { day: "Budget for all shift", days: "0", checked: false },
    { day: "Budget per shift", days: "1", checked: false },
    { day: "Budget per hour", days: "2", checked: false }
  ];

  contentShift(s, i) {
    this.shiftContent.forEach(x => {
      x.checked = false;
    });
    this.shiftContent[s].checked = true;
    this.questionArr[i].answer = "";
    this.questionArr[i].answers = "";
    this.questionArr[i].checked = false;
    this.grandTotal = 0;
  }

  textListShift(event, i) {
    this.errMsg = false;
    var index = this.shiftContent.findIndex(y => y.checked == true);
    if (index > -1 && event.target.value.length > 0) {
      this.questionArr[i].checked = true;
      var price = event.target.value;
      if (this.shiftContent[index].days == "0") {
        this.grandTotal = Number(price) / Number(this.noShifts);
        this.questionArr[i].answers = this.grandTotal;
        this.questionArr[i].answer = price;
      } else if (this.shiftContent[index].days == "1") {
        this.grandTotal = Number(price);
        this.questionArr[i].answers = this.grandTotal;
        this.questionArr[i].answer = Number(price) * Number(this.noShifts);
      } else {
        this.grandTotal = Number(price) / Number(this.noShifts);
        this.questionArr[i].answers = this.grandTotal;
        let duration = $(".timeduration").val();
        let mulShiftDuration = duration.split(":");
        let total = Number(price) * mulShiftDuration[0];
        let durationPrice;
        if (mulShiftDuration[1] == "15") {
          durationPrice = Number(price) / 4;
          this.questionArr[i].answer = total + durationPrice;
        } else if (mulShiftDuration[1] == "30") {
          durationPrice = Number(price) / 3;
          this.questionArr[i].answer = total + durationPrice;
        } else if (mulShiftDuration[1] == "45") {
          durationPrice = Number(price) / 2;
          this.questionArr[i].answer = total + durationPrice;
        } else {
          this.questionArr[i].answer = total;
        }
        // let total = (Number(price) *  mulShiftDuration[0]) + ()
      }
      // console.log(this.questionArr[i])
    } else {
      this.questionArr[i].checked = false;
      this.questionArr[i].answer = "";
      this.questionArr[i].answers = "";
      this.grandTotal = 0;
    }
    // if (event.target.value.length > 0) {
    //   this.questionArr[i].answer = event.target.value;
    //   this.questionArr[i].checked = true;
    //   // this.bidPrice = event.target.value;
    // } else {
    //   this.questionArr[i].checked = false;
    //   // this.bidPrice = "";
    // }
  }

  startEndTime: any;
  startEndDate: any;
  errMsgs: any;
  selectDays = [];
  bookingShift = [];
  listday = [];
  noShifts: any;
  shiftDuration: any;
  grandTotal: number;

  public shiftDateList = [
    { day: "This Week", days: "0", checked: false, startDate: "", endDate: "" },
    {
      day: "This Month",
      days: "1",
      checked: false,
      startDate: "",
      endDate: ""
    },
    {
      day: "Select Date",
      days: "2",
      checked: false,
      startDate: "",
      endDate: ""
    }
  ];

  dateSelect(i) {
    this.errMsgs = false;
    this.selectDays.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList[i].checked = true;
    this.startEndTime = this.shiftDateList[i].days == "2" ? true : false;
    let today = moment().format("YYYY-MM-DD");
    let endOfWeek = moment()
      .endOf("week")
      .toDate();
    let endOfMonth = moment()
      .endOf("month")
      .toDate();
    if (this.shiftDateList[i].days == "0") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfWeek).format("YYYY-MM-DD");
    } else if (this.shiftDateList[i].days == "1") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfMonth).format("YYYY-MM-DD");
    } else {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = "";
    }
    let start = moment(
      this.shiftDateList[i].startDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    let end = moment(
      this.shiftDateList[i].endDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    this.startEndDate = start + " - " + end;
  }

  public shiftDaysList = [
    // { day: "All days of the week", days: "0", checked: false },
    // { day: "All weekdays", days: "1", checked: false },
    // { day: "All weekends", days: "2", checked: false },
    { day: "Select All Days", days: "0", checked: false }
  ];

  daysShift(i) {
    this.errMsgs = false;
    this.shiftDaysList[i].checked = true;
    this.selectDays.forEach(x => {
      x.checked = true;
    });
  }

  public daysList = [
    { day: "Sunday", checked: false },
    { day: "Monday", checked: false },
    { day: "Tuesday", checked: false },
    { day: "Wednesday", checked: false },
    { day: "Thursday", checked: false },
    { day: "Friday", checked: false },
    { day: "Saturday", checked: false }
  ];

  daySelect(i) {
    this.errMsgs = false;
    this.shiftDaysList[0].checked = false;
    if (!this.selectDays[i].checked) {
      this.selectDays[i].checked = true;
    } else {
      this.selectDays[i].checked = false;
    }
  }

  multipleShift(i: number) {
    this.errMsgs = false;
    var stepActive = false;
    if (i != -1) {
      var index = -1;
      if (this.shiftTime == 0) {
        this.selectDays = [];
        index = this.shiftDateList.findIndex(y => y.checked == true);
        if (index > -1) {
          let start = moment(this.shiftDateList[index].startDate, "YYYY-MM-DD");
          let end = moment(this.shiftDateList[index].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          if (this.shiftDateList[index].endDate && days > 0) {
            stepActive = true;
            if (days < 6) {
              for (let i = 0; i <= days; i++) {
                let add = moment()
                  .add(i, "days")
                  .format("dddd");
                let daysIndex = this.daysList.findIndex(m => m.day == add);
                this.selectDays.push(this.daysList[daysIndex]);
              }
            } else {
              this.selectDays = this.daysList;
            }
            // this.selectDays.forEach(x => {
            //   x.checked = false;
            // })
            // console.log(this.selectDays)
          } else {
            stepActive = false;
            this.errMsgs = "Select a end date which greater than start date";
            return false;
          }
        } else {
          stepActive = false;
        }
      } else if (this.shiftTime == 1) {
        index = this.selectDays.findIndex(k => k.checked == true);
        this.listday = [];
        var addday = [];
        this.bookingShift = [];
        if (index > -1) {
          var indexS = this.shiftDateList.findIndex(y => y.checked == true);
          let start = moment(
            this.shiftDateList[indexS].startDate,
            "YYYY-MM-DD"
          );
          let end = moment(this.shiftDateList[indexS].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          this.selectDays.forEach(m => {
            if (m.checked == true) {
              this.listday.push(m.day);
            }
          });
          for (let k = 0; k <= days; k++) {
            let add = moment()
              .add(k, "days")
              .format("dddd");
            addday.push(add);
          }
          for (var j = 0; j < this.listday.length; j++) {
            for (var k = 0; k < addday.length; k++) {
              if (this.listday[j] == addday[k]) {
                this.bookingShift.push(addday[k]);
              }
            }
          }
          // console.log(this.bookingShift)
          if (this.bookingShift.length > 1) {
            this.noShifts = this.bookingShift.length;
            stepActive = true;
          } else {
            stepActive = false;
            this.errMsgs =
              "Minimum 2 shifts required for multiple shift booking.";
            return false;
          }
        } else {
          stepActive = false;
          this.errMsgs = "Select an days";
          return false;
        }
      } else {
        var time = $(".timePick").val();
        var scheduleTime = $(".timeduration").val();
        var Time = moment().format("hh:mm:ss a");
        var startTime = moment(Time, "hh:mm:ss a");
        var endTime = moment(time, "hh:mm:ss a");
        let times = endTime.diff(startTime, "minutes");

        if (times <= 59) {
          stepActive = false;
          this.errMsgs = "Select start time minimum 1 hour from current time.";
          return false;
        }

        if (time && scheduleTime) {
          let times = scheduleTime.split(":");
          let scheduleTimes = times[0] == "00" ? 1 : times[0];
          // console.log(scheduleTimes)
          this.shiftDuration = scheduleTimes + " hr :" + times[1];
          this.bidShift = false;
          stepActive = true;
          this.changeStep(1);
        } else {
          stepActive = false;
          this.errMsgs = "Select time and duration";
          return false;
        }
      }
    } else {
      stepActive = true;
      if (this.shiftTime == 0) {
        this.asapSchedule = 0;
        this.bidShift = false;
      }
    }
    // console.log(i, index, stepActive)
    if (stepActive) {
      this.shiftTime = Math.min(2, Math.max(0, this.shiftTime + i));
    } else {
      this.errMsgs = "Select an option to continue";
    }
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val, i) {
    $(".timePick").val("");
    $(".timeduration").val("");
    this.bidShift = val == 2 ? true : false;
    this.errMsg = false;
    // this.otherDate = false;
    // if (val == 0) {
    //   this.asapSchedule = false;
    //   this.sendDate.forEach(x => {
    //     x.active = false;
    //   });
    //   this.questionArr[i].checked = true;
    //   this.questionArr[i].answer = "Now";
    // } else {
    //   this.asapSchedule = true;
    //   this.sendDate[0].active = true;
    //   // console.log(this.sendDate[0].unformat);
    //   let date = this.sendDate[0].unformat;
    //   this.bookDates = moment(date).format('dddd,D MMM Y');
    //   this.questionArr[i].checked = true;
    //   this.questionArr[i].answer = "Schedule";
    // }
    this.questionArr[i].checked = true;
    this.errMsgs = false;
    this.shiftTime = 0;
    this.counts = 1;
    this.otherDate = false;
    if (val == 0) {
      this.questionArr[i].answer = "Now";
      this.asapSchedule = 0;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else if (val == 1) {
      this.questionArr[i].answer = "Schedule";
      this.asapSchedule = 1;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    } else {
      this.questionArr[i].answer = "Repeat";
      this.asapSchedule = 2;
    }
  }

  dateChange() {
    this.errMsgs = false;
    this.errMsg = false;
    var day = [];
    var time = "";
    var scheduleTime = "";
    if (this.asapSchedule == 1) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        this.bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        this.bookDate = this.bookDates;
      }
    } else if (this.asapSchedule == 0) {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment()
        .add(bookDate, "days")
        .format("dddd,D MMM Y");
    } else {
      this.bookingType = 3;
      this.bookTitle = "Booking Shift";
      var endDTime;
      var bookingShift = this.bookingShift.length;
      day = this.listday;
      this.shiftDateList.forEach(x => {
        if (x.checked == true) {
          this.bookDate = moment(x.startDate, "YYYY-MM-DD").format(
            "dddd,D MMM Y"
          );
          endDTime = moment(x.endDate, "YYYY-MM-DD").format("dddd,D MMM Y");
        }
      });
    }

    time = $(".timePick").val();
    scheduleTime = $(".timeduration").val();
    this.scheduleDateTimeStamp = moment(
      this.bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    this.endDTime = moment(endDTime + " 23:00", "dddd,D MMM Y hh:mm").unix();
    let scheduleTimes = scheduleTime.split(":");
    let schTime = scheduleTimes[0] == "00" ? 1 : scheduleTimes[0];
    this.scheduleTime =
      scheduleTimes[0] == "00"
        ? 0 + Number(scheduleTimes[1])
        : Number(scheduleTimes[0]) * 60 + Number(scheduleTimes[1]);
    let shiftDuration = scheduleTimes[0] + " hr :" + scheduleTimes[1];
    // console.log(scheduleTime, this.scheduleTime, time)
    let list = {
      bookTitle: this.bookTitle,
      bookingType: this.bookingType,
      callType: this.typeCall,
      bookDate: this.bookDate,
      endDate: endDTime,
      days: day,
      bookTime: time,
      bookCount: this.scheduleTime,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      startTimeStamp: this.scheduleDateTimeStamp,
      endTimeStamp: this.endDTime,
      scheduleTime: this.scheduleTime || 60,
      shiftDuration: shiftDuration,
      bookingShift: bookingShift || ""
    };
    // console.log(list)
    // let times = moment().add(1, 'hour').format('hh:mm a');
    var Time = moment().format("hh:mm:ss a");
    var startTime = moment(Time, "hh:mm:ss a");
    var endTime = moment(time, "hh:mm:ss a");
    let times = endTime.diff(startTime, "minutes");
    // if (times <= 59) {
    //   this.errMsg = "Select start time minimum 1 hour from current time.";
    //   return false;
    // }
    if (this.asapSchedule == 0 || (time && scheduleTime)) {
      this._conf.setItem("bookingDates", JSON.stringify(list));
      this.notaryService();
    } else {
      this.errMsg = "Select time and duration";
    }
  }

  changeStep(i: number) {
    this.errMsgs = false;
    this.shiftTime = 0;
    // this.asapSchedule = 0;
    let per = Math.round(100 / this.questionArr.length);
    let re = 100 - per * this.questionArr.length;
    let len = this.questionArr.length - 1;
    let mandatory = this.questionArr[this.count].isManadatory;
    let checked = this.questionArr[this.count].checked;
    if (
      (mandatory == 1 && checked && this.cardWallets) ||
      (mandatory == 0 || i == -1)
    ) {
      this.errMsg = false;
      if (
        this.questionArr &&
        this.count == this.questionArr.length - 1 &&
        i == 1
      ) {
        this.biddingAll();
        // console.log("yes");
      } else {
        var time = $(".timePick").val();
        var scheduleTime = $(".timeduration").val();
        if (this.asapSchedule == 1 && (!time || !scheduleTime)) {
          this.errMsgs = "Select time and duration";
          return false;
        }

        this.count = Math.min(len, Math.max(0, this.count + i));
        // console.log(this.questionArr[this.count].questionType)
        if (
          this.questionArr[this.count].questionType == 2 &&
          this.asapSchedule == 2 &&
          this.shiftTime != 2
        ) {
          this.bidShift = true;
        } else {
          this.bidShift = false;
        }
        this.questionArr.forEach(x => {
          x.active = false;
        });
        this.questionArr[this.count].active = true;
        this.widthBar = this.count * per + re;
        if (
          this.questionArr[this.count].questionType == 0 &&
          this.asapSchedule == 2
        ) {
          let change = i == 1 ? 1 : -1;
          this.changeStep(change);
        }
      }
    } else {
      if (!this.cardWallets) {
        $("#cardWallets").modal("show");
      } else {
        let type = this.questionArr[this.count].questionType;
        switch (type) {
          case 0:
            this.errMsg = "Select an option to continue";
            break;
          case 1:
            this.errMsg = "Please enter the price";
            break;
          case 2:
            this.errMsg = "Please select type of booking";
            break;
          case 3:
            this.errMsg = "Please enter the job location";
            break;
          case 4:
            this.errMsg = "Please select type of payment";
            break;
          case 5:
            this.errMsg = "Please enter the  requirement";
            break;
          case 6:
            this.errMsg = "Select an option to continue";
            break;
          case 7:
            this.errMsg = "Select an option to continue";
            break;
          case 8:
            this.errMsg = "Please select the date";
            break;
          case 9:
            this.errMsg = "Please select the date";
            break;
          case 10:
            this.errMsg = "Please upload the images";
        }
      }
    }
    // console.log(this.questionArr[this.count])
  }

  skipBid(i) {
    if (this.questionArr[i].preDefined) {
      this.questionArr[i].preDefined.forEach(x => {
        x.checked = false;
      });
    } else {
      this.questionArr[i].answer = "";
      if (this.questionArr[i].questionType == 0) {
        this.couponCode = "";
      }
      if (this.questionArr[i].questionType == 10) {
        this.questionArr[i].answer = [];
      }
    }
    this.changeStep(1);
  }

  biddingAll() {
    this.PaymentLoader = true;
    this.questionAndAnswer = [];
    this.questionArr.forEach(x => {
      if (x.checked == true) {
        let list = {
          _id: x._id,
          name: x.questionName,
          answer: x.answer,
          questionType: x.questionType
        };
        if (x.questionType == 10 || x.questionType == 7) {
          console.log(x.answer);
          // console.log(x.answer.toString())
          list.answer = x.answer.toString();
        }
        if (x.questionType == 1) {
          this.bidPrice = x.answers;
          // console.log(x, this.bidPrice);
        }
        this.questionAndAnswer.push(list);
      }
    });
    this._conf.setItem(
      "questionAndAnswer",
      JSON.stringify(this.questionAndAnswer)
    );
    if (this.questionAndAnswer[0].answer == "In - Call") {
      this.questionAndAnswer.splice(0, 1);     
      this.notaryService();
    } else {
      this.dateChange();
    }
  }


  notaryService() {
    const cityAddress = this.biddingList.cityName;
    const catName = this.biddingList.cat_name.replace(/[^A-Z0-9]/gi, "_");    
    if (this.biddingList.service_type == 1) {
      this._router.navigate(["./", cityAddress, catName, this.catId, "0"]);
    } else {
      this._router.navigate(["./", cityAddress, catName, this.biddingList._id]);
    }
  }

  addressSelect(i, j) {
    // console.log(this.savedAddress[i]);
    this.savedAddress.forEach(x => {
      x.checked = false;
    });
    this.savedAddress[i].checked = true;
    this.errMsg = false;
    $("#bidFlow").val("");
    $(".locateLoader").hide();
    this.questionArr[j].checked = true;
    this.questionArr[j].answer = this.savedAddress[i].addLine1;
    this.cityAddress = this.savedAddress[i].addLine1;
    this.latlng = {
      lat: this.savedAddress[i].latitude,
      lng: this.savedAddress[i].longitude
    };
  }
  typeCall: any;
  radioSelect(i, j) {
    this.errMsg = false;
    this.questionArr[i].preDefined.forEach(x => {
      x.checked = false;
    });
    this.questionArr[i].preDefined[j].checked = true;
    this.questionArr[i].checked = true;
    this.questionArr[i].answer = this.questionArr[i].preDefined[j].name;
    this.questionArr.forEach(x => {
      if (x.questionType == 2) {
        this.scheduleList = x;
      }
    });
    const index = this.questionArr.findIndex(y => y.questionType == 2);
    if (index > -1) {
      this.questionArr.splice(index, 1);
    }
    this.typeCall = 1;
    if (this.questionArr[0].answer == "Out - Call") {
      this.typeCall = 2;
      this.questionArr = [...this.questionArr, this.scheduleList];
    }
  }

  checkBoxSelect(event, i, j){
    this.errMsg = false;
    // console.log(event)
    if(!this.questionArr[i].answer){
      this.questionArr[i].answer = [];
    }
    const index = this.questionArr[i].answer.findIndex(x=>x == this.questionArr[i].preDefined[j].name);
    if(index > -1){
      this.questionArr[i].answer.splice(index, 1);
      this.questionArr[i].preDefined[j].checked = false;
    }  
    if(event.target.checked){
      this.questionArr[i].preDefined[j].checked = true;
      this.questionArr[i].checked = true;
      this.questionArr[i].answer.push(this.questionArr[i].preDefined[j].name);
    }
    this.questionArr[i].checked = this.questionArr[i].answer.length == 0?false:true;
    // console.log(this.questionArr[i])
  }

  textList(event, i) {
    this.errMsg = false;
    if (event.target.value.length > 0) {
      this.questionArr[i].answer = event.target.value;
      this.questionArr[i].checked = true;
      // this.bidPrice = event.target.value;
      this.questionArr[i].answers = event.target.value;
      this.questionArr[i].answer = event.target.value;
    } else {
      this.questionArr[i].checked = false;
      this.questionArr[i].answer = "";
      this.questionArr[i].answers = "";
    }
  }

  datePicker() {
    flatpickr(".BtypePickers", {
      altInput: true,
      // enableTime: true,
      allowInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: "today",
      // minTime: moment().format('HH:MM'),
      onChange: x => {
        // let date = $(".BtypePicker").val();
        // // console.log(x, date)
        // this.questionArr[i].answer = date;
        // this.questionArr[i].checked = true;
      }
    });
  }

  /** repeat booking **/
  startdatePicker() {
    this.startpicker = flatpickr(".repeatStartTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: moment().format("YYYY-MM-DD"),
      allowInput: false,
      enableTime: true,
      minDate: "today",
      // maxDate: $('.repeatEndTime').attr('value'),
      // onClose: (selectedDates, dateStr, instance) => {
      //   this.endpicker.setDate('minDate', moment(dateStr).add(1, 'days').format('YYYY-MM-DD'));
      // },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatStartTime").val(datestr);
        this.shiftDateList[2].startDate = moment(datestr).format("YYYY-MM-DD");
      }
    });
  }

  enddatePicker() {
    console.log($(".repeatStartTime").attr("value"));
    this.endpicker = flatpickr(".repeatEndTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: "Z",
      minDate: $(".repeatStartTime").attr("value"),
      allowInput: false,
      // onClose: (selectedDates, dateStr, instance) => {
      //   this.startpicker.setDate('maxDate', dateStr);
      // },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatEndTime").val(datestr);
        this.shiftDateList[2].endDate = moment(datestr).format("YYYY-MM-DD");
      }
    });
  }

  datePickerPast(event, i) {
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);

    flatpickr("#" + value, {
      altInput: true,
      // enableTime: true,
      allowInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      maxDate: "today",
      // minTime: moment().format('HH:MM'),
      onChange: x => {
        let date = $("#" + value).val();
        // console.log(x, date)
        this.questionArr[i].answer = date;
        this.questionArr[i].checked = true;
      }
    });
  }

  datePickerFuture(event, i) {
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    flatpickr("#" + value, {
      altInput: true,
      // enableTime: true,
      allowInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: "today",
      // minTime: moment().format('HH:MM'),
      onChange: x => {
        let date = $("#" + value).val();
        // console.log(x, date)
        this.questionArr[i].answer = date;
        this.questionArr[i].checked = true;
      }
    });
  }

  UploadFile(e, i) {
    var target = e.target || e.srcElement || e.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    console.log(e, value);
    this.errMsg = false;
    setTimeout(() => {
      this.questionArr[i].loader = false;
    }, 5000);
    var bucket = new AWS.S3({ params: { Bucket: "heonapp" } });
    var fileChooser = e.srcElement;
    var file = fileChooser.files[0];
    let date = moment().format("DYMhmsA");
    if (file) {
      this.questionArr[i].loader = true;
      var params = {
        Key: "keymaing" + date,
        ContentType: file.type,
        Body: file
      };
      bucket.upload(params, (err, data) => {
        // this._zone.run(() => {
        console.log(data);
        // $("#" + value).siblings(".imgBid").attr("src", data.Location);
        this.questionArr[i].answer.push(data.Location);
        this.questionArr[i].checked = true;
        this.questionArr[i].loader = false;
        this.errMsg = false;
        // console.log(this.questionArr[i].answer)
        // });
      });
    }
    return false;
  }

  removeImg(i, j) {
    this.questionArr[i].answer.splice(j, 1);
  }

  paymentNavMenus(id: number, i, val) {
    this.errMsg = false;
    $(".allSmallCheckout").removeClass("active");
    $("#" + id + id + "payNavMenu").addClass("active");
    this.paymentMethod = id;
    this.paidByWallet = 0;
    this.questionArr[i].answer = val;
    this.questionArr[i].checked = true;
    this.cardWallets = id == 3 ? false : true;
  }

  cardToggle(val) {
    $("body").toggleClass("hideHidden");
    this.newCardToggle = !this.newCardToggle;
    if (val == 1) {
      this.cardWallet = true;
    } else {
      if (this.cardId) {
        this.cardWallet = false;
      } else {
        this.errorSaveCart = "Please add a card";
        this.cardWallet = true;
      }
    }
  }

  valdationCreditCard(value: any) {
    $(".activeGetCart").removeClass("active");
    this.errorSaveCart = false;
    var visaCards = creditCardType(value);
    var val = value.split(" ").join("");
    if (val.length > 0) {
      val = val.match(new RegExp(".{1,4}", "g")).join(" ");
    }
    this.cardCredit = val;
    if (value.length <= 17) {
      this.validCD = "The card number should be 15 or 16 digits!";
      // this.stripeCardDetails = true;
    } else {
      this.validCD = false;
      // this.stripeCardDetails = false;
    }

    if (value.length <= 4) {
      if (visaCards != "") {
        // console.log(visaCards[0].type);
        this.cardBrand = visaCards[0].type;
        this.validCD = false;
        this.stripeCardDetails = true;
      } else {
        this.validCD = "invalid card number!";
        this.stripeCardDetails = false;
        this.cardCredit = "";
        console.log("error cardtype");
      }
    }
  }

  valdationDate(val: any) {
    this.errorSaveCart = false;
    this.monthYear = val.replace(/(\d{2}(?!\s))/g, "$1/");

    if (isNaN(val)) {
      val = val.replace(/[^0-9\/]/g, "");
      if (val.split("/").length > 2) val = val.replace(/\/+$/, "");
      this.monthYear = val;
    }

    var validDm = /^(0[1-9]|1[0-2])\/\d{4}$/.test(val);
    // console.log(val);
    if (val.length > 2 && validDm == false) {
      this.validDM = true;
      this.stripeCardDetails = false;
    } else {
      this.validDM = false;
      this.stripeCardDetails = true;
    }

    if (val.length == 1) {
      if (val.charAt(0) == 0 || val.charAt(0) == 1) {
        this.monthYear = val;
      } else {
        this.monthYear = "0" + val;
      }
    } else if (val.length == 2) {
      if (val <= 12) {
        this.monthYear = val;
      } else {
        this.monthYear = "";
      }
    }
  }

  valdationcvv(val: any) {
    this.errorSaveCart = false;
    if (val.length == 3) {
      this.stripeCardDetails = true;
    } else {
      this.stripeCardDetails = false;
    }
  }

  stripeCard() {
    // var val = String(Number(this.monthYear));
    var validDateMnth = /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.monthYear);
    // console.log(this.monthYear, validDateMnth);
    if (
      this.cardCredit &&
      this.monthYear &&
      this.cardcvv &&
      validDateMnth == true
    ) {
      this.validDM = false;
      this.sendCardStripToken();
    } else {
      this.validDM = true;
      console.log("error stripeCartList");
    }
  }

  sendCardStripToken() {
    this.PaymentLoader = true;
    setTimeout(() => {
      this.PaymentLoader = false;
    }, 5000);
    var MYdetails = this.monthYear.split("/");
    var numberCard = this.cardCredit.split(" ").join("");
    (<any>window).Stripe.card.createToken(
      {
        number: this.cardCredit,
        exp_month: Number(MYdetails[0]),
        exp_year: Number(MYdetails[1]),
        cvc: this.cardcvv
      },
      (status: number, response: any) => {
        this._zone.run(() => {
          if (status === 200) {
            this.errorSaveCart = false;
            this.stripeToken = response.id;
            this.sendCardStrip();
            console.log(response.id);
          } else {
            this.errorSaveCart = response.error.message;
            setTimeout(() => {
              this.errorSaveCart = false;
            }, 5000);
            console.log(response.error.message);
          }
        });
      }
    );
  }

  sendCardStrip() {
    var email = this._conf.getItem("email");
    let stripeCartList = {
      cardToken: this.stripeToken,
      email: email
    };
    this._serviceCheckout.paymentStripeCart(stripeCartList).subscribe(
      (res: any) => {
        this.PaymentLoader = false;
        console.log("stripeRes", res);
        $(".modal").modal("hide");
        this.sendGetCart();
        this.resetSaveCart();
        this.cardToggle(1);
      },
      err => {
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  sendGetCart() {
    this._serviceCheckout.paymentGetCart().subscribe(
      (res: any) => {
        console.log("paymentget-card", res);
        this.payGetCartList = res.data;
        if (this.payGetCartList && this.payGetCartList.length > 0) {
          this.payGetCartList.forEach(x => {
            x.checked = false;
          });
          this.payGetCartList[0].checked = true;
          this.cardId = this.payGetCartList[0].id;
        }
      },
      err => {
        console.log("pget", err);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  deleteGetCart(i: number, id: any) {
    this.index = i;
    this.id = id;
    $("#cardDelete").modal("show");
  }

  deleteCard() {
    let deletepayGetCart = {
      cardId: this.id
    };
    this._serviceCheckout.paymentGetCartDelete(deletepayGetCart).subscribe(
      (res: any) => {
        this.sendGetCart();
        this.payGetCartList.splice(this.index, 1);
      },
      err => {
        console.log("pget", err);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  selectPayments(i: number) {
    this.payGetCartList.forEach(x => {
      x.checked = false;
    });
    this.payGetCartList[i].checked = true;
    this.cardId = this.payGetCartList[i].id;
  }

  resetSaveCart() {
    this.cardCredit = "";
    this.monthYear = "";
    this.cardcvv = "";
    this.cardHolder = "";
  }

  getWallet() {
    this._serviceProfile.getwalletMoney().subscribe(
      (res: any) => {
        console.log("customer/wallet", res);
        this.walletMoney = res.data;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  addWallet() {
    let list = {
      cardId: this.cardId,
      amount: this.addMoney
    };
    setTimeout(() => {
      this.errorMsg = false;
      this.PaymentLoader = false;
    }, 5000);
    if (this.addMoney) {
      this.PaymentLoader = true;
      this.errorMsg = false;
      this._serviceProfile.getaddwalletMoney(list).subscribe(
        (res: any) => {
          console.log("wallet/recharge", res);
          this.PaymentLoader = false;
          this.errorMsg = res.message;
          this.getWallet();
          this.addMoney = "";
          this.cardToggle(1);
        },
        err => {
          this.PaymentLoader = false;
          console.log(err.error.message);
          this.errorMsg = err.error.message;
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.errorMsg = "Please enter the amount to continue.";
    }
  }

  walletPayment(val: number) {
    this.paidByWallet = 1;
    this.paymentMethod = val;
    this.cardWallets = true;
  }

  sendGetAddress() {
    this._serviceProfile.getAddress().subscribe(
      (res: any) => {
        console.log("customer/address", res);
        this.savedAddress = res.data;
        this.savedAddress.forEach(x => {
          x.checked = false;
          x.taggedAs = x.taggedAs.toLowerCase();
          if (x.taggedAs == "home") {
            x.taggedType = 1;
          } else if (x.taggedAs == "office" || x.taggedAs == "work") {
            x.taggedType = 2;
          } else {
            x.taggedType = 3;
          }
        });
      },
      err => {
        console.log(err.error);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  deleteAddressList(addressID: any, i: any) {
    $("#deleteAddress").modal("show");
    this.addressID = addressID;
    this.index = i;
  }

  deleteAddress() {
    this._serviceProfile.addressDelete(this.addressID).subscribe(
      (result: any) => {
        this.Others = "";
        this.addressID = "";
        this.savedAddress.splice(this.index, 1);
        $("#deleteAddress").modal("hide");
      },
      err => {
        var error = JSON.parse(err._body);
        // console.log(error)
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  closeOpenSearch() {
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;
  }

  editAddress(index) {
    // console.log(this.savedAddress[index])
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;
    // this.closeOpenSearch();
    this.addressID = this.savedAddress[index]._id;
    let address1 = this.savedAddress[index].addLine1;
    var latAddress = this.savedAddress[index].latitude;
    var lngAddress = this.savedAddress[index].longitude;
    this.selectType(this.savedAddress[index].taggedType);
    this.latlng = {
      lat: latAddress,
      lng: lngAddress
    };
    $("#txtPlaces").val(address1);
    $(".areaName").val(address1);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.continueMap();
  }

  selectType(type: number) {
    this.addressType = type;
    if (type == 1) {
      this.typeOne = !this.typeOne;
      this.typeTwo = false;
      this.typeThree = false;
    } else if (type == 2) {
      this.typeTwo = !this.typeTwo;
      this.typeOne = false;
      this.typeThree = false;
    } else if (type == 3) {
      this.otherText = true;
      this.typeThree = !this.typeThree;
      this.typeTwo = false;
      this.typeOne = false;
    }
  }

  saveAddress() {
    this.loaderList = true;
    var address = $(".areaName").val();
    var lat = $(".arealat").val();
    var lng = $(".arealng").val();
    if (this.addressType == 1) {
      var name = "Home";
    } else if (this.addressType == 2) {
      var name = "Office";
    } else if (this.addressType == 3) {
      var name = "Other";
    }
    let list = {
      addLine1: address,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: lat,
      longitude: lng,
      taggedAs: name,
      userType: 1
    };
    if (this.Others) {
      list.taggedAs = this.Others;
    }
    this._serviceProfile.addressEditSaved(list, this.addressID).subscribe(
      (res: any) => {
        this.addressID = "";
        this.Others = "";
        this.loaderList = true;
        console.log("edit customer/address", res);
        this.sendGetAddress();
        $("body").toggleClass("hideHidden");
        this.searchLocationToggle = false;
      },
      err => {
        this.loaderList = true;
        console.log(err.error);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  applyCoupons() {
    if (this.couponCode) {
      this.errMsg = false;
      let list = {
        latitude: this.latlng.lat,
        longitude: this.latlng.lng,
        cartId: this.catId,
        paymentMethod: this.paymentMethod,
        paidByWallet: this.paidByWallet,
        couponCode: this.couponCode
      };
      setTimeout(() => {
        this.errMsg = false;
        this.loaderApply = false;
      }, 3000);
      this.loaderApply = true;
      this._serviceCheckout.couponList(list).subscribe(
        (res: any) => {
          console.log("coupon", res);
          this.cancelBtn = true;
          this.loaderApply = false;
          this.errMsg = false;
          this.successMsg = res.message;
          // this.couponPrice = parseFloat(res.data.discountAmount).toFixed(2);
          this.deliveryFee = res.data.deliveryFee;
          this.promoId = res.data.promoId;
          this.promoCode = this.couponCode;
          // this.grandTotal = (parseFloat(this.grandTotal) - parseFloat(this.couponPrice)).toFixed(2);
        },
        err => {
          this.cancelBtn = true;
          this.loaderApply = false;
          var error = err.error;
          console.log(err.error);
          this.errMsg = error.message;
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.errMsg = "Enter coupon";
    }
  }

  cancelCoupons() {
    this.couponCode = "";
    this.cancelBtn = false;
    this.couponPrice = "";
    // this.grandTotal = parseFloat(this.cartList.totalAmount).toFixed(2)
  }

  checkoutSubmitCart() {
    this.PaymentLoader = true;
    let datetime = moment().format("YYYY-MM-DD HH:mm:ss");
    let list = {
      bookingModel: 3,
      bookingType: this.bookingType,
      callType: 2,
      paymentMethod: this.paymentMethod,
      paidByWallet: this.paidByWallet,
      placeName: "",
      addLine1: this.cityAddress,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: this.latlng.lat,
      longitude: this.latlng.lng,
      providerId: "",
      categoryId: this.catId,
      cartId: "",
      jobDescription: "",
      promoCode: this.promoCode || "",
      paymentCardId: this.paymentMethod == 1 ? "" : this.cardId,
      bookingDate: "",
      scheduleTime: 0,
      endTimeStamp: 0,
      days: [],
      deviceTime: datetime,
      questionAndAnswer: this.questionAndAnswer,
      bidPrice: Number(this.bidPrice)
    };
    if (this.bookingType == 2) {
      list.bookingDate = String(this.scheduleDateTimeStamp);
      list.scheduleTime = Number(this.scheduleTime);
    } else if (this.bookingType == 3) {
      list.bookingDate = String(this.scheduleDateTimeStamp);
      list.endTimeStamp = Number(this.endDTime);
      list.scheduleTime = Number(this.scheduleTime);
      list.days = this.daysLists;
    } else {
      list.bookingDate = "";
    }
    // console.log(list)
    this._serviceCheckout.submitCheckout(list).subscribe(
      (res: any) => {
        $(".modal").modal("hide");
        $("#orderSubmit").modal("show");
        this.PaymentLoader = false;
        this.orderSuccess = res.message;
        toastr["success"](
          "Yay !We will let you know when the kyhootpro accepts your booking!",
          "Heon"
        );
      },
      err => {
        this.PaymentLoader = false;
        this.errMsg = err.error.message;
        console.log(err.error.message);
        setTimeout(() => {
          this.errMsg = false;
        }, 5000);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  profileList() {
    this._router.navigate(["./profile"]);
  }

  closeLocation(j) {
    // $("#locatFill").val("");
    $("#bidFlow").val("");
    this.savedAddress.forEach(x => {
      x.checked = false;
    });
    this.errMsg = false;
    this.questionArr[j].checked = false;
  }

  currentLocation(k) {
    $(".locateLoader").show();
    setTimeout(() => {
      $(".locateLoader").hide();
      var loc = $("#bidFlow").val();
      if (!loc) {
        this.errMsg = "Please enter the job location";
      }
    }, 3000);
    this.errMsg = false;
    this.savedAddress.forEach(x => {
      x.checked = false;
    });
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        this.latlng = {
          lat: lat,
          lng: lng
        };
        console.log("current", this.latlng);
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = (geocoder = new google.maps.Geocoder());
        geocoder.geocode({ latLng: latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (
                var i = 0;
                i < results[1].address_components.length;
                i += 1
              ) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === "locality") {
                    var City = addressObj.long_name;
                    // console.log(addressObj.long_name);
                    // this.cityAddress = City;
                  }
                  if (addressObj.types[j] === "administrative_area_level_1") {
                    var state = addressObj.short_name;
                    // console.log(addressObj.short_name);
                  }
                }
              }
              // $("#locatFill").val(results[0].formatted_address);
              $("#bidFlow").val(results[0].formatted_address);
              this.questionArr[k].checked = true;
              this.questionArr[k].answer = results[0].formatted_address;
              $("#latB").val(lat);
              $("#lngB").val(lng);
              this.cityAddress = results[0].formatted_address;
              $(".locateLoader").hide();
            }
          }
        });
      }, this.geoError);
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    console.log("Geolocation is not supported by this browser.");
  }

  locationSearch(k) {
    this.errMsg = false;
    this.savedAddress.forEach(x => {
      x.checked = false;
    });
    var input = document.getElementById("bidFlow");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      let placelatlng = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };
      $("#latB").val(place.geometry.location.lat());
      $("#lngB").val(place.geometry.location.lng());
      // this._conf.setItem('latlng', JSON.stringify(placelatlng));
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === "locality") {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
            // this.cityAddress = City;
          }
          if (addressObj.types[j] === "administrative_area_level_1") {
            var state = addressObj.short_name;
          }
        }
      }
      this.questionArr[k].checked = true;
      this.questionArr[k].answer = $("#bidFlow").val();
      this.cityAddress = $("#bidFlow").val();
    });
  }

  changeLocation() {
    var input = document.getElementById("txtPlaces");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === "locality") {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === "administrative_area_level_1") {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
        }
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      this.latlng = {
        lat: lat,
        lng: lng
      };
      $(".areaName").val($("#txtPlaces").val());
      $(".arealat").val(lat);
      $(".arealng").val(lng);
    });
  }

  locationChange() {
    setTimeout(() => {
      this.continueMap();
    }, 500);
  }

  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById("mapData"), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: true,
      mapTypeControl: true
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = (geocoder = new google.maps.Geocoder());
    geocoder.geocode({ latLng: latlng }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // $(".addressArea").val(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });

    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, "dragend", () => {
      geocoder.geocode({ latLng: marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            $(".addressArea").val(results[0].formatted_address);
            $(".areaName").val(results[0].formatted_address);
            $(".arealat").val(marker.getPosition().lat());
            $(".arealng").val(marker.getPosition().lng());
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
          }
        }
      });
    });
  }
}
