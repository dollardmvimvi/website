import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { CalltypeComponent } from './calltype.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: CalltypeComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    CalltypeComponent
  ]
})
export class CalltypeModule { }
