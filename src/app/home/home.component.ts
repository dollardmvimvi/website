import { Component, OnInit } from "@angular/core";
import { HomeService } from "./home.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgxCarousel } from "ngx-carousel";
import { MissionService } from "../app.service";
import { ProfileService } from "../profile/profile.service";
import { Configuration } from "../app.constant";

import { Observable } from "rxjs";
import { fromEvent } from "rxjs";
import { map, filter, debounceTime, tap, switchAll } from "rxjs/operators";
import { ThrowStmt } from "@angular/compiler";

declare var $: any;
declare var moment: any;
declare var google: any;
declare var flatpickr: any;

@Component({
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    private _service: HomeService,
    private _missionService: MissionService,
    private _router: Router,
    private _conf: Configuration,
    private _serviceProfile: ProfileService,
    private activateRoute: ActivatedRoute
  ) {
    _missionService.catSearchPopOpen$.subscribe(val => {
      // if (val == true) {
      //   this.getCity();
      // } else {
      this.typeBooking(val);
      // }
    });
  }

  public carouselTileItems: Array<any>;
  public carouselTileRecommend: Array<any>;
  public ratingItems: Array<any>;

  public carouselTile: NgxCarousel;
  public carouselRecommend: NgxCarousel;
  public carouselReview: NgxCarousel;

  trendingList: any;
  citys: any;
  dataList: any;
  cityName: any;
  catSearch: any;
  searchCate: string;
  cityId: any;
  loaderList = false;
  loaderLists = false;
  catLoaderList = true;
  shimmer: any = [];
  categoryToggle: any;
  catAll: any;

  bookingType: any;
  catName: any;
  catId: any;
  cityAddress: any;
  locAddress: any;
  ipAddress: any;
  latlng: any;
  ipList: any;

  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = 0;
  shiftTime = 0;
  locationLists = false;
  bookingList = false;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  startDTime: any;
  endDTime: any;
  bookDates: any;

  bInvoice: any;
  bId: any;
  reviewMsg: any;
  ratingList: any = [];
  reviewSuccess: any;
  reviewErr: any;
  popupCart = false;

  defaultImage =
    "https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/serviceEmpty.jpg";
  defaultTrending =
    "https://d2qb2fbt5wuzik.cloudfront.net/iserve_goTasker/images/Trbusser.svg";
  offset = 100;

  cLocation: any;
  index: any;

  ngOnInit() {
    this._missionService.scrollTops(false);
    flatpickr(".BtypePicker", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: moment()
        .add(9, "days")
        .format("YYYY-MM-DD"),
      allowInput: false
    });

    /** repeat booking **/
    var startpicker = flatpickr(".repeatStartTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: moment().format("YYYY-MM-DD"),
      allowInput: false,
      enableTime: true,
      minDate: "today",
      maxDate: $(".repeatEndTime").attr("value"),
      onClose: (selectedDates, dateStr, instance) => {
        console.log(dateStr);
        // endpicker.set('minDate', moment(dateStr).add(1, 'days').format('YYYY-MM-DD'));
      },
      onChange: (dateobj, datestr) => {
        this.errMsg = false;
        let date = $(".repeatStartTime").val(datestr);
        this.shiftDateList[2].startDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    var endpicker = flatpickr(".repeatEndTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: "Z",
      minDate: $(".repeatStartTime").attr("value"),
      allowInput: false,
      onClose: (selectedDates, dateStr, instance) => {
        // startpicker.set('maxDate', dateStr);
      },
      onChange: (dateobj, datestr) => {
        this.errMsg = false;
        let date = $(".repeatEndTime").val(datestr);
        this.shiftDateList[2].endDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    this._missionService.scrollTop(false);
    this._conf.removeItem("backClicked");    
    let urlPath = this._conf.getItem("pathname");
    // console.log("testss", urlPath)
    if (urlPath && urlPath != "/") {
      // let list = urlPath.split("/");
      // var index = list.indexOf("website");
      // if (index > -1) {
      //   list.splice(index, 1);
      // }
      // console.log(list.join('/'));
      // let url = list.join('/');
      this._router.navigate([urlPath]);
      this._conf.removeItem("pathname");
    } else {
      this._conf.removeItem("inCall");
      this._conf.removeItem("questionAndAnswer");
      this._conf.removeItem("bookingDates");
      this.cityName = this._conf.getItem("city") || "Select City";     
      $("#cityNames").val(this.cityName);
      this.placeHolder();
      this.getIp();

      this.getDate();
      this.getTimes();
      let review = this._conf.getItem('review') ; 
      !review ? this.reviewPending() :'';
      // this.activateRoute.queryParams.subscribe((res)=>{
      //   !res['review'] ?  this.reviewPending() :'';
      // });
    }
    this.loaderList = false;
    setTimeout(() => {
      this.loaderList = true;
      this.cityName = $("#cityNames").val() || "Select City";      
      this.getCity();
    }, 1000);

    // setTimeout(() => {
    //   if (this.catLoaderList) {
    //     this._missionService.scrollTops(false);
    //   }
    // }, 6000);
    this.selectLocate();

    this.carouselTile = {
      grid: { xs: 2.5, sm: 2.5, md: 3.5, lg: 4.5, all: 0 },
      slide: 4,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };

    this.carouselRecommend = {
      grid: { xs: 1.5, sm: 2.5, md: 3.5, lg: 4.5, all: 0 },
      slide: 1,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };

    this.carouselReview = {
      grid: { xs: 1, sm: 2, md: 3, lg: 3, all: 0 },
      slide: 1,
      speed: 400,
      // animation: 'lazy',
      point: {
        visible: false
      },
      load: 1,
      touch: true
      // easing: 'ease'
    };
  }

  public carouselTileLoad(evt: any) {
    const len = this.carouselTileItems.length;
    for (let i = len; i < len; i++) {
      this.carouselTileItems.push(i);
    }
  }

  public carouselTileReview(evt: any) {
    const len = this.ratingItems.length;
    for (let i = len; i < len; i++) {
      this.ratingItems.push(i);
    }
  }

  public carouselRecommendLoad(evt: any) {
    const len = this.carouselTileRecommend.length;
    for (let i = len; i < len; i++) {
      this.carouselTileRecommend.push(i);
    }
  }

  ngOnDestroy(): void {
    this._missionService.scrollTop(true);
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  TimePicker(event) {
    this.errMsg = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    // var nexthour = moment()
    //   .add(1, "hour")
    //   .add(1, "minutes")
    //   .format("hh a");
    var nexthour = moment(new Date(),'hh:mm a')
    .add(60,'minutes')
    .format("hh:mm a");
      console.log("next hour", nexthour)
    $(".timePick").val(nexthour);
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      altInput: true,
      dateFormat: "h:i K",
      altFormat: "h:i K",
      defaultDate: nexthour,
      minDate: nexthour,
      // minTime: nexthour,
      time_24hr: true,
      minuteIncrement: 60,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $(".timePick").val(datestr);
      }
    });
  }

  TimeDuration(event) {
    this.errMsg = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    $(".timeduration").val("01:00");
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      dateFormat: "H",
      altFormat: "H",
      defaultDate: "01:00",
      minDate: "01:00",
      // minTime: nexthour,
      time_24hr: true,
      altInput: true,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        let Time = $("#" + value).val();
        $(".timeduration").val(datestr);
      }
    });
  }

  selectLocate() {
    let no = this._conf.getItem("notaryTask");
    let latlng = this._conf.getItem("latlng");
    let cityName = this._conf.getItem("city");
    if (!latlng && no != "1" && !cityName) {
      this.cLocation = true;
      $("#notaryTask").modal("show");
    }
  }

  submitLocate() {
    var loc = $("#locatFill").val();
    this.cityName = this._conf.getItem("city");
    if (this.index >= 0) {
      this.listCat(this.index);
      $(".modal").modal("hide");
    } else {
      if (loc || this.cityName) {
        this.cLocation = false;
        this.getCity();
        $(".modal").modal("hide");
        this._conf.setItem("place", JSON.stringify(loc));
      } else {
        this.locAddress = "";
      }
    }
  }

  getIp() {
    this._service.getIpAddress().subscribe((res: any) => {
      // let ipList = res.loc.split(",");
      this.ipList = {
        lat: res.latitude,
        lng: res.longitude
      };
      this.ipAddress = res.ip;
      // this.getCity();
      /** lat lng get ip**/
      this._conf.setItem("ip", res.ip);
      let latlng = this._conf.getItem("latlng");
      if (!latlng) {
        this._conf.setItem("latlng", JSON.stringify(this.ipList));
      }
    });
  }

  reviewPending() {
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      this._service.pendingReviewList().subscribe(
        (res: any) => {
          console.log("customer/reviewAndRatingPending", res);
          let rating = res.data;
          console.log("rating[0]", rating);
          if (rating && rating.length > 0) {
            this.bId = rating[0];
            this.getReviewList();
          }
          this._conf.setItem('review', true);
        },
        err => {
          console.log(err.error.message);
          if (err.status == 498) {
            this._missionService.loginOpen(false);
          }
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    }
  }

  getReviewList() {
    let list = {
      bId: this.bId
    };
    this._serviceProfile.getReview(list).subscribe(
      (res: any) => {
        console.log("booking/invoice", res);
        this.bInvoice = res.data;
        if (this.bInvoice.customerRating) {
          this.bInvoice.customerRating.forEach(x => {
            x.rating = 5;
          });
        }
        if (
          this.bInvoice.additionalService &&
          this.bInvoice.additionalService.length > 0
        ) {
          this.bInvoice.additionalService.forEach(x => {
            x.price = parseFloat(x.price).toFixed(2);
          });
        }
        this.bId = res.data.bookingId;
        res.data.bookingRequestedFor = moment
          .unix(res.data.bookingRequestedFor)
          .format("DD MMM, YYYY | hh:mm A");
        res.data.accounting.total = parseFloat(
          res.data.accounting.total
        ).toFixed(2);
        this.reviewSuccess = false;
        $("#reviewProvider").modal("show");
        this.popupCart = false;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(false);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  show_rating_txt(val, list, i) {
    // console.log(this.bInvoice.customerRating[i])
    this.bInvoice.customerRating[i].rating = val;
    // this.reviewErr = false;
    // let ratingList = {
    //   _id: list._id,
    //   name: list.name,
    //   rating: val
    // }
    // let itemIndex = this.ratingList.findIndex(x => x._id == list._id);
    // if (itemIndex > -1) {
    //   this.ratingList[itemIndex].rating = val;
    // } else {
    //   this.ratingList.push(ratingList);
    // }
    // console.log(this.ratingList)
  }

  reviewSubmitList() {
    this.ratingList = [];
    var ratings = false;
    if (this.bInvoice.customerRating && this.bInvoice.customerRating.length > 0) {
      this.bInvoice.customerRating.forEach(x => {
        let ratingList = {
          _id: x._id,
          name: x.name,
          rating: x.rating
        };
        this.ratingList.push(ratingList);
      });
      ratings = this.ratingList && this.ratingList.length > 0?true:false;
    }else{
      ratings = this.reviewMsg?true:false;
    }    
    // console.log(ratings)
    if (ratings) {
      let list = {
        bookingId: this.bId,
        rating: JSON.stringify(this.ratingList),
        review: this.reviewMsg || ""
      };
      this.loaderLists = true;
      this._serviceProfile.reviewSubmit(list).subscribe(
        (res: any) => {
          console.log("booking/invoice", res);
          this.loaderLists = true;
          this.reviewSuccess = true;
          setTimeout(() => {
            $(".modal").modal("hide");
            this._router.navigate([""]);
          }, 2500);
        },
        err => {
          console.log(err.error.message);
          if (err.status == 498) {
            this._missionService.loginOpen(false);
          }
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.reviewErr = "Please rate";
    }
  }

  favoriteAddorRemove(pId, cId, favorite) {
    let list = {
      categoryId: cId,
      providerId: pId
    };
    this._serviceProfile.addorRemoveFavorite(list, favorite).subscribe(
      (res: any) => {
        this.bInvoice.favouriteProvider = favorite ? false : true;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: "",
        nexthour: ""
      };
      hour_array.hour = moment()
        .add(i, "hour")
        .format("h A");
      var curr = moment()
        .add(i, "hour")
        .format("h h");
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, "hour")
        .format("h A");
      this.sendTime.push(hour_array);
    }
  }

  getCity() {
    this.cityName = this._conf.getItem("city");
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
    } else {
      // this.latlng = {
      //   lat: parseFloat(this.ipList.lat),
      //   lng: parseFloat(this.ipList.lng)
      // };
    }
    this._service.cityList().subscribe(
      (res: any) => {
        console.log("website/city", res);
        res.data.sort(function(a, b) {
          var nameA = a.city.toLowerCase(),
            nameB = b.city.toLowerCase();
          if (nameA < nameB)
            //sort string ascending
            return -1;
          if (nameA > nameB) return 1;
          return 0; //default return value (no sorting)
        });
        this.citys = res.data;
        this.dataList = res.data;
        this.cityName = $("#cityNames").val() || "Select City";   
        // this.cityName = this.citys[0].city;
        this.categoriesList("0");
        // this.ratList("0");
        //this.citys[0].id
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(false);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  filterSearch(val) {
    var city = this.dataList.filter(x => {
      if (val.length > 0) {
        console.log(val.toString().toLowerCase());
        return (
          x.city
            .toString()
            .toLowerCase()
            .indexOf(val.toString().toLowerCase()) === 0
        );
      } else {
        return this.citys;
      }
    });
    // console.log(city)
    this.citys = city;
  }

  listCat(i) {
    this.index = i;
    // $("#notaryTask").modal("hide");
    // $("#cityName").text(this.citys[i].city);
    this.cityName = this.citys[i].city;
    this.cityId = this.citys[i].id;
    this.categoriesList(this.citys[i].id);
    // this.ratList(this.citys[i].id);
    this._missionService.catSearch(this.citys[i].id);
  }

  categoriesList(val) {
    let list = {
      lat: this.latlng.lat,
      long: this.latlng.lng,
      ipAddress: this.ipAddress,
      cityId: val || "0"
    };
    this.catLoaderList = true;
    this._service.catList(list, "").subscribe(
      (res: any) => {
        this.catLoaderList = false;
        console.log("website/categories", res);
        this.carouselTileItems = res.data.catArr;
        this.carouselTileRecommend = res.data.recommendedArr;
        this.trendingList = res.data.trendingArr;
      },
      err => {
        console.log(err.error.message);
        this.catLoaderList = false;
        this.carouselTileItems = [];
        if (err.status == 498) {
          this._missionService.loginOpen(false);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  ratList(val) {
    let list = {
      lat: this.latlng.lat,
      long: this.latlng.lng,
      ipAddress: this.ipAddress,
      cityId: val || "0"
    };
    this._service.ratingList(list).subscribe(
      (res: any) => {
        console.log("website/rating", res);
        this.ratingItems = res.data;
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(false);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }
  // index = 0;
  searchCat(val) {
    // fromEvent(event.target, 'keyup').pipe(
    //   map((e: any) => e.target.value),
    //   filter((val: string) => val.length > 1),
    //   debounceTime(500),
    //   tap(),
    //   map((query: string) => this._service.catList(list, query)),
    //   switchAll(),
    // ).subscribe((results) => {
    //   console.log("results", results)
    //   // this.loading.emit(false);
    //   // this.results.emit(results);
    // },
    //   (err: any) => {
    //     console.log(err);
    //     // this.loading.emit(false);
    //   },
    //   () => {
    //     // this.loading.emit(false);
    //   }
    // )

    if (val.length > 0) {
      let list = {
        lat: this.latlng.lat,
        long: this.latlng.lng,
        ipAddress: this.ipAddress,
        cityId: this.cityId || "0"
      };
      this._service.catList(list, val).subscribe(
        (res: any) => {
          console.log(res);
          this.catSearch = res.data.catArr;
        },
        err => {
          console.log(err.error.message);
          if (err.status == 498) {
            this._missionService.loginOpen(false);
          }
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.catSearch = false;
    }
  }

  blurSearch() {
    setTimeout(() => {
      this.catSearch = false;
      this.searchCate = "";
    }, 500);
  }

  listCategoryAll(list) {
    this.catAll = list;
    // console.log(list)
    this.listCategory();
  }

  listCategory() {
    $("body").toggleClass("hideHidden");
    this.categoryToggle = !this.categoryToggle;
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  startEndTime: any;
  startEndDate: any;
  errMsg: any;
  selectDays = [];
  bookingShift = [];
  listday = [];

  public shiftDateList = [
    { day: "This Week", days: "0", checked: false, startDate: "", endDate: "" },
    {
      day: "This Month",
      days: "1",
      checked: false,
      startDate: "",
      endDate: ""
    },
    {
      day: "Select Date",
      days: "2",
      checked: false,
      startDate: "",
      endDate: ""
    }
  ];

  dateSelect(i) {
    this.errMsg = false;
    this.selectDays.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList[i].checked = true;
    this.startEndTime = this.shiftDateList[i].days == "2" ? true : false;
    let today = moment().format("YYYY-MM-DD");
    let endOfWeek = moment()
      .endOf("week")
      .toDate();
    let endOfMonth = moment()
      .endOf("month")
      .toDate();
    if (this.shiftDateList[i].days == "0") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfWeek).format("YYYY-MM-DD");
    } else if (this.shiftDateList[i].days == "1") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfMonth).format("YYYY-MM-DD");
    } else {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = "";
    }
    let start = moment(
      this.shiftDateList[i].startDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    let end = moment(
      this.shiftDateList[i].endDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    this.startEndDate = start + " - " + end;
  }

  public shiftDaysList = [
    // { day: "All days of the week", days: "0", checked: false },
    // { day: "All weekdays", days: "1", checked: false },
    // { day: "All weekends", days: "2", checked: false },
    { day: "Select All Days", days: "0", checked: false }
  ];

  daysShift(i) {
    this.errMsg = false;
    this.shiftDaysList[i].checked = true;
    this.selectDays.forEach(x => {
      x.checked = true;
    });
  }

  public daysList = [
    { day: "Sunday", checked: false },
    { day: "Monday", checked: false },
    { day: "Tuesday", checked: false },
    { day: "Wednesday", checked: false },
    { day: "Thursday", checked: false },
    { day: "Friday", checked: false },
    { day: "Saturday", checked: false }
  ];

  daySelect(i) {
    this.errMsg = false;
    this.shiftDaysList[0].checked = false;
    if (!this.selectDays[i].checked) {
      this.selectDays[i].checked = true;
    } else {
      this.selectDays[i].checked = false;
    }
  }

  multipleShift(i: number) {
    this.errMsg = false;
    var stepActive = false;
    if (i != -1) {
      var index = -1;
      if (this.shiftTime == 0) {
        this.selectDays = [];
        index = this.shiftDateList.findIndex(y => y.checked == true);
        if (index > -1) {
          let start = moment(this.shiftDateList[index].startDate, "YYYY-MM-DD");
          let end = moment(this.shiftDateList[index].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          if (this.shiftDateList[index].endDate && days > 0) {
            stepActive = true;
            if (days < 6) {
              for (let i = 0; i <= days; i++) {
                let add = moment()
                  .add(i, "days")
                  .format("dddd");
                let daysIndex = this.daysList.findIndex(m => m.day == add);
                this.selectDays.push(this.daysList[daysIndex]);
              }
            } else {
              this.selectDays = this.daysList;
            }
            // this.selectDays.forEach(x => {
            //   x.checked = false;
            // })
            // console.log(this.selectDays)
          } else {
            stepActive = false;
            this.errMsg = "Select a end date which greater than start date";
            return false;
          }
        } else {
          stepActive = false;
        }
      } else if (this.shiftTime == 1) {
        index = this.selectDays.findIndex(k => k.checked == true);
        this.listday = [];
        var addday = [];
        this.bookingShift = [];
        if (index > -1) {
          var indexS = this.shiftDateList.findIndex(y => y.checked == true);
          let start = moment(
            this.shiftDateList[indexS].startDate,
            "YYYY-MM-DD"
          );
          let end = moment(this.shiftDateList[indexS].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          this.selectDays.forEach(m => {
            if (m.checked == true) {
              this.listday.push(m.day);
            }
          });
          for (let k = 0; k <= days; k++) {
            let add = moment()
              .add(k, "days")
              .format("dddd");
            addday.push(add);
          }
          for (var j = 0; j < this.listday.length; j++) {
            for (var k = 0; k < addday.length; k++) {
              if (this.listday[j] == addday[k]) {
                this.bookingShift.push(addday[k]);
              }
            }
          }
          // console.log(this.listday, addday, this.bookingShift)
          if (this.bookingShift.length > 1) {
            stepActive = true;
          } else {
            stepActive = false;
            this.errMsg =
              "Minimum 2 shifts required for multiple shift booking.";
            return false;
          }
        } else {
          stepActive = false;
          this.errMsg = "Select an days";
          return false;
        }
      }
    } else {
      stepActive = true;
      if (this.shiftTime == 0) {
        this.asapSchedule = 0;
      }
    }
    // console.log(i, index, stepActive)
    if (stepActive) {
      this.shiftTime = Math.min(2, Math.max(0, this.shiftTime + i));
    } else {
      this.errMsg = "Select an option to continue";
    }
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val) {
    $(".timePick").val("");
    $(".timeduration").val("");
    this.errMsg = false;
    this.shiftTime = 0;
    this.count = 1;
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = 0;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else if (val == 1) {
      this.asapSchedule = 1;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    } else {
      this.asapSchedule = 2;
    }
  }

  dateChange() {
    this.errMsg = false;
    var day = [];
    var time = "";
    var scheduleTime = "";
    if (this.asapSchedule == 1) {
      this.bookingType = 2;
      this.bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        this.bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        this.bookDate = this.bookDates;
      }
    } else if (this.asapSchedule == 0) {
      this.bookingType = 1;
      let bookDate = new Date();
      this.bookTitle = "Book Now";
      this.bookDate = moment()
        .add(bookDate, "days")
        .format("dddd,D MMM Y");
    } else {
      this.bookingType = 3;
      this.bookTitle = "Booking Shift";
      var endDTime;
      var bookingShift = this.bookingShift.length;
      day = this.listday;
      this.shiftDateList.forEach(x => {
        if (x.checked == true) {
          this.bookDate = moment(x.startDate, "YYYY-MM-DD").format(
            "dddd,D MMM Y"
          );
          endDTime = moment(x.endDate, "YYYY-MM-DD").format("dddd,D MMM Y");
        }
      });
    }

    time = $(".timePick").val();
    scheduleTime = $(".timeduration").val();
    this.scheduleDateTimeStamp = moment(
      this.bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    this.endDTime = moment(endDTime + " 23:00", "dddd,D MMM Y hh:mm").unix();
    let scheduleTimes = scheduleTime.split(":");
    let schTime = scheduleTimes[0] == "00" ? 1 : scheduleTimes[0];
    this.scheduleTime =
      scheduleTimes[0] == "00"
        ? 0 + Number(scheduleTimes[1])
        : Number(scheduleTimes[0]) * 60 + Number(scheduleTimes[1]);
    let shiftDuration = scheduleTimes[0] + " hr :" + scheduleTimes[1];
    // console.log(scheduleTime, this.scheduleTime, time)
    let list = {
      bookTitle: this.bookTitle,
      bookingType: this.bookingType,
      callType: this.typeCall,
      bookDate: this.bookDate,
      endDate: endDTime,
      days: day,
      bookTime: time,
      bookCount: this.scheduleTime,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      startTimeStamp: this.scheduleDateTimeStamp,
      endTimeStamp: this.endDTime,
      scheduleTime: this.scheduleTime || 60,
      shiftDuration: shiftDuration,
      bookingShift: bookingShift || ""
    };
    // console.log(list)
    // let times = moment().add(1, 'hour').format('hh:mm a');
    var Time = moment().format("hh:mm:ss a");
    var startTime = moment(Time, "hh:mm:ss a");
    var endTime = moment(time, "hh:mm:ss a");
    let times = endTime.diff(startTime, "minutes");
    // if (times <= 59) {
    //   this.errMsg = "Select start time minimum 1 hour from current time.";
    //   return false;
    // }
    if (this.asapSchedule == 0 || (time && scheduleTime)) {
      $(".modal").modal("hide");
      this._conf.setItem("bookingDates", JSON.stringify(list));
      this.notaryService();
    } else {
      this.errMsg = "Select time and duration";
    }
  }

  sType: any;
  offers: any;
  minHours:any;
  typeBooking(list) {
    this.startEndDate = false;
    this.startEndTime = false;
    this.shiftDateList.forEach(x => (x.checked = false));
    this.shiftDaysList.forEach(x => (x.checked = false));
    this.errMsg = false;
    this.asapSchedule = 0;
    console.log("test", list);
    this.cLocation = false;
    var loginFlag = this._conf.getItem("isLogin");
    this.catName = list.cat_name;
    this.catId = list._id;
    this.bookingList = list;
    this.minHours = list.minimum_hour;
    this._conf.setItem('minHours', this.minHours);
    if (list.offers) {
      this.offers = list.offers;
    }
    console.log(this.bookingList);
    this.locationLists = false;
    this.typeCallList = false;
    this.sType = list.service_type;
    this.cityAddress = this._conf.getItem("city");

    if (list.service_type == 3) {
      if (!loginFlag) {
        this._missionService.loginOpen(false);
      } else {
        var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
        this._router.navigate(["./", catName, this.catId]);
        // this.sendGetAddress();
        // $("#biddingTask").modal("show");
        // if (this._conf.getItem("place")) {
        //   $("#bidFlow").val(JSON.parse(this._conf.getItem("place")));
        // }
      }
    } else {
      list.hasOwnProperty('callType') ? 
      list.callType.incall ? this.typeCall = 1 : this.typeCall = 2 :'';
      // this.typeCall = list.callType.incall?1:2; 
      if(list.questionArr){
        var questionArr = list.questionArr.filter(a => {
          if (
            a.questionType == 5 ||
            a.questionType == 6 ||
            a.questionType == 7 ||
            a.questionType == 8 ||
            a.questionType == 9 ||
            a.questionType == 10 ||
            a.questionType == 2
          ) {
            return a;
          }
        });   
     }         
      if (questionArr && questionArr.length > 0) {
        var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
        this._router.navigate(["./call-type", catName, this.catId]);
      } else {          
        this.sendDate.forEach(x => {
          x.active = false;
        });
        $("#notaryTask").modal("show");
        if (this._conf.getItem("place")) {
          const latlng = JSON.parse(this._conf.getItem("latlng"));
          $("#latS").val(latlng.lat);
          $("#lngS").val(latlng.lng);
          $("#locatFill").val(JSON.parse(this._conf.getItem("place")));
        }
      }
    }
  }

  catListAll(list) {
    if (list.categoryName && !list.cat_name) {
      this.trendingService(list);
    } else {
      this.typeBooking(list);
    }
    this.listCategory();
  }

  trendingService(list) {
    // console.log(list);
    this.cityAddress = list.city;
    // this.catName = list.categoryName;
    this.catId = list.categoryId;
    this.carouselTileItems.forEach(x => {
      var index = x.category.findIndex(y => y._id == this.catId);
      // console.log(x.category[index]);
      if (x.category[index]) {
        this.typeBooking(x.category[index]);
      }
    });
    // this.bookingList = list;
    // this.locationLists = false;
    // // this.notaryService();
    // $("#locatFill").val(JSON.parse(this._conf.getItem("place")));
    // $("#notaryTask").modal("show");
  }

  notaryService() {
    // var catName = this.catName.split(" ").join("_");
    var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
    // this._router.navigate(["./", this.cityAddress, catName, this.catId]);
    if (this.sType == 1) {
      this._router.navigate(["./", this.cityAddress, catName, this.catId, "0"], {queryParamsHandling: 'merge',queryParams:{minHours: this.minHours} });
    } else {
      this._router.navigate(["./", this.cityAddress, catName, this.catId], {queryParamsHandling: 'merge' ,queryParams:{minHours: this.minHours}});
    }
  }

  selectTypeCall() {
    var locat = $("#locatFill").val();
    const lat = $("#latS").val();
    if (locat && lat) {
      this.typeCallList = true;
      this.locAddress = locat;
      this.asapSchedule = 0;
      this._conf.setItem("place", JSON.stringify(locat));
    } else {
      this.locAddress = "";
    }
    console.log("place", this._conf.getItem("place"))
    this.radioSelect(2);
    this.bookingListSchedule();
    console.log("selectTypeCall");
  }

  bookingListSchedule() {
    if (this.typeCall > 0) {
      this.errMsg = false;
      this.locationLists = true;
    } else {
      this.errMsg = "Select an option to continue";
    }
  }

  typeCallList: boolean;
  typeCall = 1;
  radioSelect(val) {
    this.errMsg = false;
    this.typeCall = val;
  }

  closeLocation() {
    $("#locatFill").val("");
    this.locAddress = "";
    $("#latS").val("");
    $("#lngS").val("");
  }

  currentLocation() {
    $(".errorText").hide();
    $(".locateLoader").show();
    setTimeout(() => {
      $(".locateLoader").hide();
      var loc = $("#locatFill").val();
      if (!loc) {
        this.selectTypeCall();
      }
    }, 3000);
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        let placelatlng = {
          lat: lat,
          lng: lng
        };
        console.log("currentLatLng", placelatlng);
        this._conf.setItem("latlng", JSON.stringify(placelatlng));
        $("#latS").val(lat);
        $("#lngS").val(lng);
        var latlng = new google.maps.LatLng(lat, lng);
        var geocoder = (geocoder = new google.maps.Geocoder());
        geocoder.geocode({ latLng: latlng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              for (
                var i = 0;
                i < results[1].address_components.length;
                i += 1
              ) {
                var addressObj = results[1].address_components[i];
                for (var j = 0; j < addressObj.types.length; j += 1) {
                  if (addressObj.types[j] === "locality") {
                    var City = addressObj.long_name;
                    // console.log(addressObj.long_name);
                    this.cityAddress = City;
                  }
                  if (addressObj.types[j] === "administrative_area_level_1") {
                    var state = addressObj.short_name;
                    // console.log(addressObj.short_name);
                  }
                }
              }
              $("#locatFill").val(results[0].formatted_address);
              this.locAddress = results[0].formatted_address;
              this.cityAddress = City;
              this.cityName = City;
              this._conf.setItem("city", City);
              $(".locateLoader").hide();
            }
          }
        });
      }, this.geoError);
    } else {
      console.log("Geolocation is not supported by this browser.");
    }
  }

  geoError() {
    console.log("Geolocation is not supported by this browser.");
  }

  locationSearch() {
    $("#latS").val("");
    $("#lngS").val("");
    $(".errorText").hide();
    var input = document.getElementById("locatFill");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      let placelatlng = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };
      this._conf.setItem("latlng", JSON.stringify(placelatlng));
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === "locality") {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
            this.cityAddress = City;
          }
          if (addressObj.types[j] === "administrative_area_level_1") {
            var state = addressObj.short_name;
          }
        }
      }
      let citys = City ? City : place.formatted_address;
      this.cityName = citys;
      this._conf.setItem("city", citys);
      $("#latS").val(place.geometry.location.lat());
      $("#lngS").val(place.geometry.location.lng());
    });
  }
}
