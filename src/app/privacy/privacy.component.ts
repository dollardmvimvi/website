import { Component, OnInit } from '@angular/core';
import { MissionService } from '../app.service';

@Component({
  selector: 'privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor(private _missionService: MissionService) { }

  ngOnInit() {
    this._missionService.scrollTops(false);
  }

}
