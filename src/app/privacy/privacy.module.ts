import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { PrivacyComponent } from './privacy.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: PrivacyComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    PrivacyComponent
  ]
})
export class PrivacyModule { }
