import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home/home.service';
import { MissionService } from '../app.service';
import { LanguageService } from '../app.language';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private _service: HomeService, private _missionService: MissionService, private _lang: LanguageService) { }

  listAbout: any;
  loaderList=false;
  shimmer:any = [];
  aboutLan:any;

  ngOnInit() {
    this.aboutLan = this._lang.about;
    this._missionService.scrollTops(false);
    this.placeHolder();
    this.aboutContent();
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  aboutContent() {
    this._service.aboutList()
      .subscribe((res: any) => {
        this.loaderList = true;
        console.log("website/aboutus", res);
        this.listAbout = res.data;        
      }, err => {
        console.log(err.error);
      });
  }

}
