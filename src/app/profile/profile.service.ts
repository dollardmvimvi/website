import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Configuration } from "../app.constant";

@Injectable({
  providedIn: "root"
})
export class ProfileService {
  constructor(private _http: HttpClient, private _conf: Configuration) {}

  getProfiles() {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "profile/me";
    return this._http.get(url, { headers: this._conf.headers });
  }

  getProfileupdate(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "profile/me";
    return this._http.patch(url, list, { headers: this._conf.headers });
  }

  getPwdupdate(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "password/me";
    return this._http.patch(url, list, { headers: this._conf.headers });
  }

  getEmailupdate(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "email";
    return this._http.patch(url, list, { headers: this._conf.headers });
  }

  getPhoneupdate(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "phoneNumber";
    return this._http.patch(url, list, { headers: this._conf.headers });
  }

  resendOtp(list) {
    let body = list;
    console.log(body);
    return this._http.post(this._conf.CustomerUrl + "resendOtp", body, {
      headers: this._conf.headers
    });
  }

  OtpForgot(list) {
    let body = list;
    console.log(body);
    return this._http.post(
      this._conf.CustomerUrl + "verifyVerificationCode",
      body,
      { headers: this._conf.headers }
    );
  }

  getBookings(data) {
    this._conf.setAuth();
    // let url = this._conf.CustomerUrl + "bookings/" + data;
    let url = this._conf.CustomerUrl + "bookings";
    return this._http.get(url, { headers: this._conf.headers });
  }


  getStatus(bId) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "booking/" + bId;
    return this._http.get(url, { headers: this._conf.headers });
  }

  cancelReason(bId) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "cancelReasons/" + bId;
    return this._http.get(url, { headers: this._conf.headers });
  }

  cancelReasons(list) {
    this._conf.setAuth();
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "cancelBooking";
    return this._http.patch(url, body, { headers: this._conf.headers });
  }

  addressSaved(list) {
    this._conf.setAuth();
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "address";
    return this._http.post(url, body, { headers: this._conf.headers });
  }

  addressEditSaved(list, id) {
    this._conf.setAuth();
    list.id = id;
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "address";
    return this._http.patch(url, body, { headers: this._conf.headers });
  }

  getAddress() {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "address";
    return this._http.get(url, { headers: this._conf.headers });
  }

  addressDelete(id) {
    this._conf.setAuth();
    console.log();
    let url = this._conf.CustomerUrl + "address/" + id;
    return this._http.delete(url, { headers: this._conf.headers });
  }

  getTransaction(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "wallet/transction/" + list.pageIndex;
    return this._http.get(url, { headers: this._conf.headers });
  }

  getwalletMoney() {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "wallet";
    return this._http.get(url, { headers: this._conf.headers });
  }

  getaddwalletMoney(list) {
    this._conf.setAuth();
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "wallet/recharge";
    return this._http.post(url, body, { headers: this._conf.headers });
  }

  getReview(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + "booking/invoice/" + list.bId;
    return this._http.get(url, { headers: this._conf.headers });
  }

  reviewSubmit(list) {
    this._conf.setAuth();
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "reviewAndRating";
    return this._http.post(url, body, { headers: this._conf.headers });
  }

  hireProvider(list) {
    this._conf.setAuth();
    let body = list;
    console.log(list);
    let url = this._conf.CustomerUrl + "responseBooking";
    return this._http.patch(url, body, { headers: this._conf.headers });
  }

  bidProviderList(list) {
    this._conf.setAuth();
    // console.log(list)
    let url = this._conf.CustomerUrl + "booking/" + list;
    return this._http.get(url, { headers: this._conf.headers });
  }

  addorRemoveFavorite(list, favorite) {
    this._conf.setAuth();
    console.log(list);
    if (favorite) {
      let url =
        this._conf.CustomerUrl +
        "favouriteProvider/" +
        list.providerId +
        "/" +
        list.categoryId;
      return this._http.delete(url, { headers: this._conf.headers });
    } else {
      let body = list;
      let url = this._conf.CustomerUrl + "favouriteProvider";
      return this._http.post(url, body, { headers: this._conf.headers });
    }
  }

  // paytabs payment api
  paytabPayment() {
    this._conf.setAuth();
    let url = this._conf.Url + 'paytabcard';
    return this._http.get(url, { headers: this._conf.headers });
  }
  // assign for another provider

  getAnotherProv(list){
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + 'bookingByBookingId';
    console.log("list ", list,this._conf.headers)
    return this._http.post(url, list, { headers: this._conf.headers });
  }
}
