import { Component, OnInit, NgZone } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ViewpriceService } from "../viewprice/viewprice.service";
import { LanguageService } from "../app.language";
import { MissionService } from "../app.service";
import { Configuration } from "../app.constant";
import { CheckoutService } from "./checkout.service";
import { Location } from "@angular/common";
import { ProfileService } from "../profile/profile.service";

declare var $: any;
declare var google: any;

declare var require: any;
var creditCardType = require("credit-card-type");
var getTypeInfo = require("credit-card-type").getTypeInfo;
var CardType = require("credit-card-type").types;
declare var moment: any;
declare var Stripe: any;
declare var flatpickr: any;
declare var toastr: any;

@Component({
  selector: "checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.css"]
})
export class CheckoutComponent implements OnInit {
  minHours: any = 1;
  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _serviceView: ViewpriceService,
    private _conf: Configuration,
    private _zone: NgZone,
    private _location: Location,
    private _missionService: MissionService,
    private _service: CheckoutService,
    private _serviceProfile: ProfileService,
    private viewService : ViewpriceService
  ) {
    this.route.queryParams.subscribe(params =>{
        this.minHours = params['minHours'];
        console.log("minHours", this.minHours)
        // this.minHours ? this.qty = this.minHours : this.qty = 1;
    })
  }

  catId: any;
  catName: any;
  serviceList: any;
  cartList: any;
  action = 1;
  cartListActive: any;
  endTimeis = moment().format("HH:mm");
  shimmer: any = [];
  loaderList = false;
  selectedLang:any;
  checkLan: any;
  newAddress: any;
  changeNewAddress: any;
  checkoutBtn: any;
  place: any;
  latlng: any;

  searchLocationToggle: any;
  typeOne = false;
  typeTwo = false;
  typeThree = false;
  otherText = false;
  addressType = 3;
  Others: string;
  selectedAdd: any;

  customerStripeId: any;
  payGetCartList: any;
  monthYear: string;
  Month: string;
  Year: string;
  cardCredit: string;
  cardBrand: string;
  cardcvv: string;
  cardHolder: string;
  validDM = false;
  validCD: any;
  stripeToken: any;
  stripeCardDetails = false;
  errorSaveCart: any;
  PaymentLoader = false;
  promoCode: any;
  grandTotal :any = '';
  orderSuccess: any;
  newCardToggle: any;
  serviceType: any;
  serviceTypehr: any;
  billing_model: any;
  providerId: any;
  providerList: any;
  ratingTrue: any;
  savedAddress: any;
  addressMap: string;
  doorNo: string;

  bookingType = 1;
  callType = 2;
  timing: any;
  noShifts: any = 1;
  shiftDuration: any;
  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = 0;
  shiftTime = 0;
  endDate: any;
  startDTime: any;
  endDTime: any;
  daysLists: any;
  bookingList = false;
  count = 1;
  qty = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  EndTime: any;
  bookDates: any;
  addressID: any;
  errorMsg: any;
  cartCount: any;
  CouponToggle: any;
  couponCode: string;
  jobDescription: string;
  successMsg: any;
  errMsg: any;
  loaderApply: any;
  couponPrice: any;
  deliveryFee: any;
  promoId: any;
  cancelBtn: any;
  cardWallet: any;
  cardWallets: any;
  index: any;
  id: any;
  cardId: any;
  addMoney: string;
  walletMoney: any;
  paymentMethod = 1;
  paidByWallet = 0;
  walletSubmit = false;
  listOffer: any;

  infowindow: any;
  daysCount:any;

  paymentMethod1 = 1;

  ngOnInit() {    
    this._conf.getItem("selectedPaymentMethod") && Number(this._conf.getItem("selectedPaymentMethod")) == 3 ?
    this.paymentNavMenu(Number(this._conf.getItem("selectedPaymentMethod"))) : '';
    this.newAddress = JSON.parse(this._conf.getItem("selectedAddress")) ? (
      this.selectedAdd = JSON.parse(this._conf.getItem("selectedAddress")),
      this.newAddress = JSON.parse(this._conf.getItem("newAddress")),
      this.changeNewAddress = true,
      this.checkoutBtn = false
      // this.paymentNavMenu(Number(this._conf.getItem("selectedPaymentMethod")))
     ) : '';
    //  this.selectedAdd ? 
    //  console.log('payment method', Number(this._conf.getItem("selectedPaymentMethod")), typeof Number(this._conf.getItem("selectedPaymentMethod"))) : console.log('payment method nothing')
    flatpickr(".BtypePicker", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      enableTime: false,
      time_24hr: false,
      defaultDate: moment(new Date())
        .format("YYYY-MM-DD"),
        disable: [
          function(date) {
            let pastDate = moment(date,'DD MM YYYY').format('DD MM YYYY')
            let DateToShow = moment(new Date()).add(9,'days').format('DD MM YYYY');
            return (moment(pastDate).isBefore(DateToShow, 'day'));
          }
      ],
      allowInput: false,
    });

    /** repeat booking **/
    var startpicker = flatpickr(".repeatStartTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: moment().format("YYYY-MM-DD"),
      allowInput: false,
      enableTime: true,
      time_24hr: false,
      // minDate: "today",
      disable: [
        function(date) {
          let pastDate = moment(date,'DD MM YYYY').format('DD MM YYYY')
          let DateToShow = moment(new Date()).format('DD MM YYYY');
          return (moment(pastDate).isBefore(DateToShow, 'day'));
            // disable every multiple of 8
            // return (date.getDate() < new Date().getDate());
        }
    ],
      maxDate: $(".repeatEndTime").attr("value"),
      onClose: (selectedDates, dateStr, instance) => {
        // console.log("dateStr", dateStr)
        // endpicker.set('minDate', dateStr);
        // console.log("the date is ", $(".repeatStartTime").val())
      },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatStartTime").val(datestr);
        this.shiftDateList[2].startDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    var endpicker = flatpickr(".repeatEndTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: "Z",
      time_24hr: false,
      enableTime : false,
      // disable: $(".repeatStartTime").val(),
      // minDate: $(".repeatStartTime").attr("value"),
      disable: [
        function(date) {
          let pastDate = moment(date,'DD MM YYYY').format('DD MM YYYY')
          let DateToShow = moment($(".repeatStartTime").val()).format('DD MM YYYY');
          return (moment(pastDate).isBefore(DateToShow, 'day'));
            // disable every multiple of 8
            // return (date.getDate() < new Date().getDate());
        }
      ],
      allowInput: false,
      // onClose: (selectedDates, dateStr, instance) => {
      //   startpicker.set('maxDate', dateStr);
      // },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatEndTime").val(datestr);
        this.shiftDateList[2].endDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    this._missionService.scrollTop(true);
    this.placeHolder();
    this.checkLan = this._lang.checkout;
    this.route.params.subscribe(params => {
      this.catId = params["id"];
      this.providerId = params["Pid"];
      this.listService(this.catId);
      // this.getCartList(this.catId);
    });
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      this.newAddress = JSON.parse(this._conf.getItem("place"));
      $(".arealat").val(this.latlng.lat);
      $(".arealng").val(this.latlng.lng);
    }

    // let bookDate = this._conf.getItem("bookingDate");
    // this.bookDate = moment().add(bookDate, 'days').format('dddd,D MMM Y');
    // var hour = moment().add(0, 'hour').format('h A');
    // var nexthour = moment().add(1, 'hour').format('h A');
    // this.timing = hour + " - " + nexthour;
    let stripKey = this._conf.getItem("PublishableKey");
    Stripe.setPublishableKey(stripKey);

    this.sendGetCart();
    this.sendGetAddress();
    this.getWallet();
    this.getDate();
    this.getTimes();
    let callType = Number(this._conf.getItem("inCallType"));
    // console.log("callType", callType);
    if (callType == 2) {
      if (this._conf.getItem("bookingDates")) {
        var date = JSON.parse(this._conf.getItem("bookingDates"));
        console.log("date", date);
        this.bookTitle = date.bookTitle;
        this.bookDate = date.bookDate;
        this.bookingType = date.bookingType;
        this.callType = date.callType || callType;
        this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
        this.scheduleTime = date.scheduleTime;        
        if (this.bookingType == 2) {
          this.timing = moment(date.bookTime, 'hh:mm').format("hh:mm");
          this.shiftDuration = date.shiftDuration;
        } else if (this.bookingType == 1) {
          this.timing = moment(new Date()).format("hh:mm");
        } else {
          this.endDate = date.endDate;
          this.startDTime = date.startTimeStamp;
          this.endDTime = date.endTimeStamp;
          this.daysLists = date.days;
          this.noShifts = date.bookingShift;
          this.shiftDuration = date.shiftDuration;
          this.timing = moment(date.bookTime, 'hh:mm A').format("hh:mm A");
        }

        if (this.serviceTypehr != 2) {
          this.EndTime = moment(date.bookTime, "hh:mm a")
            .add(date.scheduleTime, "minutes")
            .format("hh:mm A");                    
          const dateEnd = moment(date.endDate+ " " + date.bookTime, 'dddd,D MMM Y hh:mm:ss A').add(date.scheduleTime, 'minutes').format('dddd,D MMM Y');
          this.endDate = dateEnd;
          let start = moment(date.bookDate, "dddd,D MMM Y");
          let end = moment(dateEnd, "dddd,D MMM Y");
          this.daysCount = end.diff(start, "days");                     
        }
        
      } else {
        this.dateChange();
      }
    } else {
      let incall = JSON.parse(this._conf.getItem("inCall"));
      console.log(incall);
      this.callType = callType;
      this.bookingType = 2;
      this.serviceTypehr = 2;
      this.bookDate = incall.bookDate;
      console.log("the booking date and time ", this.bookDate);
      this.bookTitle = "Schedule";
      this.timing = incall.endTimes;
      this.EndTime = moment(incall.endTimes, "HH:mm a")
        .add(incall.SDuration, "minutes")
        .format("hh:mm A");
    }
    console.log("inig are", this.timing)
    var loginFlag = this._conf.getItem('sessionToken');
    loginFlag  && this.bookingType !== 3 ? this.getSurgePricing(this.latlng.lat,this.latlng.lng) : '';
    this._conf.getItem('lang') ? this.selectedLang = this._conf.getItem('lang') : this.selectedLang = "English";
    console.log("this.selectedLang ",this.selectedLang )
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
    setTimeout(() => {
      this.loaderList = true;
    }, 1000);
  }

  arrow = 3;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  listService(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    };
    // this.loaderList = false;
    this._serviceView.viewServiceList(list).subscribe(
      (res: any) => {
        // this.loaderList = true;
        console.log("customer/services", res);
        this.serviceList = res;
        this.providerList = res.providerData;
        this.catName = res.categoryData.cat_name;
        this.serviceType = res.categoryData.service_type;
        this.billing_model = res.categoryData.billing_model;
        // for (var i = 0; i < this.serviceList.length; i++) {
        //   this.serviceList[i].service.forEach(x => {
        //     x.btnActive = true;
        //   });
        // }
        this.getCartList(this.catId);

        // this.selectedAdd
        
        if(this.providerList && this.callType == 1){
          let list={
            addLine1:this.providerList.address.addLine1,
            latitude:this.providerList.address.latitude,
            longitude:this.providerList.address.longitude
          }
          this.selectedAdd = list;
          this.newAddress = this.providerList.address.addLine1;
          this.changeNewAddress = true;
          this.checkoutBtn = false;
        }

      },
      err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }
  latlng1:any;
  surgePrice:any = 1;
  totalServiceCompletionTime = 0;
  getSurgePricing(lat,long){
    let latlng = this._conf.getItem("latlng");
    (latlng) ? 
      this.latlng1 = JSON.parse(this._conf.getItem("latlng")):'';
  
    let body = {
      bookingType: this.bookingType,
      latitude:lat,
      longitude:long,
      categoryId:this.catId
    }
    this.viewService.getsurgePrice(body).subscribe(res =>{
        console.log("surgePrice ", res);
        this.surgePrice = res['data'].surgePrice;
    },error =>{
           console.log("surge price error", error)
    });
  }

  getCartList(id) {
    if ((this.bookingType == 2 || this.bookingType == 3) && (this.shiftDuration)) {     
      let schTime = this.shiftDuration.split(' ');
      this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
    } 

    let list = {
      catId: id,
      providerId: this.providerId,
      callType: this.callType,
      bookingType:this.bookingType,
      quntity: this.qty 
    };
    this._serviceView.getCart(list).subscribe(
      (res: any) => {
        console.log("customer/get-cart", res);
        this.cartList = res.data;
        // this.serviceType = res.data.serviceType;
        this.serviceTypehr = res.data.serviceType;
        if (this.billing_model == 3 || this.billing_model == 4) {
          this._conf.setItem("backClicked", "1");
        }
        // console.log("this.serviceList", this.serviceList);
        if (this.serviceList && this.serviceList.categoryData.offers) {
          // for (let i = 0; i < this.serviceList.categoryData.offers.length; i++) {
          //   var offer = this.serviceList.categoryData.offers[i].minShiftBooking;
          //   if (this.noShifts >= offer) {
          //     // console.log(offer)
          //     this.listOffer = this.serviceList.categoryData.offers[i];
          //     break;
          //   }
          // }
          this.serviceList.categoryData.offers.forEach(x => {
            if (
              this.noShifts >= x.minShiftBooking &&
              this.noShifts <= x.maxShiftBooking
            ) {
              this.listOffer = x;
            }
          });
        }
        console.log("listOffer :", this.listOffer , this.serviceList, this.timing);
        if (this.bookingType == 3 && this.listOffer) {
          this.EndTime = moment(this.timing, "hh:mm a")
            .add(this.cartList.item[0].quntity, "hours")
            .format("hh:mm A");
          let startA = moment(this.timing, "hh:mm a").format("A");
          let endA = moment(this.EndTime, "hh:mm a").format("A");
          console.log("startA and endA",startA,endA )
          if (startA != endA) {
            this.listOffer.add = "(+1)";
          } else {
            this.listOffer.add = "";
          }
          this.scheduleTime = this.cartList.item[0].quntity * 60;
          var grandTotal = parseFloat(res.data.totalAmount).toFixed(2);
          var visitFee = parseInt(res.data.visitFee);
          var TotalAmt = Number(grandTotal) + Number(visitFee);
          if (this.listOffer.discountType == 2) {
            let total = (this.noShifts * TotalAmt * this.listOffer.value) / 100;
            let totalDis = this.noShifts * TotalAmt - total;
            let totalShift = totalDis / this.noShifts;
            this.grandTotal = (totalShift * this.noShifts).toFixed(2);
          } else {
            // let total = this.noShifts * TotalAmt;
            let totalDisCount = TotalAmt - this.listOffer.value;
            const shiftTo = this.noShifts * totalDisCount;
            // let totalShift = totalDisCount / this.noShifts;
            this.grandTotal = shiftTo.toFixed(2);
          }
        } else {
          // let totalAmt = res.data.totalAmount;
          this.bookingType == 3 ? this.grandTotal = ((res.data.totalAmount+res.data.visitFee)* this.noShifts) : this.grandTotal = (res.data.totalAmount+res.data.visitFee);
          console.log("grnad total 33",this.grandTotal )
        }
        console.log("grnad total",this.grandTotal )

        var cartActive = false;
        this.cartList.item.forEach(x => {
          if (x.quntity > 0) {
            this.cartCount = this.cartList.item.length;
            cartActive = true;
           (x.serviceCompletionTime && x.serviceCompletionTime > 0)  ? this.totalServiceCompletionTime += x.serviceCompletionTime :'';
          }
        });
        this.totalServiceCompletionTime <= 0 ? this.totalServiceCompletionTime =  60 :'';
        this.totalServiceCompletionTime > 60 ? this.shiftDuration = moment((this.totalServiceCompletionTime/60),'HH:mm').format("HH:mm") :  this.shiftDuration = this.totalServiceCompletionTime;
        console.log("shiftDuration", this.shiftDuration, this.totalServiceCompletionTime)
        if (cartActive) {
          this.cartListActive = true;
        } else {
          this.cartListActive = false;
          this.backClicked(1);
        }

        if (!cartActive && this.callType == 1) {
          this.addCart("1", 1, 0);
        }
      },
      err => {
        // console.log(err.error);
        setTimeout(() => {
          this.backClicked(1);
        }, 1000);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }

        if(err.status == 416 || err.status == 409)
         {
          this.addCart("1", 1, 0);
        }
      }
    );
  }

  backClicked(link) {
    if (this.callType == 2) {
      let city = this.serviceList.categoryData.city;
      var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
      if (link == 1) {
        this._router.navigate([city, catName, this.catId]);
      } else {
        this._router.navigate([city, catName, this.catId, this.providerId]);
      }
    }
    // this._location.back();
  }

  reviewList(id) {
    let city = this.serviceList.categoryData.city;
    var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
    this._router.navigate(["./review", city, catName, this.catId, id]);
  }
  shwoMinHourErr:any;
  addMinusCart(id, action, index) {
    console.log("action",action,this.cartList.item)
    var maxQty = this.cartList.item[index].maxquantity;
    var Qty = this.cartList.item[index].quntity;
    maxQty == 0 ? maxQty = parseInt(this.cartList.item[index].plusXCostForQuantity) : '';
    if (
      (action == 1 && maxQty >= Qty) ||
      (action == 2 && maxQty >= Qty) ||
      action == 0 ||
      this.cartList.serviceType == 2
    ) {
      this.action = action;
      console.log("Qty > this.minHours", Qty , this.minHours)
      if(id == "1"){
        Qty > this.minHours && action == 2 ? this.addCartList(id) : action == 1 ?  this.addCartList(id) :'';
      }
      else{
        this.addCartList(id);
      }
      this.cartList.item[index].loader = true;
      setTimeout(() => {
        if (action == 1) {
          this.cartList.item[index].quntity += 1;
        } else {
          if(id == "1"){
            Qty > this.minHours && this.cartList.item[index].quntity > 0 ? this.cartList.item[index].quntity -= 1 : (this.shwoMinHourErr = true,setTimeout(()=>{
              this.shwoMinHourErr = false;
            },4000));
          }
          else{
            this.cartList.item[index].quntity > 0 ?  this.cartList.item[index].quntity -= 1 :'';
          }
        }
        this.cartList.item[index].loader = false;
      }, 500);
    } else {
      this.cartList.item[index].maxqty = true;
      setTimeout(() => {
        this.cartList.item[index].maxqty = false;
      }, 1500);
    }
  }

  addCart(id, i, index) {
    this.action = 1;
    this.addCartList(id);
    if (this.serviceList[i]) {
      this.serviceList[i].service[index].loader = true;
      setTimeout(() => {
        this.serviceList[i].service[index].loader = false;
      }, 500);
    }
  }

  addCartList(id) {
    // id === '1' ? this.qty = this.minHours : this.qty = 1;
    let list = {
      serviceType: this.serviceTypehr || 2,
      bookingType: this.bookingType,
      categoryId: this.catId,
      providerId: this.providerId,
      serviceId: id,
      quntity: this.qty,
      action: this.action,
      // callType: this.callType
    };
    if (this.action == 0) {
      list.action = 2;
    }
    this._serviceView.addCart(list).subscribe(
      (res: any) => {
        console.log("customer/cart", res);
        // this.qty = 1;
        this.minHours = 1;
        // this.cartList = res.data;
        this.getCartList(this.catId);
      },
      err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  pickUpChange(val) {
    this.newAddress = val;
    this.changeNewAddress = true;
  }

  paymentNavMenu(id: number) {
    // alert(id)
    $(".list_card").removeClass("active");
    $("#" + id + "paynavMenu").addClass("active");
    $(".list_card_information").removeClass("active");
    $("#" + id + "payNavMenu").addClass("active");
    // this.walletSubmit = false;
    this._conf.setItem("selectedPaymentMethod", id);
    this.paymentMethod1 = id;
  }

  paymentNavMenus(id: number) {
    $(".allSmallCheckout").removeClass("active");
    $("#" + id + id + "payNavMenu").addClass("active");
    this.paymentMethod = id == 1 ? 1 : 2;
    if (id == 4) {
      this.cardWallets = true;
    } else {
      this.cardWallets = false;
    }
    this.checkoutBtn = true;
    // this.walletSubmit = false;
  }

  cardToggle(val) {   
    if (val == 1) {
      this.cardWallet = true;
      // this.payTabPayment();
    } else {
      $("body").toggleClass("hideHidden");
      this.newCardToggle = !this.newCardToggle;
      if (this.cardId) {
        this.cardWallet = false;
      } else {
        this.errorSaveCart = "Please add a card";
        this.cardWallet = true;
      }
    }
  }

  // paytabs payment api
  payTabPayment() {
    var email = this._conf.getItem("email");
    let stripeCartList = {
      cardToken: this.stripeToken,
      email: email
    };
    this._service.paymentStripeCart(stripeCartList).subscribe(
      (res: any) => {
        let currentLocation = window.location;
        this._conf.setItem("pathname", currentLocation.pathname);
        console.log('paytabs response', res.data.payment_url);
        window.location.href = res.data.payment_url;
        // this.PaymentLoader = false;
        // console.log("stripeRes", res);
        // $(".modal").modal("hide");
        // this.sendGetCart();
        // this.resetSaveCart();
        // this.cardToggle(1);
      },
      err => {
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  editAddress(index) {
    // console.log(this.savedAddress[index])
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;
    // this.closeOpenSearch();
    this.addressID = this.savedAddress[index]._id;
    let address1 = this.savedAddress[index].addLine1;
    let name = this.savedAddress[index].name;
    this.doorNo = this.savedAddress[index].houseNo;
    var latAddress = this.savedAddress[index].latitude;
    var lngAddress = this.savedAddress[index].longitude;
    this.selectType(
      this.savedAddress[index].taggedType,
      this.savedAddress[index].taggedAs
    );
    this.latlng = {
      lat: latAddress,
      lng: lngAddress
    };
    // $("#txtPlaces").val(address1);
    this.addressMap = address1;
    $(".areaName").val(address1);
    $(".areaNames").val(name);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    $(".city").val(this.savedAddress[index].city);
    $(".state").val(this.savedAddress[index].state);
    $(".country").val(this.savedAddress[index].country);
    $(".zip").val(this.savedAddress[index].pincode);
    this.continueMap();
  }

  selectType(type: number, val) {
    this.addressType = type;
    if (type == 1) {
      this.typeOne = !this.typeOne;
      this.typeTwo = false;
      this.typeThree = false;
    } else if (type == 2) {
      this.typeTwo = !this.typeTwo;
      this.typeOne = false;
      this.typeThree = false;
    } else if (type == 3) {
      this.Others = val;
      this.otherText = true;
      this.typeThree = !this.typeThree;
      this.typeTwo = false;
      this.typeOne = false;
    }
  }

  closeOpenSearch() {
    $("body").toggleClass("hideHidden");
    this.searchLocationToggle = !this.searchLocationToggle;

    let latlng = this._conf.getItem("latlng");
    if (latlng && $("body").hasClass("hideHidden")) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      var address = JSON.parse(this._conf.getItem("place"));
    }
    // $("#txtPlaces").val(address);
    this.addressMap = address;
    $(".areaName").val(address);
    $(".arealat").val(this.latlng.lat);
    $(".arealng").val(this.latlng.lng);
    this.typeThree = false;
    this.typeTwo = false;
    this.typeOne = false;
    this.otherText = false;
    this.doorNo = "";
    this.continueMap();
  }

  changeAddr() {
    this.changeNewAddress = false;
  }

  addressSelect(i) {
    this.selectedAdd = this.savedAddress[i];
    this.newAddress = this.savedAddress[i].addLine1;
    this.changeNewAddress = true;
    this.checkoutBtn = false;
    console.log('this.selectedAdd', this.selectedAdd, typeof this.selectedAdd);
    console.log('this.selectedAdd1', this.newAddress, typeof this.newAddress);
    this._conf.setItem("selectedAddress", JSON.stringify(this.selectedAdd));
    this._conf.setItem("newAddress", JSON.stringify(this.newAddress));
    this.bookingType !== 3 ? this.getSurgePricing(this.selectedAdd.latitude, this.selectedAdd.longitude) :'';
  }

  sendGetAddress() {
    this.Others = "";
    this.addressID = "";
    this._serviceProfile.getAddress().subscribe(
      (res: any) => {
        console.log("customer/address", res);
        this.savedAddress = res.data;
        this.savedAddress.forEach(x => {
          x.taggedAs = x.taggedAs.toLowerCase();
          if (x.taggedAs == "home") {
            x.taggedType = 1;
          } else if (x.taggedAs == "office" || x.taggedAs == "work") {
            x.taggedType = 2;
          } else {
            x.taggedType = 3;
          }
        });        
      },
      err => {
        console.log(err.error);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  saveAddress() {
    this.progressBar();
    this.loaderList = true;
    var address = $(".areaName").val();
    var names = $(".areaNames").val();
    var city = $(".city").val();
    var state = $(".state").val();
    var country = $(".country").val();
    var zip = $(".zip").val();
    var lat = $(".arealat").val();
    var lng = $(".arealng").val();
    if (this.addressType == 1) {
      var name = "Home";
    } else if (this.addressType == 2) {
      var name = "Office";
    } else if (this.addressType == 3) {
      var name = "Other";
    }
    let list = {
      // name: names || "",
      // houseNo: this.doorNo || "",
      addLine1: address,
      addLine2: "",
      city: city || "",
      state: state || "",
      country: country || "",
      placeId: "",
      pincode: zip || "",
      latitude: lat,
      longitude: lng,
      taggedAs: name,
      userType: 1
    };
    if (this.Others) {
      list.taggedAs = this.Others;
    }
    if (this.addressID) {
      this._serviceProfile.addressEditSaved(list, this.addressID).subscribe(
        (res: any) => {
          this.addressID = "";
          this.loaderList = true;
          console.log("edit customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        },
        err => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this._serviceProfile.addressSaved(list).subscribe(
        (res: any) => {
          this.loaderList = true;
          console.log("customer/address", res);
          this.sendGetAddress();
          $("body").toggleClass("hideHidden");
          this.searchLocationToggle = false;
        },
        err => {
          this.loaderList = true;
          console.log(err.error);
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    }
  }

  progressBar() {
    var progress = setInterval(() => {
      var $bar = $(".barStore");
      if ($bar.width() >= 400) {
        clearInterval(progress);
      } else {
        $bar.width($bar.width() + 5 + "%");
      }
    }, 100);
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  TimePicker(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    var nexthour = moment()
      .add(1, "hour")
      .add(1, "minutes")
      .format("hh a");
    $(".timePick").val(nexthour);
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      altInput: true,
      timeFormat:'H:i',
      // dateFormat: "h:i K",
      // altFormat: "h:i K",
      time_24hr: false,
      defaultDate: nexthour,
      // minDate: nexthour,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $(".timePick").val(datestr);
      }
    });
  }

  TimeDuration(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    $(".timeduration").val("01:00");
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      dateFormat: "H:i",
      altFormat: "H:i",
      defaultDate: "01:00",
      minDate: "01:00",
      // minTime: nexthour,
      time_24hr: false,
      altInput: true,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $(".timeduration").val(datestr);
      }
    });
  }

  startEndTime: any;
  startEndDate: any;
  errMsgs: any;
  selectDays = [];
  bookingShift = [];
  listday = [];
  //Card #299, issue: Norwegian translation is missing. WEB, fixedBy: sowmya, date: 3-1-20
  public shiftDateList = [
    { day: {en:'This Week',no:'Denne uka'}, days: '0', checked: false, startDate: '', endDate: '' },
    {
      day: {en:'This Month',no:'Denne måneden'},
      days: '1',
      checked: false,
      startDate: '',
      endDate: ''
    },
    {
      day: {en:'Select Date',no:'Velg dato'},
      days: '2',
      checked: false,
      startDate: '',
      endDate: ''
    }
  ];

  dateSelect(i) {
    this.errMsgs = false;
    this.selectDays.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList[i].checked = true;
    this.startEndTime = this.shiftDateList[i].days == "2" ? true : false;
    let today = moment().format("YYYY-MM-DD");
    let endOfWeek = moment()
      .endOf("week")
      .toDate();
    let endOfMonth = moment()
      .endOf("month")
      .toDate();
    if (this.shiftDateList[i].days == "0") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfWeek).format("YYYY-MM-DD");
    } else if (this.shiftDateList[i].days == "1") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfMonth).format("YYYY-MM-DD");
    } else {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = "";
    }
    let start = moment(
      this.shiftDateList[i].startDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    let end = moment(
      this.shiftDateList[i].endDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    this.startEndDate = start + " - " + end;
  }

  public shiftDaysList = [
    // { day: "All days of the week", days: "0", checked: false },
    // { day: "All weekdays", days: "1", checked: false },
    // { day: "All weekends", days: "2", checked: false },
    { day: "Select All Days", days: "0", checked: false }
  ];

  daysShift(i) {
    this.errMsgs = false;
    this.shiftDaysList[i].checked = true;
    this.selectDays.forEach(x => {
      x.checked = true;
    });
  }

  public daysList = [
    { day: "Sunday", checked: false },
    { day: "Monday", checked: false },
    { day: "Tuesday", checked: false },
    { day: "Wednesday", checked: false },
    { day: "Thursday", checked: false },
    { day: "Friday", checked: false },
    { day: "Saturday", checked: false }
  ];

  daySelect(i) {
    this.errMsgs = false;
    this.shiftDaysList[0].checked = false;
    if (!this.selectDays[i].checked) {
      this.selectDays[i].checked = true;
    } else {
      this.selectDays[i].checked = false;
    }
  }

  multipleShift(i: number) {
    this.errMsgs = false;
    var stepActive = false;
    if (i != -1) {
      var index = -1;
      if (this.shiftTime == 0) {
        this.selectDays = [];
        index = this.shiftDateList.findIndex(y => y.checked == true);
        if (index > -1) {
          let start = moment(this.shiftDateList[index].startDate, "YYYY-MM-DD");
          let end = moment(this.shiftDateList[index].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          if (this.shiftDateList[index].endDate && days > 0) {
            stepActive = true;
            if (days < 6) {
              for (let i = 0; i <= days; i++) {
                let add = moment()
                  .add(i, "days")
                  .format("dddd");
                let daysIndex = this.daysList.findIndex(m => m.day == add);
                this.selectDays.push(this.daysList[daysIndex]);
              }
            } else {
              this.selectDays = this.daysList;
            }
            // this.selectDays.forEach(x => {
            //   x.checked = false;
            // })
            // console.log(this.selectDays)
          } else {
            stepActive = false;
            this.errMsgs = "Select a end date which greater than start date";
            return false;
          }
        } else {
          stepActive = false;
        }
      } else if (this.shiftTime == 1) {
        index = this.selectDays.findIndex(k => k.checked == true);
        this.listday = [];
        var addday = [];
        this.bookingShift = [];
        if (index > -1) {
          var indexS = this.shiftDateList.findIndex(y => y.checked == true);
          let start = moment(
            this.shiftDateList[indexS].startDate,
            "YYYY-MM-DD"
          );
          let end = moment(this.shiftDateList[indexS].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          this.selectDays.forEach(m => {
            if (m.checked == true) {
              this.listday.push(m.day);
            }
          });
          for (let k = 0; k <= days; k++) {
            let add = moment()
              .add(k, "days")
              .format("dddd");
            addday.push(add);
          }
          for (var j = 0; j < this.listday.length; j++) {
            for (var k = 0; k < addday.length; k++) {
              if (this.listday[j] == addday[k]) {
                this.bookingShift.push(addday[k]);
              }
            }
          }
          // console.log(this.bookingShift)
          if (this.bookingShift.length > 1) {
            stepActive = true;
          } else {
            stepActive = false;
            this.errMsgs =
              "Minimum 2 shifts required for multiple shift booking.";
            return false;
          }
        } else {
          stepActive = false;
          this.errMsgs = "Select an days";
          return false;
        }
      }
    } else {
      stepActive = true;
      if (this.shiftTime == 0) {
        this.asapSchedule = 0;
      }
    }
    // console.log(i, index, stepActive)
    if (stepActive) {
      this.shiftTime = Math.min(2, Math.max(0, this.shiftTime + i));
    } else {
      this.errMsgs = "Select an option to continue";
    }
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val) {
    $(".timePick").val("");
    $(".timeduration").val("");
    this.errMsgs = false;
    this.shiftTime = 0;
    this.count = 1;
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = 0;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else if (val == 1) {
      this.asapSchedule = 1;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    } else {
      this.asapSchedule = 2;
    }
  }

  dateChange() {
    this.errMsgs = false;
    var day = [];
    var time = "";
    var scheduleTime = "";
    var bookTitle = "";
    var bookDate = "";
    var ScheduleDate = "";
    var shiftDuration = "";
    var timeShedule;
    var timeEnd = "";
    if (this.asapSchedule == 1) {
      this.bookingType = 2;
      bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        bookDate = this.bookDates;
      }
      this.timing =
        $(".BtypeTimePicker").val() ||
        moment()
          .add(1, "hour")
          .format("hh:mm:ss A");
    } else if (this.asapSchedule == 0) {
      this.bookingType = 1;
      let bookdate = new Date();
      bookTitle = "Book Now";
      bookDate = moment()
        .add(bookdate, "days")
        .format("dddd,D MMM Y");
      this.timing = moment(new Date()).format("hh:mm:ss");
    } else {
      this.bookingType = 3;
      bookTitle = "Booking Shift";
      var endDTime;
      var bookingShift = this.bookingShift.length;
      day = this.listday;
      this.shiftDateList.forEach(x => {
        if (x.checked == true) {
          bookDate = moment(x.startDate, "YYYY-MM-DD").format("dddd,D MMM Y");
          endDTime = moment(x.endDate, "YYYY-MM-DD").format("dddd,D MMM Y");
        }
      });
      this.endDate = endDTime;
      this.daysLists = day;
      this.noShifts = bookingShift;
    }

    time = $(".timePick").val();
    scheduleTime = $(".timeduration").val();
    this.timing = time;
    ScheduleDate = moment(
      bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    timeEnd = moment(endDTime + " 23:00", "dddd,D MMM Y hh:mm").unix();
    let scheduleTimes = scheduleTime.split(":");
    let schTime = scheduleTimes[0] == "00" ? 1 : scheduleTimes[0];
    timeShedule =
      scheduleTimes[0] == "00"
        ? 0 + Number(scheduleTimes[1])
        : Number(scheduleTimes[0]) * 60 + Number(scheduleTimes[1]);
    shiftDuration = scheduleTimes[0] + " hr :" + scheduleTimes[1];
    this.EndTime = moment(this.timing, "HH:mm")
      .add(this.scheduleTime, "minutes")
      .format("HH:mm");

    let list = {
      bookTitle: bookTitle,
      bookingType: this.bookingType,
      callType:this.callType,
      bookDate: bookDate,
      endDate: endDTime,
      days: day,
      bookTime: time,
      bookCount: this.scheduleTime,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      startTimeStamp: this.scheduleDateTimeStamp,
      endTimeStamp: timeEnd,
      scheduleTime: timeShedule || 60,
      shiftDuration: shiftDuration,
      bookingShift: bookingShift || ""
    };

    var Time = moment().format("hh:mm:ss a");
    var startTime = moment(Time, "hh:mm:ss a");
    var endTime = moment(time, "hh:mm:ss a");
    let times = endTime.diff(startTime, "minutes");
    // if (times <= 59) {
    //   this.errMsgs = "Select start time minimum 1 hour from current time.";
    //   return false;
    // }

    if (this.asapSchedule == 0 || (time && scheduleTime)) {
      this.bookTitle = bookTitle;
      this.bookDate = bookDate;
      this.scheduleDateTimeStamp = ScheduleDate;
      this.endDTime = timeEnd;
      this.scheduleTime = timeShedule;
      this.shiftDuration = shiftDuration;
      this._conf.setItem("bookingDates", JSON.stringify(list));
      $(".modal").modal("hide");

      if((this.bookingType == 2 || this.bookingType == 3) && (this.serviceTypehr == 2)){
        let schTime = this.shiftDuration.split(' ');
        this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
        this.addCartList(this.cartList.serviceId);
       }else{
        this.getCartList(this.catId);
       }

      // if (this.bookingType == 3) {
      //   if (this.serviceTypehr == 2) {
      //     this.qty = Number(schTime);
      //     this.addCartList(this.cartList.serviceId);
      //   } else {
      //     this.getCartList(this.catId);
      //   }
      // }
    } else {
      this.errMsgs = "Select time and duration";
    }
  }
  offers: any;
  typeBooking() {
    this.startEndDate = false;
    this.startEndTime = false;
    this.shiftDateList.forEach(x => (x.checked = false));
    this.shiftDaysList.forEach(x => (x.checked = false));
    this.errMsgs = false;
    this.asapSchedule = 0;
    this.sendDate.forEach(x => {
      x.active = false;
    });
    this.bookingList = this.serviceList.categoryData;
    if (this.serviceList.categoryData.offers) {
      this.offers = this.serviceList.categoryData.offers;
    }
    console.log(this.bookingList);
    $("#notaryTask").modal("show");
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: "",
        nexthour: ""
      };
      hour_array.hour = moment()
        .add(i, "hour")
        .format("h A");
      var curr = moment()
        .add(i, "hour")
        .format("h h");
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, "hour")
        .format("h A");
      this.sendTime.push(hour_array);
    }
  }

  valdationCreditCard(value: any) {
    $(".activeGetCart").removeClass("active");
    this.errorSaveCart = false;
    var visaCards = creditCardType(value);
    var val = value.split(" ").join("");
    if (val.length > 0) {
      val = val.match(new RegExp(".{1,4}", "g")).join(" ");
    }
    this.cardCredit = val;
    if (value.length <= 17) {
      this.validCD = "The card number should be 15 or 16 digits!";
      // this.stripeCardDetails = true;
    } else {
      this.validCD = false;
      // this.stripeCardDetails = false;
    }

    if (value.length <= 4) {
      if (visaCards != "") {
        // console.log(visaCards[0].type);
        this.cardBrand = visaCards[0].type;
        this.validCD = false;
        this.stripeCardDetails = true;
      } else {
        this.validCD = "invalid card number!";
        this.stripeCardDetails = false;
        this.cardCredit = "";
        console.log("error cardtype");
      }
    }
  }

  valdationDate(val: any) {
    this.errorSaveCart = false;
    this.monthYear = val.replace(/(\d{2}(?!\s))/g, "$1/");

    if (isNaN(val)) {
      val = val.replace(/[^0-9\/]/g, "");
      if (val.split("/").length > 2) val = val.replace(/\/+$/, "");
      this.monthYear = val;
    }

    var validDm = /^(0[1-9]|1[0-2])\/\d{4}$/.test(val);
    // console.log(val);
    if (val.length > 2 && validDm == false) {
      this.validDM = true;
      this.stripeCardDetails = false;
    } else {
      this.validDM = false;
      this.stripeCardDetails = true;
    }

    if (val.length == 1) {
      if (val.charAt(0) == 0 || val.charAt(0) == 1) {
        this.monthYear = val;
      } else {
        this.monthYear = "0" + val;
      }
    } else if (val.length == 2) {
      if (val <= 12) {
        this.monthYear = val;
      } else {
        this.monthYear = "";
      }
    }
  }

  valdationcvv(val: any) {
    this.errorSaveCart = false;
    if (val.length == 3) {
      this.stripeCardDetails = true;
    } else {
      this.stripeCardDetails = false;
    }
  }

  stripeCard() {
    // var val = String(Number(this.monthYear));
    var validDateMnth = /^(0[1-9]|1[0-2])\/\d{4}$/.test(this.monthYear);
    // console.log(this.monthYear, validDateMnth);
    if (
      this.cardCredit &&
      this.monthYear &&
      this.cardcvv &&
      validDateMnth == true
    ) {
      this.validDM = false;
      this.sendCardStripToken();
    } else {
      this.validDM = true;
      console.log("error stripeCartList");
    }
  }

  sendCardStripToken() {
    this.PaymentLoader = true;
    setTimeout(() => {
      this.PaymentLoader = false;
    }, 5000);
    var MYdetails = this.monthYear.split("/");
    var numberCard = this.cardCredit.split(" ").join("");
    (<any>window).Stripe.card.createToken(
      {
        number: this.cardCredit,
        exp_month: Number(MYdetails[0]),
        exp_year: Number(MYdetails[1]),
        cvc: this.cardcvv
      },
      (status: number, response: any) => {
        this._zone.run(() => {
          if (status === 200) {
            this.errorSaveCart = false;
            this.stripeToken = response.id;
            this.sendCardStrip();
            // this.newCardToggle = !this.newCardToggle;
            console.log(response.id);
          } else {
            this.errorSaveCart = response.error.message;
            setTimeout(() => {
              this.errorSaveCart = false;
            }, 5000);
            console.log(response.error.message);
          }
        });
      }
    );
  }

  sendCardStrip() {
    var email = this._conf.getItem("email");
    let stripeCartList = {
      cardToken: this.stripeToken,
      email: email
    };
    this._service.paymentStripeCart(stripeCartList).subscribe(
      (res: any) => {
        this.PaymentLoader = false;
        console.log("stripeRes", res);
        $(".modal").modal("hide");
        this.sendGetCart();
        this.resetSaveCart();
        this.cardToggle(1);
      },
      err => {
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  sendGetCart() {
    this._service.paymentGetCart().subscribe(
      (res: any) => {
        console.log("paymentget-card", res);
        this.payGetCartList = res.data;
        if (this.payGetCartList && this.payGetCartList.length > 0) {
        //  const index = this.payGetCartList.findIndex(x=>x.isDefault == true);         
         this.cardId = this.payGetCartList[0].id;
        }
      },
      err => {
        console.log("pget", err);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  deleteGetCart(i: number, id: any) {
    this.index = i;
    this.id = id;
    $("#cardDelete").modal("show");
  }

  deleteCard() {
    let deletepayGetCart = {
      cardId: this.id
    };
    this._service.paymentGetCartDelete(deletepayGetCart).subscribe(
      (res: any) => {
        this.sendGetCart();
        this.payGetCartList.splice(this.index, 1);
      },
      err => {
        console.log("pget", err);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  selectPayments(i: number) {
    this.payGetCartList.forEach(x => {
      x.isDefault = false;
    });
    this.payGetCartList[i].isDefault = true;
    this.cardId = this.payGetCartList[i].id;
    this.defaultCard(this.cardId);
  }

  defaultCard(id){
    let list ={
      cardId:id
    }
    this._service.isDefaultCart(list)
    .subscribe((res: any) => {
      // this.sendGetCart();
    }, (err) => {
      console.log("isDefaultCart", err)

    });
  }

  getWallet() {
    this._serviceProfile.getwalletMoney().subscribe(
      (res: any) => {
        console.log("customer/wallet", res);
        this.walletMoney = res.data;
        this._missionService.addWallets(true);
      },
      err => {
        console.log(err.error.message);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  addWallet() {
    let list = {
      cardId: this.cardId,
      amount: this.addMoney
    };
    setTimeout(() => {
      this.errorMsg = false;
      this.PaymentLoader = false;
    }, 5000);
    if (this.addMoney) {
      this.PaymentLoader = true;
      this.errorMsg = false;
      this._serviceProfile.getaddwalletMoney(list).subscribe(
        (res: any) => {
          console.log("wallet/recharge", res);
          this.PaymentLoader = false;
          this.errorMsg = res.message;
          $(".modal").modal("hide");
          this.getWallet();
          this.addMoney = "";
          this.cardToggle(1);
        },
        err => {
          $(".modal").modal("hide");
          this.PaymentLoader = false;
          console.log(err.error.message);
          this.errorMsg = err.error.message;
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.errorMsg = "Please enter the amount to continue.";
    }
  }

  CouponToggleList() {
    this.couponCode = "";
    $("body").toggleClass("hideHidden");
    this.CouponToggle = !this.CouponToggle;
  }

  applyCoupons() {
    if (this.couponCode) {
      this.errMsg = false;
      let list = {
        latitude: this.latlng.lat,
        longitude: this.latlng.lng,
        cartId: this.cartList._id,
        paymentMethod: this.paymentMethod,
        paidByWallet: this.paidByWallet,
        couponCode: this.couponCode
      };
      setTimeout(() => {
        this.errMsg = false;
        this.loaderApply = false;
      }, 3000);
      this.loaderApply = true;
      this._service.couponList(list).subscribe(
        (res: any) => {
          this.cancelBtn = true;
          this.loaderApply = false;
          this.errMsg = false;
          this.successMsg = res.message;
          this.couponPrice = parseFloat(res.data.discountAmount).toFixed(2);
          this.deliveryFee = res.data.deliveryFee;
          this.promoId = res.data.promoId;
          this.promoCode = this.couponCode;
          this.grandTotal = (
            parseFloat(this.grandTotal) - parseFloat(this.couponPrice)
          ).toFixed(2);
        },
        err => {
          this.cancelBtn = true;
          this.loaderApply = false;
          var error = err.error;
          console.log(err.error);
          this.errMsg = error.message;
          if (err.status == 498) {
            this._missionService.loginOpen(true);
          }
          if (err.status == 440) {
            this._conf.setItem("sessionToken", err.error.data);
            this._missionService.refreshToken(true);
          }
        }
      );
    } else {
      this.errMsg = "Enter coupon";
    }
  }

  cancelCoupons() {
    this.couponCode = "";
    this.cancelBtn = false;
    this.couponPrice = "";
    this.grandTotal = parseFloat(this.cartList.totalAmount).toFixed(2);
  }

  resetSaveCart() {
    this.cardCredit = "";
    this.monthYear = "";
    this.cardcvv = "";
    this.cardHolder = "";
  }

  walletPayment(val: number) {
    this.paidByWallet = 1;
    this.paymentMethod = val;
    this.checkoutSubmitCart();
  }

  cardSubmitCart() {
    this.paidByWallet = 0;
    this.paymentMethod = 2;
    this.checkoutSubmitCart();
  }

  cashSubmitCart() {
    this.paidByWallet = 0;
    this.paymentMethod = 1;
    this.checkoutSubmitCart();
  }  

  checkoutSubmitCart() {
    this.PaymentLoader = true;
    let datetime = moment().format("YYYY-MM-DD HH:mm:ss");
    let addr = this.selectedAdd;
    let prId = this.providerId == "0" ? "" : this.providerId;
    let list = {
      bookingModel: Number(this.serviceType),
      bookingType: this.bookingType,
      // callType: this.callType,
      paymentMethod: this.paymentMethod,
      paidByWallet: this.paidByWallet,
      placeName: "",
      addLine1: addr.addLine1,
      addLine2: "",
      city: "",
      state: "",
      country: "",
      placeId: "",
      pincode: "",
      latitude: addr.latitude,
      longitude: addr.longitude,
      providerId: prId,
      categoryId: this.catId,
      cartId: this.cartList._id,
      jobDescription: this.jobDescription || "",
      promoCode: this.promoCode || "",
      paymentCardId: this.paymentMethod == 1 ? "" : this.cardId,
      bookingDate: "",
      scheduleTime: 0,
      endTimeStamp: 0,
      days: [],
      deviceTime: datetime,
      // questionAndAnswer:[]
    };
    if (this.callType == 2) {
      if (this.bookingType == 2) {
        list.bookingDate = String(this.scheduleDateTimeStamp);
        list.scheduleTime = Number(this.scheduleTime);
      } else if (this.bookingType == 3) {
        list.bookingDate = String(this.scheduleDateTimeStamp);
        list.endTimeStamp = Number(this.endDTime);
        list.scheduleTime = Number(this.scheduleTime);
        list.days = this.daysLists;
      } else {
        list.bookingDate = "";
      }
      let question = JSON.parse(this._conf.getItem("questionAndAnswer"));
      // list.questionAndAnswer = question;
    } else {
      let incall = JSON.parse(this._conf.getItem("inCall"));
      let question = JSON.parse(this._conf.getItem("questionAndAnswer"));
      list.bookingDate = String(incall.TimeStampSchedule);
      list.endTimeStamp = Number(incall.endTime);
      list.scheduleTime = Number(incall.duration);
      // list.questionAndAnswer = question;
    }
    // console.log(list);
    this._service.submitCheckout(list).subscribe(
      (res: any) => {
        $(".modal").modal("hide");
        $("#orderSubmit").modal("show");
        this.PaymentLoader = false;
        this.orderSuccess = res.message;
        this.selectedLang == "English" ? 
        toastr["info"](
          "Yay !We will let you know when the KyhootPro accepts your booking!",
          "KyhootPro"
        ) : toastr["info"](
          "Vi vil gi deg beskjed når KyhootPro godtar bookingen din!",
          "KyhootPro"
        )
        let placelatlng = {
          lat: addr.latitude,
          lng: addr.longitude
        };
        this._conf.setItem("latlng", JSON.stringify(placelatlng));
        this._conf.setItem("place", JSON.stringify(addr.addLine1));
      },
      err => {
        this.PaymentLoader = false;
        this.errorMsg = err.error.message;
        console.log(err.error.message);
        setTimeout(() => {
          this.errorMsg = false;
        }, 5000);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  profileList() {
    this._router.navigate(["./profile"]);
  }

  deleteAddressList(addressID: any, i: any) {
    $("#deleteAddress").modal("show");
    this.addressID = addressID;
    this.index = i;
  }

  deleteAddress() {
    this._serviceProfile.addressDelete(this.addressID).subscribe(
      (result: any) => {
        this.Others = "";
        this.addressID = "";
        this.savedAddress.splice(this.index, 1);
        $(".modal").modal("hide");
      },
      err => {
        var error = JSON.parse(err._body);
        // console.log(error)
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  closeLocation() {
    // $("#txtPlaces").val("");
    this.addressMap = "";
  }

  changeLocation() {
    var input = document.getElementById("txtPlaces");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      for (var i = 0; i < place.address_components.length; i += 1) {
        var addressObj = place.address_components[i];
        for (var j = 0; j < addressObj.types.length; j += 1) {
          if (addressObj.types[j] === "locality") {
            var City = addressObj.long_name;
            // console.log(addressObj.long_name);
          }
          if (addressObj.types[j] === "administrative_area_level_1") {
            var state = addressObj.short_name;
            // console.log(addressObj.short_name);
          }
          if (addressObj.types[j] === "country") {
            var country = addressObj.long_name;
          }
          if (addressObj.types[j] === "postal_code") {
            var zipcode = addressObj.long_name;
          }
        }
      }
      let lat = place.geometry.location.lat();
      let lng = place.geometry.location.lng();
      this.latlng = {
        lat: lat,
        lng: lng
      };
      $(".areaName").val($("#txtPlaces").val());
      $(".areaNames").val(place.name);
      $(".arealat").val(lat);
      $(".arealng").val(lng);
      $(".city").val(City);
      $(".state").val(state);
      $(".country").val(country);
      $(".zip").val(zipcode);
    });
  }

  locationChange() {
    setTimeout(() => {
      this.continueMap();
    }, 500);
  }

  continueMap() {
    var latlng = this.latlng;
    var map = new google.maps.Map(document.getElementById("mapData"), {
      center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      zoom: 15,
      disableDefaultUI: true,
      mapTypeControl: true
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map
      });
    }, 300);

    this.infowindow = new google.maps.InfoWindow();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

    var latlng = new google.maps.LatLng(latlng.lat, latlng.lng);
    var geocoder = (geocoder = new google.maps.Geocoder());
    geocoder.geocode({ latLng: latlng }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          // $(".addressArea").val(results[0].formatted_address);
          // $(".areaName").val(results[0].formatted_address);
          // $(".arealat").val(latlng.lat);
          // $(".arealng").val(latlng.lng);
        }
      }
    });

    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, "dragend", () => {
      geocoder.geocode({ latLng: marker.getPosition() }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            this.addressMap = results[0].formatted_address;
            $(".addressArea").val(results[0].formatted_address);
            $(".areaName").val(results[0].formatted_address);
            $(".arealat").val(marker.getPosition().lat());
            $(".arealng").val(marker.getPosition().lng());
            this.infowindow.setContent(results[0].formatted_address);
            this.infowindow.open(map, marker);
            for (var i = 0; i < results[0].address_components.length; i += 1) {
              var addressObj = results[0].address_components[i];
              for (var j = 0; j < addressObj.types.length; j += 1) {
                if (addressObj.types[j] === "locality") {
                  var City = addressObj.long_name;
                  // console.log(addressObj.long_name);
                }
                if (addressObj.types[j] === "administrative_area_level_1") {
                  var state = addressObj.short_name;
                  // console.log(addressObj.short_name);
                }
                if (addressObj.types[j] === "country") {
                  var country = addressObj.long_name;
                }
                if (addressObj.types[j] === "postal_code") {
                  var zipcode = addressObj.long_name;
                }
              }
            }
            $(".city").val(City);
            $(".state").val(state);
            $(".country").val(country);
            $(".zip").val(zipcode);
          }
        }
      });
    });
  }
   // change tags based on languages 

   getTags(tag){
    let norway = {
      home:'Hjem',
      office:'Jobb',
      other :'Andre'     
   } 
    if(this.selectedLang == "Norwegian"){
       tag == "home" || tag == "Home"? tag = norway.home : '';
       tag == "office" || tag == "Office" || tag == "work" ? tag = norway.office : '';
       tag == "other" || tag == "Other" ? tag = norway.other : '';
    }
    return tag;
   
 }
 convertTo24Hrs(val,caseValue){
  var date = JSON.parse(this._conf.getItem("bookingDates"));
  if(date){
    if(caseValue == 1){
      return moment.unix(date.startTimeStamp).format("HH:mm");
    }
    else if(caseValue == 2){
      let convertTime = moment.unix(date.startTimeStamp).format("HH:mm");
      let endTimeis = moment(convertTime ,"HH:mm").add('minute', this.totalServiceCompletionTime).format("HH:mm");
      // this.totalServiceCompletionTime > 60 ? this.shiftDuration = moment().add('minute', this.totalServiceCompletionTime).format("HH:mm") :  this.shiftDuration = this.totalServiceCompletionTime + 'mins';
      return endTimeis;
      // return moment.unix(date.endTimeStamp).format("HH:mm");
    }
  }  
 }

}
