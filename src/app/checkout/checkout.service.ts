import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private _http: HttpClient, private _conf: Configuration) {

  }



  couponList(list) {   
    let body = list;
    this._conf.setAuth();
    console.log(body);
    return this._http.post(this._conf.CustomerUrl + 'promoCodeValidation', body, { headers: this._conf.headers });
  }

  paymentStripeCart(stripeCart) {
    this._conf.setAuth();
    let url = this._conf.Url + 'card';
    let body = JSON.stringify(stripeCart);
    console.log(body);
    // console.log(this._conf.headers);

    return this._http.post(url, body, { headers: this._conf.headers });
  }

  paymentGetCart() {
    this._conf.setAuth();
    let url = this._conf.Url + 'card';
    return this._http.get(url, { headers: this._conf.headers });

  }

  paymentGetCartDelete(deleteGetCart) {
    this._conf.setAuth();
    let url = this._conf.Url + 'card';
    let body = deleteGetCart;
    console.log(body);
    return this._http.request('DELETE', url, {
      headers: this._conf.headers,
      body: body
    });
  }

  isDefaultCart(id){
    this._conf.setAuth();
    let url = this._conf.Url + 'card';
    let body = id;
    console.log(body);    
    return this._http.patch(url, body, { headers: this._conf.headers });
  }

  submitCheckout(list) {
    this._conf.setAuth();
    let url = this._conf.CustomerUrl + 'booking';
    let body = list;
    console.log(body);
    // console.log(this._conf.headers);

    return this._http.post(url, body, { headers: this._conf.headers });
  }

  // paytabs payment api
  paytabPayment() {
    this._conf.setAuth();
    let url = this._conf.Url + 'paytabcard';
    return this._http.get(url, { headers: this._conf.headers });
  }

}
