import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {

  constructor(private _http: HttpClient, private _conf: Configuration) {

  }


  providerService(list) {
    this._conf.setAuth();
    var Url = this._conf.CustomerUrl + 'provider/' + list.lat + "/" + list.long + "/" + list.catId + "/" + list.ipAddress;
    return this._http.get(Url, { headers: this._conf.headers })
  }


  providerFilter(list) {
    this._conf.setAuth();
    let body = list;
    console.log(body)
    var Url = this._conf.WebsiteUrl + 'provider/filter';
    return this._http.post(Url, body, { headers: this._conf.headers })
  }

  providerReviews(list) {
    this._conf.setAuth();
    let body = list;
    console.log(body)
    var Url = this._conf.CustomerUrl + 'providerDetails/' + list.providerId + "/" + list.categoryId + "/" + list.lat + "/" + list.lng;
    return this._http.get(Url, { headers: this._conf.headers })
  }


  listAllReview(list) {
    this._conf.setAuth();
    let body = list;
    console.log(body)
    var Url = this._conf.CustomerUrl + 'providerReview/' + list.providerId + "/" + list.categoryId + "/" + list.pageNo;
    return this._http.get(Url, { headers: this._conf.headers })
  }


}
