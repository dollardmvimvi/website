import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ProviderComponent } from './provider.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: ProviderComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    ProviderComponent
  ]
})
export class ProviderModule { }
