import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ProviderService } from "./provider.service";
import { HomeService } from "../home/home.service";
import { LanguageService } from "../app.language";
import { MissionService } from "../app.service";
import { Configuration } from "../app.constant";
import { Location } from "@angular/common";
import { Paho } from "ng2-mqtt";

declare var $: any;
declare var google: any;
declare var moment: any;
declare var flatpickr: any;

@Component({
  selector: "provider",
  templateUrl: "./provider.component.html",
  styleUrls: ["./provider.component.css"]
})
export class ProviderComponent implements OnInit {
  private _client: Paho.MQTT.Client;

  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ProviderService,
    private _serviceHome: HomeService,
    private _location: Location
  ) {}

  loaderList = false;
  cityAddress: any;
  catId: any;
  catName: any;
  shimmer: any = [];
  providerList: any;
  latlng: any;
  ipAddress: any;
  listFilter: any;
  listReview: any;
  listReviews: any = [];
  provideLoader = false;
  filterLoader = false;
  loadMore = false;
  providerId: any;
  providerRw: any;
  pageno = 0;

  keyName: string;
  distance = 0;
  minFees: number;
  maxFees: number;
  subCatId: string;
  bookingType = 2;
  callType = 1;

  bookinList: any;
  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = 0;
  shiftTime = 0;
  endDate: any;
  startDTime: any;
  endDTime: any;
  daysLists: any;
  bookingList = false;
  timing: any;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  filterLists: any;
  waitForResponse = 1;
  intervals: any;

  ngOnInit() {
    flatpickr(".BtypePicker", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: moment()
        .add(9, "days")
        .format("YYYY-MM-DD"),
      allowInput: false
    });
    /** repeat booking **/
    var startpicker = flatpickr(".repeatStartTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: moment().format("YYYY-MM-DD"),
      allowInput: false,
      enableTime: true,
      minDate: "today",
      maxDate: $(".repeatEndTime").attr("value"),
      onClose: (selectedDates, dateStr, instance) => {
        // endpicker.set('minDate', moment(dateStr).add(1, 'days').format('YYYY-MM-DD'));
      },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatStartTime").val(datestr);
        this.shiftDateList[2].startDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    var endpicker = flatpickr(".repeatEndTime", {
      altInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      defaultDate: "Z",
      minDate: $(".repeatStartTime").attr("value"),
      allowInput: false,
      onClose: (selectedDates, dateStr, instance) => {
        // startpicker.set('maxDate', dateStr);
      },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $(".repeatEndTime").val(datestr);
        this.shiftDateList[2].endDate = moment(datestr).format("YYYY-MM-DD");
      }
    });

    this._missionService.scrollTop(true);
    this.placeHolder();
    clearInterval(this.intervals);
    this.ipAddress = this._conf.getItem("ip");
    this.route.params.subscribe(params => {
      this.cityAddress = params["city"];
      this.catId = params["id"];
      this.catName = params["name"];
    });
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      let place = JSON.parse(this._conf.getItem("place"));
      $("#latS").val(this.latlng.lat);
      $("#lngS").val(this.latlng.lng);
      $("#changeLocationId").val(place);
    }

    this.getDate();
    this.getTimes();

    if (this._conf.getItem("bookingDates")) {
      var date = JSON.parse(this._conf.getItem("bookingDates"));
      console.log("date", date);
      this.bookTitle = date.bookTitle;
      this.bookDate = date.bookDate;
      this.bookingType = date.bookingType;
      this.callType = date.callType || 1;
      this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
      this.scheduleTime = date.scheduleTime;
      if (this.bookingType == 2) {
        this.timing = date.bookTime;
      } else if (this.bookingType == 1) {
        this.timing = moment(new Date()).format("hh:mm:ss");
      } else {
        this.endDate = date.endDate;
        this.startDTime = date.startTimeStamp;
        this.endDTime = date.endTimeStamp;
        this.daysLists = date.days;
      }
    } else {
      this.dateChange();
    }

    this.filterList();
    this.mqttConnect();
    // console.log('dfd', this.callType);
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  arrow = 1;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  backClicked() {
    this._location.back();
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: "",
        day: "",
        active: false,
        unformat: ""
      };
      if (i == 1) {
        date_array.day = "Tommorrow";
      } else if (i == 0) {
        date_array.day = "Today";
      } else {
        date_array.day = moment()
          .add(i, "days")
          .format("dddd");
      }
      date_array.date = moment()
        .add(i, "days")
        .format("D");

      date_array.unformat = moment()
        .add(i, "days")
        .format("YYYY-MM-DD HH:mm:ss");

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format("dddd,D MMM Y");
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  TimePicker(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    var nexthour = moment(new Date(),'hh:mm a')
    .add(60,'minutes')
    .format("hh:mm a");
    $(".timePick").val(nexthour);
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      altInput: true,
      dateFormat: "h:i K",
      altFormat: "h:i K",
      time_24hr: true,
      defaultDate: nexthour,
      // minDate: nexthour,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $(".timePick").val(datestr);
      }
    });
  }

  TimeDuration(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    $(".timeduration").val("01:00");
    flatpickr("#" + value, {
      enableTime: true,
      noCalendar: true,
      dateFormat: "H:i",
      altFormat: "H:i",
      defaultDate: "01:00",
      minDate: "01:00",
      // minTime: nexthour,
      time_24hr: true,
      altInput: true,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $(".timeduration").val(datestr);
      }
    });
  }

  startEndTime: any;
  startEndDate: any;
  errMsgs: any;
  selectDays = [];
  bookingShift = [];
  listday = [];

  public shiftDateList = [
    { day: "This Week", days: "0", checked: false, startDate: "", endDate: "" },
    {
      day: "This Month",
      days: "1",
      checked: false,
      startDate: "",
      endDate: ""
    },
    {
      day: "Select Date",
      days: "2",
      checked: false,
      startDate: "",
      endDate: ""
    }
  ];

  dateSelect(i) {
    this.errMsgs = false;
    this.selectDays.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList[i].checked = true;
    this.startEndTime = this.shiftDateList[i].days == "2" ? true : false;
    let today = moment().format("YYYY-MM-DD");
    let endOfWeek = moment()
      .endOf("week")
      .toDate();
    let endOfMonth = moment()
      .endOf("month")
      .toDate();
    if (this.shiftDateList[i].days == "0") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfWeek).format("YYYY-MM-DD");
    } else if (this.shiftDateList[i].days == "1") {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfMonth).format("YYYY-MM-DD");
    } else {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = "";
    }
    let start = moment(
      this.shiftDateList[i].startDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    let end = moment(
      this.shiftDateList[i].endDate,
      "YYYY-MM-DD YYYY-MM-DD"
    ).format("MMM, D Y");
    this.startEndDate = start + " - " + end;
  }

  public shiftDaysList = [
    // { day: "All days of the week", days: "0", checked: false },
    // { day: "All weekdays", days: "1", checked: false },
    // { day: "All weekends", days: "2", checked: false },
    { day: "Select All Days", days: "0", checked: false }
  ];

  daysShift(i) {
    this.errMsgs = false;
    this.shiftDaysList[i].checked = true;
    this.selectDays.forEach(x => {
      x.checked = true;
    });
  }

  public daysList = [
    { day: "Sunday", checked: false },
    { day: "Monday", checked: false },
    { day: "Tuesday", checked: false },
    { day: "Wednesday", checked: false },
    { day: "Thursday", checked: false },
    { day: "Friday", checked: false },
    { day: "Saturday", checked: false }
  ];

  daySelect(i) {
    this.errMsgs = false;
    this.shiftDaysList[0].checked = false;
    if (!this.selectDays[i].checked) {
      this.selectDays[i].checked = true;
    } else {
      this.selectDays[i].checked = false;
    }
  }

  multipleShift(i: number) {
    this.errMsgs = false;
    var stepActive = false;
    if (i != -1) {
      var index = -1;
      if (this.shiftTime == 0) {
        this.selectDays = [];
        index = this.shiftDateList.findIndex(y => y.checked == true);
        if (index > -1) {
          let start = moment(this.shiftDateList[index].startDate, "YYYY-MM-DD");
          let end = moment(this.shiftDateList[index].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          if (this.shiftDateList[index].endDate && days > 0) {
            stepActive = true;
            if (days < 6) {
              for (let i = 0; i <= days; i++) {
                let add = moment()
                  .add(i, "days")
                  .format("dddd");
                let daysIndex = this.daysList.findIndex(m => m.day == add);
                this.selectDays.push(this.daysList[daysIndex]);
              }
            } else {
              this.selectDays = this.daysList;
            }
            // this.selectDays.forEach(x => {
            //   x.checked = false;
            // })
            // console.log(this.selectDays)
          } else {
            stepActive = false;
            this.errMsgs = "Select a end date which greater than start date";
            return false;
          }
        } else {
          stepActive = false;
        }
      } else if (this.shiftTime == 1) {
        index = this.selectDays.findIndex(k => k.checked == true);
        this.listday = [];
        var addday = [];
        this.bookingShift = [];
        if (index > -1) {
          var indexS = this.shiftDateList.findIndex(y => y.checked == true);
          let start = moment(
            this.shiftDateList[indexS].startDate,
            "YYYY-MM-DD"
          );
          let end = moment(this.shiftDateList[indexS].endDate, "YYYY-MM-DD");
          let days = end.diff(start, "days");
          this.selectDays.forEach(m => {
            if (m.checked == true) {
              this.listday.push(m.day);
            }
          });
          for (let k = 0; k <= days; k++) {
            let add = moment()
              .add(k, "days")
              .format("dddd");
            addday.push(add);
          }
          for (var j = 0; j < this.listday.length; j++) {
            for (var k = 0; k < addday.length; k++) {
              if (this.listday[j] == addday[k]) {
                this.bookingShift.push(addday[k]);
              }
            }
          }
          // console.log(this.bookingShift)
          if (this.bookingShift.length > 1) {
            stepActive = true;
          } else {
            stepActive = false;
            this.errMsgs =
              "Minimum 2 shifts required for multiple shift booking.";
            return false;
          }
        } else {
          stepActive = false;
          this.errMsgs = "Select an days";
          return false;
        }
      }
    } else {
      stepActive = true;
      if (this.shiftTime == 0) {
        this.asapSchedule = 0;
      }
    }
    // console.log(i, index, stepActive)
    if (stepActive) {
      this.shiftTime = Math.min(2, Math.max(0, this.shiftTime + i));
    } else {
      this.errMsgs = "Select an option to continue";
    }
  }

  selectDate(i) {
    // console.log(i)
    if (i == "Ds") {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
    }
  }

  asapSchedules(val) {
    $(".timePick").val("");
    $(".timeduration").val("");
    this.errMsgs = false;
    this.shiftTime = 0;
    this.count = 1;
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = 0;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else if (val == 1) {
      this.asapSchedule = 1;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format("dddd,D MMM Y");
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    } else {
      this.asapSchedule = 2;
    }
  }

  dateChange() {
    clearInterval(this.intervals);
    this.errMsgs = false;
    var day = [];
    var time = "";
    var scheduleTime = "";
    var bookTitle = "";
    var bookDate = "";
    var ScheduleDate = "";
    var shiftDuration = "";
    var timeShedule;
    var timeEnd = "";
    if (this.asapSchedule == 1) {
      this.bookingType = 2;
      bookTitle = "Schedule";
      if (this.otherDate == true) {
        let date =
          $(".BtypePicker").val() ||
          moment()
            .add(9, "days")
            .format("YYYY-MM-DD");
        bookDate = moment(date).format("dddd,D MMM Y");
      } else {
        bookDate = this.bookDates;
      }
      this.timing =
        $(".BtypeTimePicker").val() ||
        moment()
          .add(1, "hour")
          .format("hh:mm:ss A");
    } else if (this.asapSchedule == 0) {
      this.bookingType = 1;
      let bookdate = new Date();
      bookTitle = "Book Now";
      bookDate = moment()
        .add(bookdate, "days")
        .format("dddd,D MMM Y");
      this.timing = moment(new Date()).format("hh:mm:ss");
    } else {
      this.bookingType = 3;
      bookTitle = "Booking Shift";
      var endDTime;
      var bookingShift = this.bookingShift.length;
      day = this.listday;
      this.shiftDateList.forEach(x => {
        if (x.checked == true) {
          bookDate = moment(x.startDate, "YYYY-MM-DD").format("dddd,D MMM Y");
          endDTime = moment(x.endDate, "YYYY-MM-DD").format("dddd,D MMM Y");
        }
      });
      this.endDate = endDTime;
      this.daysLists = day;
      // this.noShifts = bookingShift;
    }

    time = $(".timePick").val();
    scheduleTime = $(".timeduration").val();
    this.timing = time;
    ScheduleDate = moment(
      bookDate + " " + time,
      "dddd,D MMM Y hh:mm:ss A"
    ).unix();
    timeEnd = moment(endDTime + " 23:00", "dddd,D MMM Y hh:mm").unix();
    let scheduleTimes = scheduleTime.split(":");
    let schTime = scheduleTimes[0] == "00" ? 1 : scheduleTimes[0];
    timeShedule =
      scheduleTimes[0] == "00"
        ? 0 + Number(scheduleTimes[1])
        : Number(scheduleTimes[0]) * 60 + Number(scheduleTimes[1]);
    shiftDuration = scheduleTimes[0] + " hr :" + scheduleTimes[1];

    let list = {
      bookTitle: bookTitle,
      bookingType: this.bookingType,
      callType:this.callType,
      bookDate: bookDate,
      endDate: endDTime,
      // days: day,
      bookTime: time,
      bookCount: this.scheduleTime,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      startTimeStamp: this.scheduleDateTimeStamp,
      // endTimeStamp: timeEnd,
      scheduleTime: timeShedule || 60,
      shiftDuration: shiftDuration,
      bookingShift: bookingShift || ""
    };

    var Time = moment().format("hh:mm:ss a");
    var startTime = moment(Time, "hh:mm:ss a");
    var endTime = moment(time, "hh:mm:ss a");
    let times = endTime.diff(startTime, "minutes");
    // if (times <= 59) {
    //   this.errMsgs = "Select start time minimum 1 hour from current time.";
    //   return false;
    // }

    if (this.asapSchedule == 0 || (time && scheduleTime)) {
      this.bookTitle = bookTitle;
      this.bookDate = bookDate;
      this.scheduleDateTimeStamp = ScheduleDate;
      this.endDTime = timeEnd;
      this.scheduleTime = timeShedule;
      // this.shiftDuration = shiftDuration;
      this._conf.setItem("bookingDates", JSON.stringify(list));
      $(".modal").modal("hide");
      this.filterList();
    } else {
      this.errMsgs = "Select time and duration";
    }
  }

  offers: any;
  typeBooking() {
    this.startEndDate = false;
    this.startEndTime = false;
    this.shiftDateList.forEach(x => (x.checked = false));
    this.shiftDaysList.forEach(x => (x.checked = false));
    this.errMsgs = false;
    this.asapSchedule = 0;
    this.sendDate.forEach(x => {
      x.active = false;
    });
    this.bookingList = this.providerList.category;
    if (this.providerList.category) {
      this.offers = this.providerList.category;
    }
    console.log(this.bookingList);
    $("#notaryTask").modal("show");
  }

  ngOnDestroy() {
    $("body").removeClass("hideHidden");
    this.filterLists = false;
    // console.log("sfgasdfg");
    clearInterval(this.intervals);
    // this.intervals == 2;
    // this.setInterVal();
  }

  filtersList() {
    clearInterval(this.intervals);
    $("body").toggleClass("hideHidden");
    this.filterLists = !this.filterLists;
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: "",
        nexthour: ""
      };
      hour_array.hour = moment()
        .add(i, "hour")
        .format("h A");
      var curr = moment()
        .add(i, "hour")
        .format("h h");
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, "hour")
        .format("h A");
      this.sendTime.push(hour_array);
    }
  }

  selectProviders() {
    this.selectProvider(this.providerId);
  }

  selectProvider(id) {
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      // console.log(this.callType);
      if (this.callType == 1) {
        this._router.navigate(["./time-slot", this.catId, id]);
      } else {
        this._router.navigate([
          "./",
          this.cityAddress,
          this.catName,
          this.catId,
          id
        ]);
      }
    } else {
      this._missionService.loginOpen(false);
    }
  }

  subCat(index) {
    $(".sideListLi").removeClass("active");
    $("#" + index + "sCat").addClass("active");
    this.subCatId = this.providerList.subCategoryData[index]._id;
  }

  searchProvider(val) {
    clearInterval(this.intervals);
    // let keyName = val.length > 0 || "";
    this.keyName = val;
    this.filterList();
  }

  distanceList(val) {
    clearInterval(this.intervals);
    this.distance = Number(val);
    var input = $(".slider_range").val();
    var value =
      (input - $(".slider_range").attr("min")) /
      ($(".slider_range").attr("max") - $(".slider_range").attr("min"));
    $(".slider_range").css(
      "background-image",
      "-webkit-gradient(linear, left top, right top, " +
        "color-stop(" +
        value +
        ", #01b5f6), " +
        "color-stop(" +
        value +
        ", #C5C5C5)" +
        ")"
    );
  }

  filterContentList() {
    clearInterval(this.intervals);
    this.filterLoader = true;
    if (this.providerList && this.providerList.category) {
      let min = this.providerList.category.minimum_fees;
      let max = this.providerList.category.miximum_fees;
    }
    this.waitForResponse = 1;
    this.filterList();
  }

  errMsg: any;
  ErrMsg: any;
  minPrice(price, maxpe) {
    clearInterval(this.intervals);
    this.errMsg = false;
    // setTimeout(() => {
    if (maxpe < this.minFees) {
      this.minFees = maxpe;
      this.errMsg = "maximum price should be " + maxpe;
    } else {
      if (price > this.minFees) {
        this.minFees = price;
        this.errMsg = "minimum price should be " + price;
      }
    }
    setTimeout(() => {
      this.errMsg = false;
    }, 1500);

    // }, 2000)

    // console.log(min, inP, this.minFees)
  }

  maxPrice(price, maxpe) {
    clearInterval(this.intervals);
    this.errMsg = false;
    // setTimeout(() => {
    if (maxpe < this.maxFees) {
      this.maxFees = maxpe;
      this.errMsg = "maximum price should be " + maxpe;
    } else {
      if (price > this.maxFees) {
        this.maxFees = price;
        this.errMsg = "minimum price should be " + price;
      }
    }
    setTimeout(() => {
      this.errMsg = false;
    }, 1500);

    // }, 2000)
    // console.log(this.maxFees)
  }

  filterList() {
    this.ErrMsg = false;
    let lat = $("#latS").val();
    let lng = $("#lngS").val();
    let list = {
      lat: lat,
      long: lng,
      ip: this.ipAddress,
      // callType:this.callType,
      categoryId: this.catId,
      search: this.keyName || "0",
      // bookingType: this.callType == 1?2:this.bookingType,
      bookingType: this.bookingType,
      subCategoryId: this.subCatId || "0",
      distance: this.distance || 0,
      scheduleDateTimeStamp: "",
      scheduleTime: "",
      // endTimeStamp: 0,
      // days: [],
      minFees: this.minFees || 0,
      maxFees: this.maxFees || 0,
      waitForResponse: this.waitForResponse
    };
    if (this.bookingType == 2) {
      list.scheduleDateTimeStamp = this.scheduleDateTimeStamp;
      list.scheduleTime = this.scheduleTime;
    }
    if (this.bookingType == 3) {
      list.scheduleDateTimeStamp = this.startDTime;
      list['endTimeStamp'] = this.endDTime;
      list.scheduleTime = this.scheduleTime;
      list['days'] = this.daysLists;
    }
    if (this.waitForResponse == 1) {
      this.provideLoader = true;
    }
    
    this._service.providerFilter(list).subscribe(
      (res: any) => {
        $("body").removeClass("hideHidden");
        this.filterLists = false;
        console.log("website/provider/filter", res);
        if (
          res.data &&
          res.data.category &&
          res.data.category.service_type == "1"
        ) {
          this._router.navigate([""]);
        } else {
          if (this.waitForResponse == 1) {
            this.filterLoader = false;
            this.provideLoader = false;
            this.loaderList = true;
            this.providerList = res.data;
            if (
              res.data.category.service_type == "2" &&
              res.data.category.billing_model != "5"
            ) {
              this.minFees = res.data.category.minimum_fees;
              this.maxFees = res.data.category.miximum_fees;
            }
            res.data.provider.forEach(x => {
              x.price = parseFloat(x.price).toFixed(2);
            });
            res.data.provider.sort(a => {
              if (a.status == 1) {
                return -1;
              } else {
                return 1;
              }
            });
            this.timeFunction();
            this.setInterVal();
          }
        }
      },
      err => {
        this.filterLoader = false;
        this.provideLoader = false;
        this.loaderList = true;
        console.log(err.error.message);
        this.ErrMsg = err.error.message;
        setTimeout(() => {
          this.ErrMsg = false;
        }, 3000);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
          // var currentLocation = window.location;
          // this._conf.setItem("pathname", currentLocation.pathname);
          // this._router.navigate(['']);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  timeFunction() {
    var len = this.providerList.provider;
    for (var i = 0; i < len.length; i++) {
      if (this.providerList.provider[i].review[0]) {
        var date = this.providerList.provider[i].review[0].reviewAt;
        var post = date;
        var poston = Number(post);
        var postdate = new Date(poston * 1000);
        var currentdate = new Date();
        var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
        var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
        if (1 < diffDays) {
          this.providerList.provider[i].review[0].reviewAt =
            diffDays + " Days ago";
        } else if (1 <= diffHrs) {
          this.providerList.provider[i].review[0].reviewAt =
            diffHrs + " Hours ago";
        } else {
          this.providerList.provider[i].review[0].reviewAt =
            diffMins + " Minutes ago";
        }
      }
    }
  }

  setInterVal() {
    this.intervals = setInterval(() => {
      console.log("waitForResponse", this.waitForResponse);
      // if (this.waitForResponse == 2) {
      //   clearInterval(this.intervals);
      // } else {
      this.waitForResponse = 0;
      this.filterList();
      // }
    }, 10000);
  }

  mqttConnect() {
    var id = this._conf.getItem("sid");
    this._client = new Paho.MQTT.Client("mqtt.kyhoot.com", 2083, "/mqtt", id);

    this._client.onConnectionLost = (responseObject: Object) => {
      console.log("Connection lost.");
      this._client.connect({ useSSL: true, onSuccess: this.onConnected.bind(this),keepAliveInterval: 10, });
    };

    this._client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log("Message arrived.", JSON.parse(message.payloadString));
      let statusList = JSON.parse(message.payloadString);
      this.providerList = statusList.data;
      if (
        statusList.data.category.service_type == "2" &&
        statusList.data.category.billing_model != "5"
      ) {
        this.minFees = statusList.data.category.minimum_fees;
        this.maxFees = statusList.data.category.miximum_fees;
      }
      statusList.data.provider.forEach(x => {
        x.price = parseFloat(x.price).toFixed(2);
      });
      statusList.data.provider.sort(a => {
        if (a.status == 1) {
          return -1;
        } else {
          return 1;
        }
      });
      this.timeFunction();
    };
    if(this._client.isConnected()){
      this._client.disconnect();
      this._client.connect({ useSSL: true, onSuccess: this.onConnected.bind(this),keepAliveInterval: 10, });
    }
    else{
      this._client.connect({ useSSL: true, onSuccess: this.onConnected.bind(this),keepAliveInterval: 10, });
    }

    // this._client.connect({ useSSL:true, onSuccess: this.onConnected.bind(this) });
  }

  private onConnected(): void {
    var cId = this._conf.getItem("sid");
    this._client.subscribe("provider/" + cId, { qos: 2 });
    console.log("Connected to broker provider.");
  }

  reviewList(id) {
    this._conf.setItem("inCallType", this.callType);
    var catName = this.catName.replace(/[^A-Z0-9]/gi, "_");
    this._router.navigate([
      "./review",
      this.cityAddress,
      catName,
      this.catId,
      id
    ]);
  }

  cancelField() {
    $("#changeLocationId").val(" ");
  }

  changeLocation() {
    var input = document.getElementById("changeLocationId");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.addListener("place_changed", () => {
      var place = autocomplete.getPlace();
      $("#latS").val(place.geometry.location.lat());
      $("#lngS").val(place.geometry.location.lng());
      let placelatlng = {
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      };
      this._conf.setItem("latlng", JSON.stringify(placelatlng));
      this._conf.setItem("place", JSON.stringify(place.name));
    });
  }
}
