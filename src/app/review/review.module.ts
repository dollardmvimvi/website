import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ReviewComponent } from './review.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: ReviewComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    ReviewComponent
  ]
})
export class ReviewModule { }
