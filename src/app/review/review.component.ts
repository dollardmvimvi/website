import { Component, OnInit, HostListener, Inject, NgZone, ViewChild, AfterViewInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { ProviderService } from '../provider/provider.service';
import { HomeService } from '../home/home.service';
import { ProfileService } from '../profile/profile.service';
import { LanguageService } from '../app.language';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { Location, DOCUMENT } from '@angular/common';

declare var $: any;
declare var google: any;

@Component({
  selector: 'review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ProviderService,
    private _serviceHome: HomeService,
    private _serviceProfile: ProfileService,
    private _location: Location,
  ) { }

  loadMore = false;
  btnProvider = false;
  bId: any;
  providerId: any;
  providerRw: any;
  pageno = 0;
  loaderList: any;
  listReview: any;
  listReviews: any = [];
  catId: any;
  catName: any;
  cityAddress: any;
  shimmer: any = [];
  latlng: any;
  headerrestOpen: any;


  ngOnInit() {
    this._missionService.scrollTop(true);
    this.placeHolder();
    this.route.params.subscribe(params => {
      this.cityAddress = params['city'];
      let catName = params['name'];
      this.catId = params['id'];
      this.providerId = params['Pid'];
      this.reviewList(this.providerId);
      let cat = catName.split("_");
      if (cat[0] == "hire") {
        this.btnProvider = true;
        this.bId = cat[1];
        this.listBidd(this.bId, this.providerId);
      } else {
        this.catName = cat[0];
        this.btnProvider = false;
      }
    });
  }

  proStatus: any;
  listBidd(id, pId) {
    this._serviceProfile.bidProviderList(id)
      .subscribe((res: any) => {
        console.log("customer/booking/bId", res);
        // this.proStatus = res.data;
        if (res.data.bidDispatchLog && res.data.bidDispatchLog.length > 0) {
          let index = res.data.bidDispatchLog.findIndex(x => x.providerId == pId);
          this.proStatus = res.data.bidDispatchLog[index];
          this.proStatus.quotedPrice = parseFloat(this.proStatus.quotedPrice).toFixed(2)
          this.proStatus.currencySymbol = res.data.currencySymbol;
        } else {
          this.btnProvider = false;
          this._router.navigate(["./profile"]);
        }
      }, err => {
        console.log(err.error.message)
      })
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  backClicked() {
    this._location.back();
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = $(window).scrollTop();
    if (number > 100) {
      this.headerrestOpen = true;
    } else {
      this.headerrestOpen = false;
    }
  }


  reviewList(id) {
    let latlng = this._conf.getItem("latlng");
    if (latlng) {
      this.latlng = JSON.parse(this._conf.getItem("latlng"));
      console.log(this.latlng)
    }
    this.providerId = id;
    this.reviewListAll();
    let list = {
      providerId: id,
      categoryId: this.catId,
      lat: this.latlng.lat,
      lng: this.latlng.lng
    }
    this.loaderList = false;
    this._service.providerReviews(list)
      .subscribe((res: any) => {
        this.providerRw = true;
        this.loaderList = true;
        console.log("customer/providerDetails", res);
        this.listReview = res.data;
        setTimeout(()=>{
          this.mapProvider(res.data.location['latitude'], res.data.location['longitude']);
        }, 1000)
      }, err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      })

  }

  mapProvider(lat, lng) {
    var latlng = {
      lat:lat,
      lng:lng
    };
    console.log("latlng", latlng);
    var latlng1 = new google.maps.LatLng(parseFloat(latlng.lat), parseFloat(latlng.lng));
    var map = new google.maps.Map(document.getElementById("liveTrackMaps"), {
      // center: { lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng) },
      center: latlng1,
      zoom: 13,
      disableDefaultUI: false,
      mapTypeControl: false
    });

    setTimeout(() => {
      google.maps.event.trigger(map, "resize");
      map.setCenter(latlng);
      var marker = new google.maps.Marker({
        map: map
      });
    }, 300);    
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(latlng.lat, latlng.lng),
      map: map,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: false
    });
    var geoCoder = new google.maps.Geocoder();
    var address  = '';
    geoCoder.geocode({ 'latLng': latlng1 },  (results, status) =>{
      if (status !== google.maps.GeocoderStatus.OK) {
          alert(status);
      }
      // This is checking to see if the Geoeode Status is OK before proceeding
      if (status == google.maps.GeocoderStatus.OK) {
          address = (results[0].formatted_address);
          address ? this.listReview.location['addLine1'] = address :'';
      }
  });
    // console.log("listReview", this.listReview)
  }

  reviewMore() {
    this.pageno += 1;
    this.reviewListAll();
  }

  allProvider() {
    this.providerRw = false;
  }

  reviewListAll() {
    let list = {
      providerId: this.providerId,
      categoryId: this.catId,
      pageNo: this.pageno
    }
    this._service.listAllReview(list)
      .subscribe((res: any) => {
        console.log("customer/providerReview", res);
        let data = res.data.reviews;
        if (data && data.length == 5) {
          this.loadMore = true;
        } else {
          this.loadMore = false;
        }

        for (var i = 0; i < data.length; i++) {
          this.listReviews.push(data[i]);
        }
        this.timeReview();

      }, err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
      })
  }

  timeReview() {
    var len = this.listReviews;
    for (var i = 0; i < len.length; i++) {
      if (this.listReviews) {
        var date = this.listReviews[i].reviewAt;
        // console.log(this.listReviews[i].reviewAt)
        var post = date;
        var poston = Number(post);
        var postdate = new Date(poston * 1000);
        var currentdate = new Date();
        var timeDiff = Math.abs(postdate.getTime() - currentdate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var diffHrs = Math.round((timeDiff % 86400000) / 3600000);
        var diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
        if (1 < diffDays) {
          this.listReviews[i].reviewAts = diffDays + " Days ago";
        } else if (1 <= diffHrs) {
          this.listReviews[i].reviewAts = diffHrs + " Hours ago";
        } else {
          this.listReviews[i].reviewAts = diffMins + " Minutes ago";
        }
      }
    }
  }

  selectProvider() {   
    var loginFlag = this._conf.getItem("isLogin");
    if (loginFlag == "true") {
      const callType = Number(this._conf.getItem("inCallType"));
      if (callType == 1) {
        this._router.navigate(["./time-slot", this.catId, this.providerId]);
      } else {
      this._router.navigate(["./", this.cityAddress, this.catName, this.catId, this.providerId]);
      }
    } else {
      this._missionService.loginOpen(false);
    }
  }

  hireProvider() {
    let list = {
      bookingId: this.bId,
      providerId: this.providerId,
    }
    this._serviceProfile.hireProvider(list)
      .subscribe((res: any) => {
        console.log("customer/responseBooking", res);
        this._router.navigate(["./profile"]);
      }, err => {
        console.log(err.error.message)
      })
  }

}
