import { Component, OnInit, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy,DoCheck, AfterContentInit, AfterViewChecked } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewpriceService } from './viewprice.service';
import { HomeService } from '../home/home.service';
import { LanguageService } from '../app.language';
import { MissionService } from '../app.service';
import { Configuration } from '../app.constant';
import { Location } from '@angular/common';
import { reduce } from 'rxjs/operators';
// import { FirsCharUpperCasePipe } from '../firs-char-upper-case.pipe';
// import { lstat } from 'fs';

declare var $: any;
declare var moment: any;
declare var flatpickr: any;

@Component({
  selector: 'viewprice',
  templateUrl: './viewprice.component.html',
  styleUrls: ['./viewprice.component.css'],
  // pipes: [FirsCharUpperCasePipe]      
  // changeDetection: ChangeDetectionStrategy.OnPush,
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ViewpriceComponent implements OnInit, AfterViewChecked {
  constructor(
    private _lang: LanguageService,
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: ViewpriceService,
    private _serviceHome: HomeService,
    private _location: Location,
    private changeDetection: ChangeDetectorRef
  ) {}

  catId: any;
  catName: any;
  providerId: any;
  serviceList: any;
  cartList: any;
  action = 1;
  catSearch: any;
  subCatList: any;
  providerList: any;
  cartListActive: any;
  qtyAllow: any;
  descList: any;
  serviceProvider = false;
  ratingTrue = false;
  shimmer: any = [];
  loaderList = false;
  serviceType: number;
  serviceTypehr: any;

  service_type: any;
  billing_model: any;
  hourlyList: any;
  fixedList: any;
  cartCount: any;

  sendDate: any = [];
  sendTime: any = [];
  asapSchedule = 0;
  shiftTime = 0;
  endDate: any;
  startDTime: any;
  endDTime: any;
  shiftDuration: any;
  daysLists: any;
  bookingList = false;
  timing: any;
  bookingType: any;
  callType = 2;
  count = 1;
  bookDate: any;
  bookTitle: any;
  otherDate = false;
  scheduleDateTimeStamp: any;
  scheduleTime: any;
  bookDates: any;
  errMsg: any;
  minHours: any = 1;
  qty = 1;
  languageSelction:any;
  ngOnInit() {
    flatpickr('.BtypePicker', {
      altInput: true,
      enableTime: false,
      dateFormat: 'Y-m-d',
      altFormat: 'Y-m-d',
      minDate: moment()
        .add(9, 'days')
        .format('YYYY-MM-DD'),
      allowInput: false
    });

    /** repeat booking **/
    var startpicker = flatpickr('.repeatStartTime', {
      altInput: true,
      dateFormat: 'Y-m-d',
      altFormat: 'Y-m-d',
      defaultDate: moment().format('YYYY-MM-DD'),
      allowInput: false,
      enableTime: false,
      time_24hr:false,
      minDate: 'today',
      maxDate: $('.repeatEndTime').attr('value'),
      onClose: (selectedDates, dateStr, instance) => {
        // endpicker.set('minDate', moment(dateStr).add(1, 'days').format('YYYY-MM-DD'));
      },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $('.repeatStartTime').val(datestr);
        this.shiftDateList[2].startDate = moment(datestr).format('YYYY-MM-DD');
      }
    });

    var endpicker = flatpickr('.repeatEndTime', {
      altInput: true,
      dateFormat: 'Y-m-d',
      enableTime: false,
      altFormat: 'Y-m-d',
      defaultDate: 'Z',
      time_24hr:false,
      minDate: $('.repeatStartTime').attr('value'),
      allowInput: false,
      onClose: (selectedDates, dateStr, instance) => {
        // startpicker.set('maxDate', dateStr);
      },
      onChange: (dateobj, datestr) => {
        this.errMsgs = false;
        let date = $('.repeatEndTime').val(datestr);
        this.shiftDateList[2].endDate = moment(datestr).format('YYYY-MM-DD');
      }
    });

    this._missionService.scrollTop(true);
    this.placeHolder();
    let back = this._conf.getItem('backClicked');
    this.route.params.subscribe(params => {
      this.catId = params['id'];
      this.catName = params['name'];
      this.providerId = params['Pid'];
      if (back == '1') {
        if (this.providerId != '0') {
          this._location.back();
        } else {
          this._router.navigate(['']);
        }
        this._conf.removeItem('backClicked');
      } else {
        this.listService(this.catId);
        this.getDate();
        this.getTimes();
        if (this._conf.getItem('bookingDates')) {
          var date = JSON.parse(this._conf.getItem('bookingDates'));
          console.log('date', date);
          this.bookTitle = date.bookTitle;
          this.bookDate = date.bookDate;
          this.bookingType = date.bookingType;
          this.callType = 2,
          this.scheduleDateTimeStamp = date.scheduleDateTimeStamp;
          this.scheduleTime = date.scheduleTime;
          if (this.bookingType == 2) {
            this.timing = date.bookTime;
            this.shiftDuration = date.shiftDuration;
          } else if (this.bookingType == 1) {
            this.timing = moment(new Date()).format('hh:mm:ss');
          } else {
            this.endDate = date.endDate;
            this.startDTime = date.startTimeStamp;
            this.endDTime = date.endTimeStamp;
            this.shiftDuration = date.shiftDuration;
            this.daysLists = date.days;
          }
        } else {
          this.dateChange();
        }
      }
    });
    // this.route.queryParams.subscribe(params =>{
    //     this.minHours = params['minHours'];
    //     //  this.minHours ? this.qty = this.minHours :'';
    // });
    this.minHours = parseInt(this._conf.getItem('minHours'));
    var loginFlag = this._conf.getItem('sessionToken');
    // loginFlag? this.getSurgePricing() : '';
    console.log("loginFlag",loginFlag);
    this.languageSelction = this._conf.getItem('lang')
  }
  latlng:any;
  surgePrice:any = 1;
  getSurgePricing(){
    let latlng = this._conf.getItem("latlng");
    (latlng) ? 
      this.latlng = JSON.parse(this._conf.getItem("latlng")):'';
  
    let body = {
      bookingType: this.bookingType,
      latitude:this.latlng.lat,
      longitude:this.latlng.lng,
      categoryId:this.catId
    }
    this._service.getsurgePrice(body).subscribe(res =>{
        console.log("surgePrice ", res);
        this.surgePrice = res['data'].surgePrice;
    },error =>{
           console.log("surge price error", error)
    });
  }
  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  addHour(i: number) {
    this.count = Math.min(Math.max(1, this.count + i));
  }

  arrow = 2;
  arrowChange(i: number) {
    this.arrow = Math.min(3, Math.max(1, this.arrow + i));
    // console.log(this.arrow)
  }

  backClicked() {
    if (this.providerId != '0') {
      this._location.back();
    } else {
      this._router.navigate(['']);
    }
  }

  getDate() {
    let daysRequired = 8;
    for (let i = 1; i <= daysRequired; i++) {
      // console.log(this.send_date);
      var date_array = {
        date: '',
        day: '',
        active: false,
        unformat: ''
      };
      if (i == 1) {
        date_array.day = 'Tommorrow';
      } else if (i == 0) {
        date_array.day = 'Today';
      } else {
        date_array.day = moment()
          .add(i, 'days')
          .format('dddd');
      }
      date_array.date = moment()
        .add(i, 'days')
        .format('D');

      date_array.unformat = moment()
        .add(i, 'days')
        .format('YYYY-MM-DD HH:mm:ss');

      this.sendDate.push(date_array);
      this.sendDate[0].active = true;
      let date = this.sendDate[0].unformat;
      this.bookDate = moment(date).format('dddd,D MMM Y');
    }
    // var asapNow = moment().format('YYYY-MM-DD HH:mm:ss');
    // this._conf.setItem("bookingDate", asapNow);
    // console.log(this.sendDate);
  }

  TimePicker(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    var nexthour = moment()
      .add(1, 'hour')
      .add(1, 'minutes')
      .format('hh a');
    $('.timePick').val(nexthour);
    flatpickr('#' + value, {
      enableTime: true,
      noCalendar: true,
      altInput: true,
      dateFormat: 'h:i',
      altFormat: 'h:i',
      time_24hr: true,
      defaultDate: nexthour,
      // minDate: nexthour,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $('.timePick').val(datestr);
      }
    });
  }

  TimeDuration(event) {
    this.errMsgs = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    // console.log(value, event.target.value.length);
    $('.timeduration').val('01:00');
    flatpickr('#' + value, {
      enableTime: true,
      noCalendar: true,
      dateFormat: 'h:i',
      altFormat: 'h:i',
      defaultDate: '01:00',
      minDate: '01:00',
      // minTime: nexthour,
      time_24hr: true,
      timeFormat: 'h:i',
      altInput: true,
      minuteIncrement: 30,
      onChange: (dateobj, datestr) => {
        // let Time = $("#" + value).val();
        $('.timeduration').val(datestr);
      }
    });
  }

  startEndTime: any;
  startEndDate: any;
  errMsgs: any;
  selectDays = [];
  bookingShift = [];
  listday = [];
 //Card #299, issue: Norwegian translation is missing. WEB, fixedBy: sowmya, date: 3-1-20
  public shiftDateList = [
    { day: {en:'This Week',no:'Denne uka'}, days: '0', checked: false, startDate: '', endDate: '' },
    {
      day: {en:'This Month',no:'Denne måneden'},
      days: '1',
      checked: false,
      startDate: '',
      endDate: ''
    },
    {
      day: {en:'Select Date',no:'Velg dato'},
      days: '2',
      checked: false,
      startDate: '',
      endDate: ''
    }
  ];
  //cdfhvdjfkv

  dateSelect(i) {
    this.errMsgs = false;
    this.selectDays.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList.forEach(x => {
      x.checked = false;
    });
    this.shiftDateList[i].checked = true;
    this.startEndTime = this.shiftDateList[i].days == '2' ? true : false;
    let today = moment().format('YYYY-MM-DD');
    let endOfWeek = moment()
      .endOf('week')
      .toDate();
    let endOfMonth = moment()
      .endOf('month')
      .toDate();
    if (this.shiftDateList[i].days == '0') {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfWeek).format('YYYY-MM-DD');
    } else if (this.shiftDateList[i].days == '1') {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = moment(endOfMonth).format('YYYY-MM-DD');
    } else {
      this.shiftDateList[i].startDate = today;
      this.shiftDateList[i].endDate = '';
    }
    let start = moment(
      this.shiftDateList[i].startDate,
      'YYYY-MM-DD YYYY-MM-DD'
    ).format('MMM, D Y');
    let end = moment(
      this.shiftDateList[i].endDate,
      'YYYY-MM-DD YYYY-MM-DD'
    ).format('MMM, D Y');
    this.startEndDate = start + ' - ' + end;
  }

  public shiftDaysList = [
    // { day: "All days of the week", days: "0", checked: false },
    // { day: "All weekdays", days: "1", checked: false },
    // { day: "All weekends", days: "2", checked: false },
    { day: 'Select All Days', days: '0', checked: false }
  ];

  daysShift(i) {
    this.errMsgs = false;
    this.shiftDaysList[i].checked = true;
    this.selectDays.forEach(x => {
      x.checked = true;
    });
  }

  public daysList = [
    { day: 'Sunday', checked: false },
    { day: 'Monday', checked: false },
    { day: 'Tuesday', checked: false },
    { day: 'Wednesday', checked: false },
    { day: 'Thursday', checked: false },
    { day: 'Friday', checked: false },
    { day: 'Saturday', checked: false }
  ];

  daySelect(i) {
    this.errMsgs = false;
    this.shiftDaysList[0].checked = false;
    if (!this.selectDays[i].checked) {
      this.selectDays[i].checked = true;
    } else {
      this.selectDays[i].checked = false;
    }
  }

  multipleShift(i: number) {
    this.errMsgs = false;
    var stepActive = false;
    if (i != -1) {
      var index = -1;
      if (this.shiftTime == 0) {
        this.selectDays = [];
        index = this.shiftDateList.findIndex(y => y.checked == true);
        if (index > -1) {
          let start = moment(this.shiftDateList[index].startDate, 'YYYY-MM-DD');
          let end = moment(this.shiftDateList[index].endDate, 'YYYY-MM-DD');
          let days = end.diff(start, 'days');
          //card#401, issue: 401-multiple-shift-per-week, fixed: sowmyasv, date: 3-2-20
          if (this.shiftDateList[index].endDate && days >= 0) {
            stepActive = true;
            if (days < 6) {
              for (let i = 0; i <= days; i++) {
                let add = moment()
                  .add(i, 'days')
                  .format('dddd');
                let daysIndex = this.daysList.findIndex(m => m.day == add);
                this.selectDays.push(this.daysList[daysIndex]);
              }
            } else {
              this.selectDays = this.daysList;
            }
            this.shiftDaysList[i].checked = false;
            this.selectDays.forEach(x => {
              x.checked = false;
            })
            // console.log(this.selectDays)
          } else {
            stepActive = false;
            this.languageSelction == "English" ? this.errMsgs = "Select a end date which greater than start date" : this.errMsgs = "Velg en sluttdato som er større enn startdato";
            // this.errMsgs = 'Select a end date which greater than start date';
            return false;
          }
        } else {
          stepActive = false;
        }
      } else if (this.shiftTime == 1) {
        index = this.selectDays.findIndex(k => k.checked == true);
        this.listday = [];
        var addday = [];
        this.bookingShift = [];
        if (index > -1) {
          var indexS = this.shiftDateList.findIndex(y => y.checked == true);
          let start = moment(
            this.shiftDateList[indexS].startDate,
            'YYYY-MM-DD'
          );
          let end = moment(this.shiftDateList[indexS].endDate, 'YYYY-MM-DD');
          let days = end.diff(start, 'days');
          this.selectDays.forEach(m => {
            if (m.checked == true) {
              this.listday.push(m.day);
            }
          });
          for (let k = 0; k <= days; k++) {
            let add = moment()
              .add(k, 'days')
              .format('dddd');
            addday.push(add);
          }
          for (var j = 0; j < this.listday.length; j++) {
            for (var k = 0; k < addday.length; k++) {
              if (this.listday[j] == addday[k]) {
                this.bookingShift.push(addday[k]);
              }
            }
          }
          // console.log(this.bookingShift)
          if (this.bookingShift.length >= 1) {
            stepActive = true;
          } else {
            stepActive = false;
            this.languageSelction == "English" ? this.errMsgs = "Minimum 2 shifts required for multiple shift booking." : this.errMsgs = "Minimum 2 skift kreves for bestilling av flere skift.";
            // this.errMsgs =
            //   'Minimum 2 shifts required for multiple shift booking.';
            return false;
          }
        } else {
          stepActive = false;
          this.languageSelction == "English" ? this.errMsgs = "Select an days" : this.errMsgs = "Velg en dag";
          // this.errMsgs = 'Select an days';
          return false;
        }
      }
    } else {
      stepActive = true;
      if (this.shiftTime == 0) {
        this.asapSchedule = 0;
      }
    }
    // console.log(i, index, stepActive)
    if (stepActive) {
      this.shiftTime = Math.min(2, Math.max(0, this.shiftTime + i));
    } else {
      // this.errMsgs = 'Select an option to continue';
      this.languageSelction == "English" ? this.errMsgs = "Select an option to continue" : this.errMsgs = "Velg et alternativ for å fortsette";
    }
  }

  selectDate(i) {
    // console.log(i)
    if (i == 'Ds') {
      this.otherDate = true;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else {
      this.otherDate = false;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[i].active = true;
      // this._conf.setItem("bookingDate", this.sendDate[i].unformat);
      let date = this.sendDate[i].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
    }
  }

  asapSchedules(val) {
    $('.timePick').val('');
    $('.timeduration').val('01:00');
    this.errMsgs = false;
    this.shiftTime = 0;
    this.count = 1;
    this.otherDate = false;
    if (val == 0) {
      this.asapSchedule = 0;
      this.sendDate.forEach(x => {
        x.active = false;
      });
    } else if (val == 1) {
      this.asapSchedule = 1;
      this.sendDate.forEach(x => {
        x.active = false;
      });
      this.sendDate[0].active = true;
      // console.log(this.sendDate[0].unformat);
      let date = this.sendDate[0].unformat;
      this.bookDates = moment(date).format('dddd,D MMM Y');
      // this._conf.setItem("bookingDate", this.sendDate[0].unformat);
    } else {
      this.asapSchedule = 2;
    }
  }

  dateChange() {
    this.errMsgs = false;
    var day = [];
    var time = '';
    var scheduleTime = '';
    var bookTitle = '';
    var bookDate = '';
    var ScheduleDate = '';
    var shiftDuration = '';
    var timeShedule;
    var timeEnd = '';
    if (this.asapSchedule == 1) {
      this.bookingType = 2;
      bookTitle = 'Schedule';
      if (this.otherDate == true) {
        let date =
          $('.BtypePicker').val() ||
          moment()
            .add(9, 'days')
            .format('YYYY-MM-DD');
        bookDate = moment(date).format('dddd,D MMM Y');
      } else {
        bookDate = this.bookDates;
      }
      this.timing =
        $('.BtypeTimePicker').val() ||
        moment()
          .add(1, 'hour')
          .format('hh:mm:ss A');
    } else if (this.asapSchedule == 0) {
      this.bookingType = 1;
      let bookdate = new Date();
      bookTitle = 'Book Now';
      bookDate = moment()
        .add(bookdate, 'days')
        .format('dddd,D MMM Y');
      this.timing = moment(new Date()).format('hh:mm:ss');
    } else {
      this.bookingType = 3;
      bookTitle = 'Booking Shift';
      var endDTime;
      var bookingShift = this.bookingShift.length;
      day = this.listday;
      this.shiftDateList.forEach(x => {
        if (x.checked == true) {
          bookDate = moment(x.startDate, 'YYYY-MM-DD').format('dddd,D MMM Y');
          endDTime = moment(x.endDate, 'YYYY-MM-DD').format('dddd,D MMM Y');
        }
      });
      this.endDate = endDTime;
      this.daysLists = day;
      // this.noShifts = bookingShift;
    }

    time = $('.timePick').val();
    // scheduleTime = $('.timeduration').val();
    scheduleTime = "01:00";
    this.timing = time;
    ScheduleDate = moment(
      bookDate + ' ' + time,
      'dddd,D MMM Y hh:mm:ss A'
    ).unix();
    timeEnd = moment(endDTime + ' 23:00', 'dddd,D MMM Y hh:mm').unix();
    let scheduleTimes = scheduleTime.split(':');
    let schTime = scheduleTimes[0] == '00' ? 1 : scheduleTimes[0];
    timeShedule =
      scheduleTimes[0] == '00'
        ? 0 + Number(scheduleTimes[1])
        : Number(scheduleTimes[0]) * 60 + Number(scheduleTimes[1]);
    shiftDuration = scheduleTimes[0] + ' hr :' + scheduleTimes[1];

    let list = {
      bookTitle: bookTitle,
      bookingType: this.bookingType,
      callType: this.callType,
      bookDate: bookDate,
      endDate: endDTime,
      days: day,
      bookTime: time,
      bookCount: this.scheduleTime,
      scheduleDateTimeStamp: this.scheduleDateTimeStamp,
      startTimeStamp: this.scheduleDateTimeStamp,
      endTimeStamp: timeEnd,
      scheduleTime: timeShedule || 60,
      shiftDuration: shiftDuration,
      bookingShift: bookingShift || ''
    };
    var Time = moment().format('hh:mm:ss a');
    var startTime = moment(Time, 'hh:mm:ss a');
    var endTime = moment(time, 'hh:mm:ss a');
    let times = endTime.diff(startTime, 'minutes');
    // if (times <= 59) {
    //   this.errMsgs = 'Select start time minimum 1 hour from current time.';
    //   return false;
    // }

    if (this.asapSchedule == 0 || (time && scheduleTime)) {
      this.bookTitle = bookTitle;
      this.bookDate = bookDate;
      this.scheduleDateTimeStamp = ScheduleDate;
      this.endDTime = timeEnd;
      this.scheduleTime = timeShedule;
      this.shiftDuration = shiftDuration;
      this._conf.setItem('bookingDates', JSON.stringify(list));
      $('.modal').modal('hide');
      if((this.bookingType == 2 || this.bookingType == 3) && (this.serviceTypehr == 2)){
        let schTime = this.shiftDuration.split(' ');
        this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
        this.addCartList('1',1);
       }else{
        this.getCartList(this.catId);
       }

    } else {
      this.languageSelction == "English" ? this.errMsgs = 'Select time': this.errMsgs = "Velg tid";
    }
  }
  offers: any;
  typeBooking() {
    this.startEndDate = false;
    this.startEndTime = false;
    this.shiftDateList.forEach(x => (x.checked = false));
    this.shiftDaysList.forEach(x => (x.checked = false));
    this.errMsgs = false;
    this.asapSchedule = 0;
    this.sendDate.forEach(x => {
      x.active = false;
    });
    this.bookingList = this.subCatList;
    if (this.subCatList.offers) {
      this.offers = this.subCatList.offers;
    }
    console.log(this.bookingList);
    $('#notaryTask').modal('show');
  }

  // function for displaying time
  getTimes() {
    let hoursRequired = 46;
    for (let i = 0; i <= hoursRequired; i++) {
      var hour_array = {
        hour: '',
        nexthour: ''
      };
      hour_array.hour = moment()
        .add(i, 'hour')
        .format('h A');
      var curr = moment()
        .add(i, 'hour')
        .format('h h');
      var current_hour = i;
      current_hour++;
      hour_array.nexthour = moment()
        .add(current_hour, 'hour')
        .format('h A');
      this.sendTime.push(hour_array);
    }
  }
  selectingServices: any;
  ServicesList: any = [];
  listService(id) {
    let list = {
      catId: id,
      providerId: this.providerId
    };
    this.loaderList = false;
    this._service.viewServiceList(list).subscribe(
      (res: any) => {
        console.log('customer/services', res);
        this.serviceList = res.data;
        // res && res.data ? this.serviceList = this.makeFirstCharUpper(res.data) :'';
        this.subCatList = res.categoryData;
        this.providerList = res.providerData;
        this.subCatList.btnActive = true;
        this.subCatList.loader = false;
        this.service_type = this.subCatList.service_type;
        this.billing_model = this.subCatList.billing_model;
        // this.minHours = res.categoryData.minimum_hour;
        this.selectingServices =  res.categoryData.selectService;
        console.log("this.selectingServices", typeof this.selectingServices,  this.selectingServices);
        if (this.billing_model != 3 && this.billing_model != 4) {
          this.loaderList = true;
        }
        // setTimeout(()=>{
          // res && res.data ? (this.makeFirstCharUpper(res.data),this.changeDetection.detectChanges()):'';
          //Card #365 , issues:old cart are not showing properly, fixedBY:sowmyaSV, date: 15-1-20
          for (var i = 0; i < this.serviceList.length; i++) {
            this.serviceList[i].service.forEach(x => {
              x.btnActive = true;
              x.qty = x.quantity;
            });
          }
        // },1000);
        this.getCartList(this.catId);
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }
  // to get old cart
  getCartList(id) { 
    if (this.bookingType == 2 || this.bookingType == 3) {     
      let schTime = this.shiftDuration.split(' ');
      this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
    } 
   
    let list = {
      catId: id,
      providerId: this.providerId,
      // callType:2,
      bookingType:this.bookingType,
      quntity: this.qty 
    };
    this._service.getCart(list).subscribe(
      (res: any) => {
        console.log('customer/get-cart', res);
        this.cartList = res.data;
        console.log("this.cartList", this.cartList);
        this.serviceTypehr = res.data.serviceType;
        if (this.billing_model == 3 || this.billing_model == 4) {
          if (res.data.totalQuntity == 0) {
            if ((this.bookingType == 2 || this.bookingType == 3) && (this.serviceTypehr == 2)) {
              let schTime = this.shiftDuration.split(' ');
              this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
              this.addCartList('1',1);
            } else {
              this.addCart('1', 1, 0, 0);
            }
          } else {
            var loginFlag = this._conf.getItem('isLogin');
            if (loginFlag == 'true') {
              this.checkOut();
              // this.getSurgePricing();
            } else {
              this._missionService.loginOpen(false);
            }
          }
        }
        // this.listService(this.catId);
        var cartActive = false;
        var len = [];
        this.cartList.item.forEach(x => {
          if (x.quntity > 0) {
            var serviceId = x.serviceId;
            // if (this.cartList.serviceType == 1) {
              //Card #356 , issues:fixed services selection, fixedBY:sowmyaSV, date: 15-1-20
              if (x.serviceType == 1) {
              for (var i = 0; i < this.serviceList.length; i++) {
                var index = this.serviceList[i].service.findIndex(
                  x => x._id == serviceId
                );
                
                if (index > -1) {
                  this.serviceList[i].service[index].btnActive = false;
                  this.serviceList[i].service[index].qty = x.quntity;
                  this.subCatList.btnActive = true;
                }
              }
            } else {
              this.subCatList.btnActive = false;
              this.subCatList.qty = x.quntity;
              for (var i = 0; i < this.serviceList.length; i++) {
                this.serviceList[i].service.forEach(x => {
                  x.btnActive = true;
                });
              }
              !this.deleteCartForFirst ? this.deleteOldCart() :'';
            }
            len.push(x);
            this.cartCount = len.length;
            cartActive = true;
          } else {
            var serviceId = x.serviceId;
            if (x.serviceType == 1) {
              for (var i = 0; i < this.serviceList.length; i++) {
                var index = this.serviceList[i].service.findIndex(
                  x => x._id == serviceId
                );

                if (index > -1) {
                  this.serviceList[i].service[index].btnActive = true;
                  this.serviceList[i].service[index].qty = x.quntity;
                  this.subCatList.btnActive = true;
                }
              }
            } else {
              this.subCatList.btnActive = true;
              this.subCatList.qty = x.quntity;
              for (var i = 0; i < this.serviceList.length; i++) {
                this.serviceList[i].service.forEach(x => {
                  x.btnActive = true;
                });
              }
              this.reduceCart();
            }
          }
        });
        
        if (cartActive) {
          this.cartListActive = true;
        } else {
          this.cartListActive = false;
        }
      },
      err => {
        this.cartCount = 0;
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
        if (err.status == 409) {
          this.loaderList = true;
          $("#resetCart").modal("show");
          this.errMsg = error.message;
        }
        if (
          (err.status == 416) &&
          (this.billing_model == 3 || this.billing_model == 4)
        ) {
          console.log("dsjfsdvdbvd")
          this.addCart('1', 1, 0, 0);
        }
        // else if( (err.status == 416) &&
        // (this.billing_model == 2)){
        //   this.reduceCart();
        // }
        // this.addCart('1', this.minHours,0,0);
      }
    );
  }
  // for single services deletes others which has the carts already
  deleteOtherServices(subCatInx,serviceInx){
    console.log("error for deleting cart")
    let list = {};
    let id = this.serviceList[subCatInx].service[serviceInx]._id;
    this.serviceList[subCatInx].service.forEach((element,index) => {
      if(element._id !== id){
        list['cartId'] = id;
        this._service.deleteCart(list).subscribe(res =>{
          res ? this.serviceList[subCatInx].service[index].btnActive = true : '';
        },error =>{
          console.log("error for deleting cart",error)
        })
      }
    });
  }
  addResetCart() {    
    this.addCart('1', 1, 0, 0);
  }

  addMinusCart(id, action, index) {
    var maxQty = this.cartList.item[index].maxquantity;
    var Qty = this.cartList.item[index].quntity;
    maxQty == 0 ? maxQty = parseInt(this.cartList.item[index].plusXCostForQuantity) : '';
    console.log("id", id)
    if (
      (action == 1 && maxQty >= Qty) ||
      (action == 2 && maxQty >= Qty) ||
      action == 0 ||
      this.cartList.serviceType == 2
    ) {
      this.action = action;
      this.serviceType = this.cartList.serviceType;
      if(id == "1"){
        Qty > this.minHours && action == 2 ? this.addCartList(id,1) : action == 1 ?  this.addCartList(id,1) :'';
      }
      else
      {
        // this.cartList.item[index].disableBtn = true;
        this.addCartList(id,1);
      }
      // this.addCartList(id,1);
      this.cartList.item[index].loader = true;
      setTimeout(() => {
        if (action == 1) {
          this.cartList.item[index].quntity += 1;
        } else {
          if(id == "1"){
            Qty > this.minHours && this.cartList.item[index].quntity > 0 ? this.cartList.item[index].quntity -= 1 : (this.shwoMinHourErr = true,setTimeout(()=>{
              this.shwoMinHourErr = false;
            },4000));
          }
          else{
              // console.log("this.cartList.item[index].quntity", this.cartList.item[index].quntity)
              this.cartList.item[index].quntity > 0 ?  this.cartList.item[index].quntity -= 1 :'';
              // console.log("this.cartList.item[index].quntity 1", this.cartList.item[index].quntity)
          }
        }
        this.cartList.item[index].loader = false;
      }, 500);
    } else {
      this.cartList.item[index].maxqty = true;
      setTimeout(() => {
        this.cartList.item[index].maxqty = false;
      }, 1500);
    }
  }
  shwoMinHourErr:any;
  addCart(id, action, i, index) {
    console.log("addCart", id,action, this.serviceList)
    var loginFlag = this._conf.getItem('isLogin');
    if (loginFlag) {
      if (this.serviceList && this.serviceList.length > 0 && this.serviceList[i].service) {
        var maxQty = this.serviceList[i].service[index].maxquantity;
        var Qty = this.serviceList[i].service[index].qty;
        var QtyAction = Number(this.serviceList[i].service[index].quantity);
        //card#404, issue: 404-personal-assistant-category-error (service) ,fixed: sowmyasV, date: 3-2-20
        maxQty == 0 ? maxQty = parseInt(this.serviceList[i].service[index].plusXCostForQuantity) : '';
        console.log("maxQty", maxQty, Qty,QtyAction)
      }
      if (
        (action == 1 && maxQty >= Qty) ||
        (action == 1 && QtyAction == 0) ||
        (action == 2 && maxQty >= Qty) ||
        action == 0 ||
        id == '1'
      ) {
        if (id == '1') {
          this.serviceType = 2;
          this.subCatList.loader = true;
          setTimeout(() => {
            this.subCatList.loader = false;
          }, 1500);
        } else {
          this.serviceType = 1;
          this.deleteCartForFirst = false;
          this.serviceList[i].service[index].loader = true;
          setTimeout(() => {
            this.serviceList[i].service[index].loader = false;
          }, 1500);
        }
        this.action = action;
        if ((this.bookingType == 2 || this.bookingType == 3) && (this.serviceType == 2)) {
          let schTime = this.shiftDuration.split(' ');
          this.qty = schTime[0] == '00' ? 1 : Number(schTime[0]);
          // console.log("sdf", this.qty, this.shiftDuration, schTime)
        }
        console.log("this.selectingServices",this.selectingServices,Qty, this.minHours);
        // this.addCartList(id,1);
        if(id == '1'){
          !Qty ? Qty = 1 : (Qty = this.minHours);
          console.log("this.selectingServices 1 ",this.selectingServices,Qty, action);
          Qty > this.minHours && action == 0 ? this.addCartList(id,1) : Qty < this.minHours && action == 1 ?
           this.addCartList(id,1) : '';
           Qty == this.minHours && action == 0 ? this.addCartList(id,1) : Qty == this.minHours && action == 1 ?
           this.addCartList(id,1) : Qty == this.minHours && action == 2 ?
          (this.shwoMinHourErr = true,setTimeout(()=>{
            this.shwoMinHourErr = false;
          },4000)) :'' ;
          // Qty == this.minHours && action == 0 ? this.addCartList(id,1) : Qty == this.minHours && action == 1 ?
          //  this.addCartList(id,1) : 
          // (this.shwoMinHourErr = true
          // //   setTimeout(()=>{
          // //   this.shwoMinHourErr = false;
          // // },4000)
          // )  ;
        }
        else{
          console.log("this.selectingServices 3",this.selectingServices,Qty, this.serviceList[i]);
          id == '1' ? this.addCartList(id,0) : this.addCartList(id,1);
        }
        // this.selectingServices === "1" ?  this.deleteOtherServices(i,index) :'';
        this.serviceList && this.serviceList[i].selectService  == 1 ? this.deleteOtherServices(i,index) :'';
      } else {
        this.serviceList[i].service[index].maxqty = true;
        setTimeout(() => {
          this.serviceList[i].service[index].maxqty = false;
        }, 1500);
      }
    } else {
      this._missionService.loginOpen(false);
    }
  }

  addCartList(id,val) {
    // id === '1' ? this.qty = this.minHours : this.qty = 1;
    // this.qty = 1;
    // val != 0 ? this.qty = 1 :this.qty = this.minHours;
    val == 1 ? this.qty = 1 : this.qty = this.minHours;
    let list = {
      serviceType: this.serviceType || this.serviceTypehr || 2,
      bookingType: this.bookingType,
      categoryId: this.catId,
      providerId: this.providerId,
      serviceId: id,
      quntity: this.qty,
      action: this.action,
      // callType:2
    };
    !list['serviceType'] ?  list['serviceType'] = 2 :'';
    if (this.action == 0) {
      list.action = 2;
    }
    this._service.addCart(list).subscribe(
      (res: any) => {
        console.log('customer/cart sfsfd', res);
        // this.qty = 1;
        if (this.billing_model == 3 || this.billing_model == 4) {
          this.checkOut();
          // this.minHours = 1;
          // this.getSurgePricing();
        } else {
          // this.cartList = res.data;
          this.getCartList(this.catId);
        }
      },
      err => {
        var error = err.error;
        console.log(error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  reviewList(id) {
    let city = this.subCatList.city;
    var catName = this.catName.replace(/[^A-Z0-9]/gi, '_');
    this._router.navigate(['./review', city, catName, this.catId, id]);
  }

  descriptionList(list) {
    this.descList = list;
    $('#descList').modal('show');
  }

  checkOut() {
    this._conf.setItem("inCallType", 2);
    this._router.navigate(['./checkout', this.catId, this.providerId],{queryParamsHandling:'merge', queryParams:{minHours: this.minHours}});
  }
   // delet old cart for hourly catgeory or hourly-fixed category 
   deleteOldCart(){
    let list = {
      serviceType: this.serviceType || this.serviceTypehr || 2,
      bookingType: this.bookingType,
      categoryId: this.catId,
      providerId: this.providerId,
      serviceId: this.subCatList._id,
      quntity: this.subCatList.qty || this.minHours,
      action: 2,
      // callType:2
    };
    !list['serviceType'] ?  list['serviceType'] = 2 :'';
    this._service.addCart(list).subscribe((res)=>{
        if(res['data']){
        this.cartList = res['data'];
        console.log("delete cart", this.cartList)
        this.cartList.item.forEach(x => {
          // if (x.quntity > 0) {
            var serviceId = x.serviceId;
            if (this.cartList.serviceType != 1) {
              this.subCatList.btnActive = false;
              this.subCatList.qty = x.quntity;
              for (var i = 0; i < this.serviceList.length; i++) {
                this.serviceList[i].service.forEach(x => {
                  x.btnActive = true;
                });
              }
            }
          // }
        });
        // setTimeout(()=>{
          this.reduceCart();
        // },100)
      }
    },error =>{
        console.log("error for deleting cart",error)
      });
  }
   // remove old cart if there and add new cart based on minHours for hourl catgeory 
   deleteCartForFirst:any;
   reduceCart(){
     let remainHours = 1;
     console.log("sub", this.subCatList)
      if(this.subCatList.qty <= 0){
       this.action = 1;
       this.qty = this.minHours;
       this.addCartList('1', 0);
      }
      // else if(!this.subCatList.qt){
      //   this.action = 1;
      //   this.qty = this.minHours;
      //   this.addCartList('1', 0);
      // }
      this.deleteCartForFirst = true;
   }
   // card#308, issue: Get rid of capital letters, fixedBy:sowmyaSv, date: 6-1-20
   makeFirstCharUpper(valueArr){
    valueArr.map((item,index)=>{
      item && item['sub_cat_name'] ? item['sub_cat_name'] = (item['sub_cat_name'].charAt(0).toUpperCase()+ item['sub_cat_name'].slice(1).toLowerCase()) :'';
    });
    // console.log("valueArr", valueArr);
    this.serviceList = Object.assign([], valueArr);
    return this.serviceList;
    // !this.changeDetection.detectChanges['destroyed'] ? this.changeDetection.detectChanges() :'';
    // this.serviceList = valueArr;
    // this.changeDetection.detectChanges();
    // return valueArr;
   }
    // card#308, issue: Get rid of capital letters, fixedBy:sowmyaSv, date: 6-1-20
   trackItem(index, song){
    return `${index}-${song.sub_cat_id}`;
   }
  //  ngAfterContentInit() {
  //    setTimeout(()=>{
  //     // this.makeFirstCharUpper(this.serviceList);
  //     this.changeDetection.detectChanges();
  //    }, 1000)
  //  } 
   ngAfterViewChecked() : void {
    // your code
    this.changeDetection.detectChanges();
}
}
