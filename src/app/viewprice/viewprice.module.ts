import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ViewpriceComponent } from './viewprice.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: ViewpriceComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    ViewpriceComponent
  ]
})
export class ViewpriceModule { }
