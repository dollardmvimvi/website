import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';
import { LivechatWidgetModule } from '@livechat/angular-widget';
import { Angular2SocialLoginModule } from "angular2-social-login";
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LazyLoadImageModule } from 'ng-lazyload-image';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { SharedModule } from './shared/shared.module';
// import { TimeslotComponent } from './timeslot/timeslot.component';
// import { CalltypeComponent } from './calltype/calltype.component';
// import { ViewpriceComponent } from './viewprice/viewprice.component';
// import { CheckoutComponent } from './checkout/checkout.component';
// import { ProfileComponent } from './profile/profile.component';
// import { ProviderComponent } from './provider/provider.component';
// import { BecomeaproComponent } from './becomeapro/becomeapro.component';
// import { AboutComponent } from './about/about.component';
// import { PrivacyComponent } from './privacy/privacy.component';
// import { TermsComponent } from './terms/terms.component';
// import { ContactusComponent } from './contactus/contactus.component';
// import { ReviewComponent } from './review/review.component';
// import { BiddingComponent } from './bidding/bidding.component';

let providers = {
  "google": {
    "clientId": "453431831056-ohq2u57u0qomr1j6co1ika72ut5cba78.apps.googleusercontent.com"
  },
  // "linkedin": {
  //   "clientId": "LINKEDIN_CLIENT_ID"
  // },
  "facebook": {
    "clientId": "2218146521551471",
    "apiVersion": "v2.11" //like v2.4 
  }
};


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: '', component: PaymentSuccessComponent },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
  { path: 'becomekyhootpro', loadChildren: './becomeapro/becomeapro.module#BecomeaproModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsModule' },
  { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyModule' },
  { path: 'about', loadChildren: './about/about.module#AboutModule' },
  { path: 'contact-us', loadChildren: './contactus/contactus.module#ContactusModule' },
  { path: 'call-type/:name/:id', loadChildren: './calltype/calltype.module#CalltypeModule' },
  { path: 'time-slot/:id/:Pid', loadChildren: './timeslot/timeslot.module#TimeslotModule' },
  { path: ':name/:id', loadChildren: './bidding/bidding.module#BiddingModule' },
  { path: 'review/:city/:name/:id/:Pid', loadChildren: './review/review.module#ReviewModule' },
  { path: 'checkout/:id/:Pid', loadChildren: './checkout/checkout.module#CheckoutModule' },
  { path: ':city/:name/:id/:Pid', loadChildren: './viewprice/viewprice.module#ViewpriceModule' },
  { path: ':city/:name/:id', loadChildren: './provider/provider.module#ProviderModule' },
  { path: '**', component: HomeComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    PaymentSuccessComponent,
    // TimeslotComponent,
    // CalltypeComponent,
    // ViewpriceComponent,
    // CheckoutComponent,
    // ProfileComponent,
    // ProviderComponent,
    // BecomeaproComponent,
    // AboutComponent,
    // PrivacyComponent,
    // TermsComponent,
    // ContactusComponent,
    // ReviewComponent,
    // BiddingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxCarouselModule,
    HttpClientModule,
    Angular2SocialLoginModule,
    LazyLoadImageModule,
    TagInputModule,
    LivechatWidgetModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

Angular2SocialLoginModule.loadProvidersScripts(providers);
