import { Component } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { GoogleAnalyticsEventsService } from "./google-analytics-events.service";
import { TranslateService } from "@ngx-translate/core";
import { Configuration } from "./app.constant";
import { HomeService } from '../app/home/home.service';
declare var ga: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "app";

  constructor(
    public router: Router,
    public googleAnalyticsEventsService: GoogleAnalyticsEventsService,
    public translate: TranslateService,
    private _conf: Configuration,
    private homeService: HomeService
  ) {
    this.router.events.subscribe(event => {
      // console.log("ga", event)
      if (event instanceof NavigationEnd) {
        ga("set", "page", event.urlAfterRedirects);
        ga("send", "pageview");
        this.homeService.getCurrentURL(event.urlAfterRedirects);
      }
    });
  }

  ngOnInit() {}

  translates(val) {
    console.log("lang -->", val)
    this.translate.use(val);
    this._conf.setItem("lang",val);
    location.reload()
  }

}
