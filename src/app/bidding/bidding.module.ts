import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BiddingComponent } from './bidding.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: BiddingComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    BiddingComponent
  ]
})
export class BiddingModule { }
