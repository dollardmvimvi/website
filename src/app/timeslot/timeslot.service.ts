import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';



@Injectable({
  providedIn: 'root'
})

export class TimeSlotService {


  constructor(private _http: HttpClient, private _conf: Configuration) {
  }

  timeslotList(bookingData) {
    this._conf.setAuth();
    return this._http.get(this._conf.CustomerUrl + 'providerSlot/'+bookingData.providerId+'/'+bookingData.categoryId+'/'+bookingData.date+'/'+bookingData.callType, { headers: this._conf.headers });
  }


  

}
