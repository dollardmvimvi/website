import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { LanguageService } from "../app.language";
import { MissionService } from "../app.service";
import { Configuration } from "../app.constant";
import { Location } from "@angular/common";
import { TimeSlotService } from "./timeslot.service";

declare var $: any;
declare var moment: any;

@Component({
  selector: "timeslot",
  templateUrl: "./timeslot.component.html",
  styleUrls: ["./timeslot.component.css"]
})
export class TimeslotComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _conf: Configuration,
    private _missionService: MissionService,
    private _service: TimeSlotService
  ) {}

  shimmer: any = [];
  loadershimmer = false;
  catId: any;
  PId: any;
  MorningSlots = [];
  EveningSlot = [];
  AfterNoonSlots = [];
  NightSlot = [];
  providerTimeSlot = [];
  dateCount = 0;
  datechange = 0;
  CurrentDateTimSlot: any;
  selectedTimeSlot: any;
  SeletcedTimeDuration: any;
  noTimeSlot: boolean = false;

  ngOnInit() {
    this.placeHolder();
    this.route.params.subscribe(params => {
      this.catId = params["id"];
      this.PId = params["Pid"];
      this.getTimeSlot();
    });
  }

  placeHolder() {
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  getTimeSlot() {
    this.MorningSlots = [];
    this.EveningSlot = [];
    this.AfterNoonSlots = [];
    this.NightSlot = [];   
    let presentDate = moment(new Date()).format("ddd MMM DD, YYYY");
    let currentDate1 = moment(new Date())      
      .format("ddd L");
    let currentTime = moment(new Date(), "HH:mm A").add(1, "hour").format("HH:mm A");
    console.log("the current time is :", currentTime);
    let timestamp3 = moment(
      currentDate1 + ", 5:30:00 AM",
      "ddd L, hh:mm:ss A"
    ).unix();
    if (this.dateCount && this.dateCount > 0) {
      currentDate1 = moment(new Date())
        .add(1, "days")
        .format("ddd L");
      timestamp3 = moment(
        this.CurrentDateTimSlot + ", 5:30:00 AM",
        "ddd MMM DD YYYY, hh:mm:ss A"
      ).unix();
    } else {
      console.log("-------------------------current date");
      this.CurrentDateTimSlot = moment(new Date()).format("ddd MMM DD, YYYY");

    }    
    let list = {
      providerId: this.PId,
      categoryId: this.catId,
      date: timestamp3,
      callType: 1
    };

    if (this.datechange == 0) {
      this.loadershimmer = false;
    }

    console.log("the llist for provider slot", list);
    this._service.timeslotList(list).subscribe(
      (res: any) => {
        this.loadershimmer = true;
        console.log("customer/providerSlot", res);
        this.providerTimeSlot = res.data;
        res.data.forEach(x => {
          x.active = false;
          // x.from =  moment.unix(x.from).format("HH:mm A");   
          // if(x.from  >= "06:00 AM" && x.from >= "11:59 PM" && x.isBook == 1) {
          //   this.MorningSlots.push(x)
          // }      
        });
        let todaySlots = [];
        var noTimeSlot = true;

        for (let inst = 0; inst < this.providerTimeSlot.length; inst++) {
          // TimeSlotFormat[inst].from = moment(TimeSlotFormat[inst].from).format("HH:mm");
          this.providerTimeSlot[inst].from = moment
            .unix(this.providerTimeSlot[inst].from)
            .format("HH:mm");
          //morning Slot
          if (
            this.providerTimeSlot[inst].from >= "05:30" &&
            this.providerTimeSlot[inst].from < "11:59" &&
            this.providerTimeSlot[inst].isBook == 1
          ) {
            if (this.CurrentDateTimSlot == presentDate) {
              if (
                this.providerTimeSlot[inst].from > currentTime &&
                this.providerTimeSlot[inst].from < "11:59"
              ) {
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.MorningSlots.push(this.providerTimeSlot[inst]);
                todaySlots.push(this.providerTimeSlot[inst]);
              }
            } else {
              let timestampConvertor = moment(
                this.providerTimeSlot[inst].from,
                "hh:mm A"
              ).format("hh:mm A");
              this.providerTimeSlot[inst].from = timestampConvertor;
              this.MorningSlots.push(this.providerTimeSlot[inst]);
            }

            if (this.MorningSlots.length == 0) {
              noTimeSlot = true;
            } else {
              noTimeSlot = false;
            }
          }
          //AfterNoon slot
          else if (
            this.providerTimeSlot[inst].from >= "12:00" &&
            this.providerTimeSlot[inst].from < "15:59" &&
            this.providerTimeSlot[inst].isBook == 1
          ) {
            if (this.CurrentDateTimSlot == presentDate) {
              if (
                this.providerTimeSlot[inst].from > currentTime &&
                this.providerTimeSlot[inst].from < "15:59"
              ) {
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.AfterNoonSlots.push(this.providerTimeSlot[inst]);
                todaySlots.push(this.providerTimeSlot[inst]);
              }
            } else {
              let timestampConvertor = moment(
                this.providerTimeSlot[inst].from,
                "hh:mm A"
              ).format("hh:mm A");
              this.providerTimeSlot[inst].from = timestampConvertor;
              this.AfterNoonSlots.push(this.providerTimeSlot[inst]);
            }
            if (this.AfterNoonSlots.length == 0) {
              noTimeSlot = true;
            } else {
              noTimeSlot = false;
            }
          }
          //Evening slot
          else if (
            this.providerTimeSlot[inst].from >= "16:00" &&
            this.providerTimeSlot[inst].from < "19:59" &&
            this.providerTimeSlot[inst].isBook == 1
          ) {
            if (this.CurrentDateTimSlot == presentDate) {
              if (
                this.providerTimeSlot[inst].from > currentTime &&
                this.providerTimeSlot[inst].from < "19:59"
              ) {
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.EveningSlot.push(this.providerTimeSlot[inst]);
                todaySlots.push(this.providerTimeSlot[inst]);
              }
            } else {
              let timestampConvertor = moment(
                this.providerTimeSlot[inst].from,
                "hh:mm A"
              ).format("hh:mm A");
              this.providerTimeSlot[inst].from = timestampConvertor;
              this.EveningSlot.push(this.providerTimeSlot[inst]);
            }
            if (this.EveningSlot.length == 0) {
              noTimeSlot = true;
            } else {
              noTimeSlot = false;
            }
          }
          //Night slot
          else if (
            this.providerTimeSlot[inst].from >= "20:00" &&
            this.providerTimeSlot[inst].from <= "24:00" &&
            this.providerTimeSlot[inst].isBook == 1
          ) {
            if (this.CurrentDateTimSlot == presentDate) {
              if (
                this.providerTimeSlot[inst].from > currentTime &&
                this.providerTimeSlot[inst].from <= "24:00"
              ) {
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.NightSlot.push(this.providerTimeSlot[inst]);
              }
            } else {
              if( this.providerTimeSlot[inst].from >= "00:00" &&
              this.providerTimeSlot[inst].from <= "05:30"){
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.NightSlot.push(this.providerTimeSlot[inst]);
              }
              else{
                let timestampConvertor = moment(
                  this.providerTimeSlot[inst].from,
                  "hh:mm A"
                ).format("hh:mm A");
                this.providerTimeSlot[inst].from = timestampConvertor;
                this.NightSlot.push(this.providerTimeSlot[inst]);
              }
            }
            if (this.NightSlot.length == 0) {
              noTimeSlot = true;
            } else {
              noTimeSlot = false;
            }
          }
        }
        this.noTimeSlot = noTimeSlot;
        console.log("noTimeSlot", noTimeSlot);
      },
      err => {
        console.log(err.error.message);
        if (err.status == 498) {
          this._missionService.loginOpen(true);
        }
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  selectDate(inst) {
    this.timeList = undefined;
    this.datechange = 1;
    if (inst == 1) {
      let currentDate = moment(new Date()).format("ddd MMM DD, YYYY");
      this.dateCount--;
      if (this.CurrentDateTimSlot !== currentDate) {
        this.CurrentDateTimSlot = moment(new Date())
          .add(this.dateCount, "days")
          .format("ddd MMM DD, YYYY");
        console.log("the current date is :", this.CurrentDateTimSlot);
        this.getTimeSlot();
      } else {
        this.CurrentDateTimSlot = moment(new Date()).format("ddd MMM DD, YYYY");
        this.getTimeSlot();
      }
    } else {
      ++this.dateCount;
      this.CurrentDateTimSlot = moment(new Date())
        .add(this.dateCount, "days")
        .format("ddd MMM DD, YYYY");
      console.log("the current date is :", this.CurrentDateTimSlot);
      this.getTimeSlot();
    }
  }

  timeList:any;
  unselectedtimeslot=false;
  submitTimeSlot(event, index, caseValue) {
    console.log("the checkout variables :", event.target.dataset.value);
    this.selectedTimeSlot = event.target.dataset.value;
    if (this.selectedTimeSlot && this.selectedTimeSlot.length > 0) {
      this.unselectedtimeslot=false;
      if (
        this.providerTimeSlot.findIndex(
          item => item.from == this.selectedTimeSlot
        ) > -1
      ) {
        this.SeletcedTimeDuration = this.providerTimeSlot.findIndex(
          item => item.from == this.selectedTimeSlot
        );
      }
      let bookingDate1 = moment(this.CurrentDateTimSlot, "ddd MMM DD, YYYY").format(
        "DD MMM YYYY"
      );
      let TimeStampSchedule = moment(
        bookingDate1 + " " + this.selectedTimeSlot,
        "DD MMM YYYY hh:mm A"
      ).unix();
      let duration = this.providerTimeSlot[this.SeletcedTimeDuration].duration;
      let endTime = this.providerTimeSlot[this.SeletcedTimeDuration].to;
      console.log("the start time :", bookingDate1 + " " + this.selectedTimeSlot);
      console.log("the end time :",TimeStampSchedule, endTime);
      console.log("the duration is :", duration);
      console.log("the currenct date is:",this.CurrentDateTimSlot )
      this.timeList = {
        bookDate: moment(this.CurrentDateTimSlot, "ddd MMM DD, YYYY").format("dddd,D MMM Y"),
        endTimes:this.selectedTimeSlot,
        SDuration:duration,
        TimeStampSchedule:TimeStampSchedule,
        endTime:endTime,
        duration:duration
      }
    }
    // console.log(this.timeList)
    this.MorningSlots.forEach(x => {
      x.active = false;
    });
    this.AfterNoonSlots.forEach(x => {
      x.active = false;
    });
    this.EveningSlot.forEach(x => {
      x.active = false;
    });
    this.NightSlot.forEach(x => {
      x.active = false;
    });
    switch (caseValue) {
      case 1:
      this.MorningSlots[index].active =true;
        console.log("the index is :", caseValue, index);
        break;

      case 2:
      this.AfterNoonSlots[index].active =true;
        console.log("the index is :", caseValue, index);
        break;

      case 3:
      this.EveningSlot[index].active =true;
        console.log("the index is :", caseValue, index);
        break;
      case 4:
      this.NightSlot[index].active =true;
        console.log("the index is :", caseValue, index);
        break;
    }
  }

  checkOut() {   
    console.log("this.timeList", this.timeList);
    if(this.timeList != undefined){
      this._conf.setItem("inCallType", 1);
      this._conf.setItem("inCall", JSON.stringify(this.timeList));
    this._router.navigate(['./checkout', this.catId, this.PId]);
    }else{
      this.unselectedtimeslot=true;
    }
  }
}
