import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TimeslotComponent } from './timeslot.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: TimeslotComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    TimeslotComponent
  ]
})
export class TimeslotModule { }
