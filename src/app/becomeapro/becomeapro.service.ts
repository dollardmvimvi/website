import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Configuration } from '../app.constant';

@Injectable({
  providedIn: 'root'
})
export class BecomeaproService {

  constructor(private _http: HttpClient, private _conf: Configuration) {

  }

  cityList() {
    return this._http.get(this._conf.ProviderUrl + 'city', { headers: this._conf.headers });
  }

  catList(list) {
    console.log(list);
    return this._http.get(this._conf.ProviderUrl + 'serviceCateogries/' + list.cityId, { headers: this._conf.headers });
  }

  emailCheck(list) {
    console.log(list);
    return this._http.post(this._conf.ProviderUrl + 'emailValidation', list, { headers: this._conf.headers });
  }

  phoneCheck(list) {
    console.log(list);
    return this._http.post(this._conf.ProviderUrl + 'phoneValidation', list, { headers: this._conf.headers });
  }

  providerSignup(list) {
    console.log(list);
    return this._http.post(this._conf.WebsiteUrl + 'provider/signUp', list, { headers: this._conf.headers });
  }

  signUpOtp(list) {
    let body = list;
    console.log(body);
    return this._http.post(this._conf.ProviderUrl + 'verifyPhoneNumber', body, { headers: this._conf.headers });
  }


  resendOtp(list) {
    let body = list;
    console.log(body);
    return this._http.post(this._conf.ProviderUrl + 'resendOtp', body, { headers: this._conf.headers });
  }

}
