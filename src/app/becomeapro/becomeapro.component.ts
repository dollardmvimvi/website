import { Component, OnInit, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { NgxCarousel } from "ngx-carousel";
import { MissionService } from "../app.service";
import { Configuration } from "../app.constant";
import { LanguageService } from "../app.language";
import { BecomeaproService } from "./becomeapro.service";
import { TranslateService } from "@ngx-translate/core";

declare var $: any;
declare var moment: any;
declare var google: any;
declare var AWS: any;
declare var flatpickr: any;

@Component({
  selector: "becomeapro",
  templateUrl: "./becomeapro.component.html",
  styleUrls: ["./becomeapro.component.css"]
})
export class BecomeaproComponent implements OnInit {
  constructor(
    private _missionService: MissionService,
    private _service: BecomeaproService,
    private _lang: LanguageService,
    private _zone: NgZone,
    private _conf: Configuration,
    public translate: TranslateService
  ) {}

  proLan: any;
  cityName: any;
  citys: any;
  latlng: any;
  cityId: any;
  categoryList: any;
  catLoaderList = false;
  loaderList = false;
  shimmer: any = [];
  catVal: any = [];
  catlist: any = [];
  subCatVal: any = [];
  subCatlist: any = [];
  metaDataVal: any = [];
  metaData: any = [];
  dataMeta: any = [];
  documentDataVal: any = [];
  itemInDocList: any = [];
  errStep: any;

  firstName: string;
  lastName: string;
  address: string;
  gender: number;
  email: string;
  birthday: string;
  phoneNumber: string;
  password: string;
  itemsAsObjects: any;

  email_Error: any;
  registerSave = false;
  phone_Error: any;
  pwd_Error: any;

  stepBecome1: any;
  stepBecome2: any;
  stepBecome3: any;
  stepBecome4: any;
  widthBar: number;
  documentBtn = false;

  errMsg: any;
  loaderList1: any;
  OtpNumber: string;
  providerId: any;

  ngOnInit() {
    // this._missionService.scrollTop(true)
    this.proLan = this._lang.becomePro;
    this.cityName = "Select City";
    this.placeHolder();
    this.getCity();

    flatpickr("#dob", {
      altInput: true,
      allowInput: false,
      dateFormat: "m/d/Y",
      altFormat: "m/d/Y",
      defaultDate: moment()
        .add(-18, "years")
        .format("MM/DD/YYYY"),
      maxDate: moment()
        .add(-18, "years")
        .format("MM/DD/YYYY")
    });

    $("#becomePhone").intlTelInput({
      autoHideDialCode: true,
      geoIpLookup: callback => {
        $.get("https://ipinfo.io", () => {}, "jsonp").always(resp => {
          var countryCode = resp && resp.country ? resp.country : "";
          callback(countryCode);
        });
      },
      initialCountry: "auto",
      nationalMode: true,
      separateDialCode: true,
      utilsScript:
        "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/14.0.6/js/utils.js"
    });
  }

  placeHolder() {
    window.scrollTo(0, 0);
    for (var i = 0; i < 3; i++) {
      this.shimmer.push(i);
    }
  }

  getCity() {
    window.scrollTo(0, 0);
    this._service.cityList().subscribe(
      (res: any) => {
        this.becomeStep1();
        this.loaderList = true;
        console.log("provider/city", res);
        this.citys = res.data;
      },
      err => {
        console.log(err._body);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  listCat(i) {
    this.errStep = false;
    this.cityName = this.citys[i].city;
    this.cityId = this.citys[i].id;
    this.categoriesList(this.citys[i].id);
    this.widthBar = 25;
  }

  categoriesList(val) {
    let list = {
      cityId: val || "0"
    };
    this.catLoaderList = true;
    this._service.catList(list).subscribe(
      (res: any) => {
        this.catLoaderList = false;
        console.log("provider/categories", res);
        this.categoryList = res.data;
        this.catVal = [];
        this.subCatVal = [];
        this.metaData = [];
        this.documentDataVal = [];
        this.categoryList.forEach(x => {
          x.checked = false;
          x.isSubCat = true;
          if (x.subCategory && x.subCategory.length > 0) {
            x.isSubCat = false;
          }
          // if (x.metaData && x.metaData.length > 0) {
          //   x.checked = false;
          // }
        });
      },
      err => {
        console.log(err);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  catSelect(i) {
    if (!this.categoryList[i].checked) {
      this.categoryList[i].checked = true;
    } else {
      this.categoryList[i].checked = false;
    }
    this.categoryList[i].subCategory.forEach(x => {
      x.checked = false;
    });
    this.catSelectList();
  }

  subCatSelect(i, j) {
    if (!this.categoryList[i].subCategory[j].checked) {
      this.categoryList[i].subCategory[j].checked = true;
    } else {
      this.categoryList[i].subCategory[j].checked = false;
    }
    this.catSelectList();
  }

  catSelectList() {
    this.errStep = false;
    this.catVal = [];
    this.subCatVal = [];
    this.categoryList.forEach(x => {
      if (x.checked == true) {
        this.catVal.push(x);
        /** sub category **/
        if (x.subCategory && x.subCategory.length > 0) {
          var index = x.subCategory.findIndex(y => y.checked == true);
          // console.log(index)
          if (index > -1) {
            x.isSubCat = true;
          } else {
            x.isSubCat = false;
          }
        }
        // x.subCategory.forEach(y => {
        //   if (y.checked == true) {
        //     this.subCatVal.push(y);
        //   }
        // })

        /** meta **/
        // if (x.metaData && x.metaData.length > 0) {
        //   x.metaData.forEach(m => {
        //     m.checked = false;
        //     if (m.preDefined && m.preDefined.length > 0) {
        //       m.preDefined.forEach(p => {
        //         p.checked = false;
        //       });
        //     }
        //   });
        // }
        /** document **/
        // if (x.document && x.document.length > 0) {
        //   x.document.forEach(d => {
        //     d.checked = false;
        //     d.field.forEach(f => {
        //       f.checked = false;
        //     });
        //   });
        // }
      }
    });
    // console.log(this.subCatVal)
  }

  becomeStep1() {
    this.errStep = false;
    this.widthBar = 0;
    $(".2nd").removeClass("active");
    this.stepBecome1 = true;
    this.stepBecome2 = false;
    this.stepBecome3 = false;
    this.stepBecome4 = false;
    // this._missionService.scrollTops(true);
  }

  becomeStep2() {
    this.errStep = false;
    if (this.cityId) {
      var stepActive = false;
      this.metaDataVal = [];
      this.documentDataVal = [];
      this.metaData = [];
      this.dataMeta = [];
      this.catlist = [];
      this.subCatlist = [];
      for (var k = 0; k < this.catVal.length; k++) {
        if (
          this.catVal[k].subCategory &&
          this.catVal[k].subCategory.length > 0
        ) {
          /** subCat List **/
          for (var i = 0; i < this.catVal[k].subCategory.length; i++) {
            if (this.catVal[k].subCategory[i].checked == true) {
              const indexSub = this.catVal[k].subCategory.indexOf(
                this.catVal[k].subCategory[i]
              );
              this.subCatlist.push(this.catVal[k].subCategory[indexSub]._id);
            }
          }
        } else {
          if (this.catVal[k].checked == true) {
            stepActive = true;
          } else {
            stepActive = false;
          }
        }

        if (this.catVal[k].isSubCat == false) {
          stepActive = false;
          this.errStep =
            "Please Select SubCategory of " +
            this.catVal[k].catName +
            " category";
          return false;
        } else {
          stepActive = true;
        }

        /** cat List **/
        if (this.catVal[k].checked == true) {
          const index = this.catVal.indexOf(this.catVal[k]);
          // console.log(index);
          this.catlist.push(this.catVal[index].catId);
        }
        /** metaData **/
        if (this.catVal[k].metaData) {
          for (var m = 0; m < this.catVal[k].metaData.length; m++) {
            this.metaDataVal.push(this.catVal[k].metaData[m]);
            // this.metaDataVal.forEach(x => {
            //   x.checked = false;
            // });
          }
        }

        /** Document **/
        if (this.catVal[k].document) {
          for (var m = 0; m < this.catVal[k].document.length; m++) {
            this.documentDataVal.push(this.catVal[k].document[m]);
          }
        }
      }

      /** metaData  unique id **/
      var seen = {};
      var uniqueStudents = this.metaDataVal.filter(item => {
        if (seen.hasOwnProperty(item._id)) {
          return false;
        } else {
          this.metaData.push(item);
          seen[item._id] = true;
          return true;
        }
      });

      // console.log("catlist", this.catlist.toString());
      // console.log("subCatlist", this.subCatlist.toString());

      // console.log(stepActive)
      if (stepActive == false) {
        this.errStep = "Please select the service category";
      } else {
        this.widthBar = 50;
        $(".2nd").addClass("active");
        $(".3nd").removeClass("active");
        this.stepBecome1 = false;
        this.stepBecome2 = true;
        this.stepBecome3 = false;
        this.stepBecome4 = false;
        // this.becomeStep4();
      }
      console.log("metaData", this.metaData);
      console.log("documentDataVal", this.documentDataVal);
      this._missionService.scrollTops(true);
    } else {
      this.errStep = "Please select the city";
    }

    if (this.documentDataVal && this.documentDataVal.length > 0) {
      this.documentBtn = true;
    } else {
      this.documentBtn = false;
    }
    // console.log("document", this.documentDataVal);
    // console.log("document", this.documentDataVal);
  }

  dob: any;
  becomeStep3() {
    this.errStep = false;
    var dob = $("#dob").val();
    let profile = $(".imgFilesmeta").attr("src");
    let lat = $(".latpro").val();
    if (
      this.firstName &&
      lat &&
      dob &&
      this.lastName &&
      this.gender &&
      profile
    ) {
      var bod = false;
      if (dob) {
        var a = moment()
          .add(-18, "years")
          .format("YYYY-MM-DD");
        var b = moment(dob, "MM/DD/YYYY").format("YYYY-MM-DD");
        // console.log(a, b)
        if (a >= b) {
          bod = true;
          this.dob = false;
        } else {
          bod = false;
          this.dob = true;
          this.errStep = "madatory field is missing";
        }
      }
      var isManadatory = true;
      if (this.metaData && this.metaData.length > 0) {
        this.metaData.forEach(x => {
          if (
            (x.fieldType != 2 && x.isManadatory == 1 && !x.checked) ||
            ((x.fieldType == 2 &&
              x.isManadatory == 1 &&
              !x.checked &&
              !x.datas) ||
              (x.datas && x.datas.length == 0))
          ) {
            this.errStep = "Please fill the "+" "+x.fieldName+" "+"to continue";
            isManadatory = false;
          }
        });
      }

      if (isManadatory && bod) {
        this.widthBar = 75;
        this.errStep = false;
        $(".3nd").addClass("active");
        $(".4nd").removeClass("active");
        this.stepBecome1 = false;
        this.stepBecome2 = false;
        this.stepBecome3 = true;
        this.stepBecome4 = false;
        this.listOfMeta();
        this._missionService.scrollTops(true);
      }
    } else {
      // this.dob = true;
      this.errStep = "madatory field is missing";
    }
  }

  becomeStep4() {
    this.errStep = false;
    if (
      this.registerSave &&
      this.email &&
      this.phoneNumber &&
      this.password &&
      !this.pwd_Error &&
      !this.email_Error &&
      !this.phone_Error
    ) {
      this.widthBar = 100;
      $(".4nd").addClass("active");
      this.stepBecome1 = false;
      this.stepBecome2 = false;
      this.stepBecome3 = false;
      this.stepBecome4 = true;
      this._missionService.scrollTops(true);
    } else {
      this.errStep = "madatory field is missing";
    }
  }

  noDocument() {
    this.errStep = false;
    if (
      this.registerSave &&
      this.email &&
      this.phoneNumber &&
      this.password &&
      !this.pwd_Error &&
      !this.email_Error &&
      !this.phone_Error
    ) {
      this.widthBar = 100;
      this.docmentList();
    } else {
      this.errStep = "madatory field is missing";
    }
  }

  submitProvider() {
    let catList = this.catlist.toString();
    let subCatlist = this.subCatlist.toString();
    var dob = $("#dob").val();
    let flag = $("#becomePhone").intlTelInput("getSelectedCountryData");
    let datetime = moment().format("YYYY-MM-DD HH:mm:ss");
    let profile = $(".imgFilesmeta").attr("src");
    let address = $("#locationAddress").val();
    let city = $("#city").val();
    let state = $("#state").val();
    let zip = $("#zipcode").val();
    let email = this.email.trim();
    let lat = $(".latpro").val();
    let lng = $(".lngpro").val();
    let list = {
      firstName: this.firstName,
      lastName: this.lastName,
      email: email.toLowerCase(),
      password: this.password,
      countryCode: "+" + flag.dialCode,
      mobile: this.phoneNumber,
      gender: this.gender,
      dob: dob || "",
      // link: "",
      catlist: catList,
      subCatlist: subCatlist,
      cityId: this.cityId,
      latitude: lat || "",
      longitude: lng || "",
      profilePic: profile || "",
      // deviceTime: datetime,
      document: this.itemInDocList || "",
      metaData: this.dataMeta || "",
      addLine1: address,
      addLine2: "",
      city: city || "",
      state: state || "",
      country: "",
      placeId: "",
      pincode: zip || "",
      taggedAs: "",
      referralCode: ""
    };
    // console.log("provider", list);

    this._service.providerSignup(list).subscribe(
      (res: any) => {
        console.log("provider/signUp", res);
        this.providerId = res.data.providerId;
        $("#verificationOtp").modal("show");
        this.OtpNumber = "1111";
        this.countdown();
      },
      err => {
        console.log(err.error);
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  /** metaData **/
  metaPredefined(i, j) {
    this.errStep = false;
    if (
      !this.metaData[i].preDefined[j].checked ||
      this.metaData[i].preDefined[j].checked == undefined
    ) {
      this.metaData[i].preDefined[j].checked = true;
    } else {
      this.metaData[i].preDefined[j].checked = false;
    }

    if (this.metaData[i].preDefined && this.metaData[i].preDefined.length > 0) {
      var index = this.metaData[i].preDefined.findIndex(y => y.checked == true);
      // console.log(index)
      if (index > -1) {
        this.metaData[i].checked = true;
      } else {
        this.metaData[i].checked = false;
      }
    }

    // this.metaData[i].preDefined.every(x => {
    //   if (x.checked == false) {
    //     this.metaData[i].checked = false;
    //   } else {
    //     this.metaData[i].checked = true;
    //   }
    // });

    var metaName = [];
    this.metaData[i].preDefined.forEach(x => {
      if (x.checked == true) {
        metaName.push(x.name);
        this.metaData[i].data = metaName.toString();
      }
    });
  }

  metaTextAreaList(i, event) {
    this.errStep = false;
    if (event.target.value.length > 0) {
      this.metaData[i].data = event.target.value;
      this.metaData[i].checked = true;
    } else {
      this.metaData[i].checked = false;
    }
  }

  listOfMeta() {
    this.metaData.forEach(x => {
      if (x.datas && x.datas.length > 0) {
        var tags = [];
        x.datas.forEach(y => {
          tags.push(y.value);
        });
        x.checked = true;
        x.data = tags.toString();
        // console.log(tags.toString());
      } else if (x.datas && x.datas.length == 0) {
        x.checked = false;
      }
      if (x.checked == true) {
        let list = {
          metaId: x._id,
          data: x.data
        };
        this.dataMeta.push(list);
      }
    });
    console.log("dataMeta", this.dataMeta);
  }

  /** document **/

  documentTextList(event, i, j) {
    this.errStep = false;
    if (event.target.value.length > 0) {
      this.documentDataVal[i].field[j].data = event.target.value;
      this.documentDataVal[i].field[j].catId = this.documentDataVal[
        i
      ].Service_category_id;
      this.documentDataVal[i].field[j].checked = true;
    } else {
      this.documentDataVal[i].field[j].checked = false;
    }
  }

  documentUploadFile(e, i, j) {
    this.errStep = false;
    this.documentDataVal[i].field[j].loader = true;
    var bucket = new AWS.S3({ params: { Bucket: "kyhoot2" } });
    var fileChooser = e.srcElement;
    var file = fileChooser.files[0];
    let date = moment().format("DYMhmsA");
    if (file) {
      var params = {
        Key: "keymaing" + date,
        ContentType: file.type,
        Body: file
      };
      bucket.upload(params, (err, data) => {
        this._zone.run(() => {
          console.log(data);
          this.documentDataVal[i].field[j].data = data.Location;
          this.documentDataVal[i].field[j].catId = this.documentDataVal[
            i
          ].Service_category_id;
          this.documentDataVal[i].field[j].checked = true;
          this.documentDataVal[i].field[j].loader = false;
        });
      });
    }
    return false;
  }

  removeImgDoc(i, j) {
    this.documentDataVal[i].field[j].data = "";
    this.documentDataVal[i].field[j].checked = false;
  }

  pastDatePicker(event, i, j) {
    this.errStep = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    console.log(value, event.target.value.length);

    flatpickr("#" + value, {
      altInput: true,
      // enableTime: true,
      allowInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      maxDate: new Date(),
      // minTime: moment().format('HH:MM'),
      onChange: x => {
        let date = $("#" + value).val();
        console.log(x, date);
        this.documentDataVal[i].field[j].data = date;
        this.documentDataVal[i].field[j].catId = this.documentDataVal[
          i
        ].Service_category_id;
        this.documentDataVal[i].field[j].checked = true;
      }
    });
    if (event.target.value.length == 0) {
      this.documentDataVal[i].field[j].checked = false;
    }
  }

  futureDatePicker(event, i, j) {
    this.errStep = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var value = idAttr.nodeValue;
    console.log(value, event.target.value.length);

    flatpickr("#" + value, {
      altInput: true,
      // enableTime: true,
      allowInput: true,
      dateFormat: "Y-m-d",
      altFormat: "Y-m-d",
      minDate: new Date(),
      // minTime: moment().format('HH:MM'),
      onChange: x => {
        let date = $("#" + value).val();
        console.log(x, date);
        this.documentDataVal[i].field[j].data = date;
        this.documentDataVal[i].field[j].catId = this.documentDataVal[
          i
        ].Service_category_id;
        this.documentDataVal[i].field[j].checked = true;
      }
    });
    if (event.target.value.length == 0) {
      this.documentDataVal[i].field[j].checked = false;
    }
  }

  docmentList() {
    this.errStep = false;
    var dataDocument = [];
    var documentArr = [];
    this.itemInDocList = [];
    console.log(this.documentDataVal);
    this.documentDataVal.forEach(x => {
      x.field.forEach(y => {
        if (y.checked == true) {
          let list = {
            catId: y.catId,
            isManadatory: y.isManadatory,
            fId: y.fId,
            fName: y.fName,
            fType: y.fType,
            data: y.data
          };
          dataDocument.push(list);
        }
      });
    });
    for (var i = 0; i < dataDocument.length; i++) {
      if (documentArr.includes(dataDocument[i].catId) == false) {
        documentArr.push(dataDocument[i].catId);
      }
    }

    for (var j = 0; j < documentArr.length; j++) {
      var items = [];
      for (var k = 0; k < dataDocument.length; k++) {
        if (documentArr[j] == dataDocument[k].catId) {
          let item = {
            isManadatory: dataDocument[k].isManadatory,
            fId: dataDocument[k].fId,
            fName: dataDocument[k].fName,
            fType: dataDocument[k].fType,
            data: dataDocument[k].data
          };
          items.push(item);
        }
      }
      let list = {
        categoryId: documentArr[j],
        Fields: items
      };
      this.itemInDocList.push(list);
    }
    console.log("dataMeta", this.itemInDocList);
    var isManadatory = true;
    if (this.documentDataVal && this.documentDataVal.length > 0) {
      this.documentDataVal.forEach(x => {
        x.field.forEach(y => {
          if (y.isManadatory == 1 && !y.checked) {
            this.errStep = "Please fill the field of"+" "+ x.docName + " " + y.fName+ " "+"for the support team to review";
            isManadatory = false;
          }
        });
      });
    }
    if (isManadatory) {
      this.submitProvider();
    }
  }

  emailValidation(value) {
    this.errStep = false;
    // console.log(value);
    var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

    if (regexEmail.test(value)) {
      this.email_Error = false;
      this.registerSave = true;

      this.emailCheck(value);
    } else {
      if (value.length > 5) {
        this.email_Error = "The email ID is invalid";
      }
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  mobileValidation(value, event) {
    this.errStep = false;
    var target = event.target || event.srcElement || event.currentTarget;
    var idAttr = target.attributes.id;
    var id = idAttr.nodeValue;
    // console.log(event, $(`#${value}`));
    const ele = $(`#${id}`);
    // telInput = element;
    // console.log('value--->', telInput2, value, telInput2.intlTelInput('isValidNumber'));
    if (ele.intlTelInput("isValidNumber")) {
      this.phone_Error = false;
      this.registerSave = true;
      this.phoneCheck(value);
    } else {
      if (value.length > 5) {
        this.phone_Error = "The Mobile number entered is incorrect";
      }
      this.registerSave = false;
    }
  }

  emailCheck(value) {
    let list = {
      email: value
    };
    this._service.emailCheck(list).subscribe(
      (res: any) => {
        this.email_Error = false;
        this.registerSave = true;
      },
      err => {
        console.log(err.error.message);
        this.email_Error = err.error.message;
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  phoneCheck(value) {
    let flag = $("#becomePhone").intlTelInput("getSelectedCountryData");
    let list = {
      countryCode: "+" + flag.dialCode,
      mobile: value
    };
    this._service.phoneCheck(list).subscribe(
      (res: any) => {
        this.phone_Error = false;
        this.registerSave = true;
      },
      err => {
        console.log(err.error.message);
        this.phone_Error = err.error.message;
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  passwordValidation(val) {
    var regexPhone = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/;
    if (val.match(regexPhone)) {
      this.pwd_Error = false;
      this.registerSave = true;
    } else {
      this.pwd_Error =
        "Passwords must contain at least six characters, including uppercase, lowercase letters and a number.";
      this.registerSave = false;
      // setTimeout(() => {
      //   this.errMsg = false;
      // }, 3000)
    }
  }

  uploadFile(e) {
    $(".imgLoader").show();
    var bucket = new AWS.S3({ params: { Bucket: "kyhoot2" } });
    var fileChooser = e.srcElement;
    var file = fileChooser.files[0];
    let date = moment().format("DYMhmsA");
    if (file) {
      var params = {
        Key: "keymaing" + date,
        ContentType: file.type,
        Body: file
      };
      bucket.upload(params, (err, data) => {
        console.log(data);
        $(".imgFile").show();
        $(".imgFilesmeta").attr("src", data.Location);
        $(".imgLoader").hide();
      });
    }
    return false;
  }

  removeUpload() {
    $(".imgFile").hide();
    $(".imgFilesmeta").attr("src", "");
  }

  locationSetting() {
    if (this.address.length > 0) {
      this.errStep = false;
      var input = document.getElementById("locationAddress");
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.addListener("place_changed", () => {
        var place = autocomplete.getPlace();
        // console.log(place)
        for (var i = 0; i < place.address_components.length; i += 1) {
          var addressObj = place.address_components[i];
          for (var j = 0; j < addressObj.types.length; j += 1) {
            if (addressObj.types[j] === "sublocality_level_2") {
              var address1 = addressObj.long_name;
            }
            if (addressObj.types[j] === "sublocality_level_1") {
              var address2 = addressObj.long_name;
            }
            if (addressObj.types[j] === "locality") {
              var city = addressObj.long_name;
            }
            if (addressObj.types[j] === "administrative_area_level_1") {
              var state = addressObj.long_name;
            }
            if (addressObj.types[j] === "country") {
              var country = addressObj.long_name;
            }
            if (addressObj.types[j] === "postal_code") {
              var zipcode = addressObj.long_name;
            }
          }
        }
        // if (address1 != undefined && address2 != undefined) {
        //   $("#address2").val(address1 + ',' + address2);
        // }
        $("#city").val(city);
        $("#state").val(state);
        $(".latpro").val(place.geometry.location.lat());
        $(".lngpro").val(place.geometry.location.lng());
        // $("#country").val(country);
        $("#zipcode").val(zipcode);
        // $("#phoneShip").val(place.formatted_phone_number);
      });
    } else {
      $(".latpro").val("");
    }
  }

  registrationOtp() {
    this.loaderList1 = true;
    let list = {
      code: this.OtpNumber,
      providerId: this.providerId
    };
    setTimeout(() => {
      this.loaderList1 = false;
      this.errMsg = false;
    }, 3000);
    this._service.signUpOtp(list).subscribe(
      (res: any) => {
        this.loaderList1 = false;
        // if (res.message == "Phone Number has been verified.") {
        this.errMsg = res.message;

        $(".modal").modal("hide");
        $("#orderSubmit").modal("show");
        // } else {
        //   this.errMsg = res.message;
        // }
      },
      err => {
        this.loaderList1 = false;
        var error = err.error;
        this.errMsg = error.message;
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  resendOtp() {
    this.loaderList1 = true;
    this.countdown();
    let list = {
      userId: this.providerId,
      trigger: 1,
      userType: 2
    };
    setTimeout(() => {
      this.loaderList1 = false;
      this.errMsg = false;
    }, 3000);
    this._service.resendOtp(list).subscribe(
      (res: any) => {
        this.loaderList1 = false;
        this.errMsg = res.message;
      },
      err => {
        this.loaderList1 = false;
        var error = err.error;
        this.errMsg = error.message;
        if (err.status == 440) {
          this._conf.setItem("sessionToken", err.error.data);
          this._missionService.refreshToken(true);
        }
      }
    );
  }

  timer = false;
  countdown() {
    this.timer = true;
    var timer2 = "2:00";
    var interval = setInterval(() => {
      var timer = timer2.split(":");
      //by parsing integer, I avoid all extra string processing
      var minutes = parseInt(timer[0], 10);
      var seconds = parseInt(timer[1], 10);
      --seconds;
      minutes = seconds < 0 ? --minutes : minutes;
      seconds = seconds < 0 ? 59 : seconds;
      seconds = seconds < 10 ? 0 + seconds : seconds;
      //minutes = (minutes < 10) ?  minutes : minutes;
      // console.log("minutes", minutes, seconds);
      if (seconds == 0) {
        this.timer = false;
      } else {
        this.timer = true;
      }
      $("#countdownPro").html(minutes + ":" + seconds);
      if (minutes < 0) clearInterval(interval);
      //check if both minutes and seconds are 0
      if (seconds <= 0 && minutes <= 0) clearInterval(interval);
      timer2 = minutes + ":" + seconds;
    }, 1000);
  }
}
