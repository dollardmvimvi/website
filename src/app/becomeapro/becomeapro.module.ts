import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';
import { RouterModule, Routes } from '@angular/router';
import { BecomeaproComponent } from './becomeapro.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: BecomeaproComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    BecomeaproComponent 
  ]
})
export class BecomeaproModule { }