import { TestBed, inject } from '@angular/core/testing';

import { BecomeaproService } from './becomeapro.service';

describe('BecomeaproService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BecomeaproService]
    });
  });

  it('should be created', inject([BecomeaproService], (service: BecomeaproService) => {
    expect(service).toBeTruthy();
  }));
});
