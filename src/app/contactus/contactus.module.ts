import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ContactusComponent } from './contactus.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [  
  { path: '', component: ContactusComponent }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    ContactusComponent
  ]
})
export class ContactusModule { }
